<?php
SESSION_START();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
	header('location:login.php');
	exit;
}
$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';

$query = "SELECT * FROM ".$tbname."_variables ORDER BY _ID";
$run   = mysqli_query($con , $query);
$num   = mysqli_num_rows($run);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $sitename; ?></title>
		<?php include_once('topscript.php'); ?>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
			<?php include_once('topnav.php'); ?>
            <div class="content-wrapper">
                <div class="content-container">
					<?php include_once('leftnav.php'); ?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row breadcrumb-div">
                                <div class="col-sm-6">
                                    <ul class="breadcrumb">
            							<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
            							<li class="active">Variables</li>
            						</ul>
                                </div>
                            </div>
							<div class="row page-title-div">
                                <div class="col-sm-6">
                                    <h2 class="title">Variables</h2>
                                </div>
                            </div>
                        </div>
                        <section class="section">
                            <div class="container-fluid">
								<?php if($result != ''){ 
											if($result == 'dsuccess'){
								?>
											<div class="alert alert-success">variable deleted successfully</div>
								<?php 
											}
											if($result == 'dfailed'){
								?>
											<div class="alert alert-danger">error occurred</div>
								<?php 
											}
											if($result == 'failed'){
								?>
											<div class="alert alert-danger">something went wrong</div>
								<?php 
											}
										} 
								?>
								<div class="row">
									<div class="col-md-12 text-right">
										<a class="btn btn-primary" href="editvariable.php?e_action=<?php echo encrypt('add',$encrypt); ?>">Add Variable</a>
									</div>
								</div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-body p-20">
                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Value</th>
                                                            <th></th>
															<th></th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Value</th>
                                                            <th></th>
															<th></th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
														<?php 
														if($num > 0){
															while($fetch = mysqli_fetch_assoc($run)){
														?>
                                                        <tr>
                                                            <td><?php echo $fetch['_Name'];?></td>
                                                            <td><?php echo $fetch['_Value'];?></td>
                                                            <td><a href="editvariable.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('edit' , $encrypt);?>">Edit</a></td>
															<td><a onclick="return confirm('are you sure want to delete this variable?');" href="variableaction.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('delete' , $encrypt);?>">Delete</a></td>
                                                        </tr>
														<?php }} ?>
                                                    </tbody>
                                                </table>
                                </div>
                            </div>
                        </section>
                    </div>
					 <?php include_once('rightsidebar.php'); ?>
                </div>
            </div>
        </div>
        <?php include_once('bottomscript.php'); ?>
        <script>
            $(function($) {
                $('#example').DataTable({"aaSorting": []});
            });
        </script>
    </body>
</html>
