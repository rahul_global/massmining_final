<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$id1 = str_replace(" ", "+", $_GET['id']);
$id = isset($id1) ? decrypt($id1,$encrypt) : '';

if($id == ''){
	header('location:newwithdrawls.php');
	exit;
}

$title      = $sitename." : edit-withdrawal";

$query 		= "select a.* , b._Baddress,b._Coin_Type as coin_type from ".$tbname."_wiithdrwal a LEFT JOIN ".$tbname."_users b on a._UserID = b._ID WHERE a._ID = ".$id;
$run	 	= mysqli_query($con,$query);
$num        = mysqli_num_rows($run);
if($num > 0)
{
	$fetch = mysqli_fetch_assoc($run);
	$userid = $fetch['_UserID'];
	$selwith = "Select sum(_Amount)as totalamount from ".$tbname."_wiithdrwal where _UserID = '".$userid."' and _Status = '2'";
	$rowwith = mysqli_fetch_assoc(mysqli_query($con,$selwith));
	$totalamount = $rowwith['totalamount'];

	$main_amt = $fetch['_Amount'] - WITH_FEES;
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="newwithdrawls.php">Withdrawals</a></li>
					<li class="active">Edit Withdrawal</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Edit Withdrawal</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">							
							<form class="form-horizontal" action="withdrawaction.php" method="post">
								<input type="hidden" name="id" id="id" value="<?php echo encrypt($id , $encrypt); ?>" >
								<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('edit' , $encrypt); ?>" >
								<div class="form-group">
								  <label class="control-label col-sm-2" for="email">Bitcoin Address:</label>
								  <div class="col-sm-5">
									<input type="text" class="form-control" id="txtbaddress" placeholder="Enter Bitcoin Address" name="txtbaddress" value="<?php echo $fetch['_Baddress'];?>" readonly>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Amount:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtamount" placeholder="Enter Price" name="txtamount" value="<?php echo number_format($main_amt,8);?>" readonly>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Source:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtsource" placeholder="Enter Pay To" name="txtsource" value="<?php echo $fetch['_Source'];?>" readonly>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Total Amount Paid:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtsource" placeholder="Enter Pay To" name="txtsource" value="<?php echo $totalamount;?>" readonly>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Date:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtdate" placeholder="Select Date" name="txtdate" value="<?php echo date('d-M-Y H:i:s',strtotime($fetch['_Datetime']));?>" readonly>
								  </div>
								</div>

								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Status:</label>
								  <div class="col-sm-5">          
									<select name="txtstatus" id="txtstatus" class="form-control" required>
										<option value="">--- Select Status ---</option>
										<option value="0" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == '0') ? "selected='selected'" : ''; ?>>Pending</option>
										<option value="2" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == '2') ? "selected='selected'" : ''; ?>>Payed</option>
										<option value="1" <?php echo (isset($fetch['_Status']) && $fetch['_Status'] == '1') ? "selected='selected'" : ''; ?>>Cancelled</option>
									</select>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Transaction Id:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txttransactionid" placeholder="Enter Transaction id" name="txttransactionid" value="<?php echo $fetch['_Taxid'];?>" required>
								  </div>
								</div>

								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Pay URL:</label>
								  <div class="col-sm-5">
								  	<?php if($fetch['coin_type'] == 1){ ?>
								  		<a target="_blank" href="https://blockchain.info/payment_request?address=<?php echo $fetch['_Baddress'];?>&amount=<?php echo $main_amt;?>">Pay</a>
								  		<br>
								  	<?php  }elseif($fetch['coin_type'] == 2){ ?>
								  		<a target="_blank" href="https://live.blockcypher.com/ltc/address/<?php echo $fetch['_Baddress'];?>">Pay LTC</a>
								  	<?php  }elseif($fetch['coin_type'] == 3){ ?>
								  		<a target="_blank" href="https://www.etherchain.org/account/<?php echo $fetch['_Baddress'];?>">Pay</a>
								  	<?php } ?>
								  </div>
								</div>

								
								<div class="form-group">        
								  <div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="button" onclick="location.href='newwithdrawls.php';" class="btn btn-primary">Back</button>
								  </div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
