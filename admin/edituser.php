<?php
SESSION_START();
include_once('../dbopen.php');
if(!isset($_SESSION['adminuserid']) || $_SESSION['adminuserid'] == ''){
	header('location:login.php');
	exit;
}

$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
$id     = (isset($_GET['id']) && $_GET['id'] != '') ? decrypt($_GET['id'] , $encrypt) : ''; 
$action = (isset($_GET['e_action']) && $_GET['e_action'] != '') ? decrypt($_GET['e_action'] , $encrypt) : ''; 

if($id != '' && $action == 'edit'){
	$query     = "SELECT * FROM ".$tbname."_users WHERE _ID = ".$id;
	$run       = mysqli_query($con , $query);
	$fetch     = mysqli_fetch_assoc($run);
	$breadcum  = "Edit User";
	$pagetitle = "Edit User";
	$btn       = 'UPDATE';
	$usersqry = "SELECT * FROM ".$tbname."_users WHERE _ID NOT IN (".$id.") ORDER BY _ID";
} else {
	$pagetitle = "Add User";
	$breadcum  = "Add User";
	$btn       = 'ADD';
	$usersqry = "SELECT * FROM ".$tbname."_users ORDER BY _ID";
}

$runusers = mysqli_query($con , $usersqry);
$numusers = mysqli_num_rows($runusers);
$usersdata= array();
if($numusers > 0){
	while($fetchusers = mysqli_fetch_assoc($runusers)){
		$usersdata[] = array('id'=>$fetchusers['_ID'] , 'name'=>$fetchusers['_Fullname']);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $sitename;?></title>
		<?php include_once('topscript.php'); ?>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <?php echo include_once('topnav.php'); ?>
            <div class="content-wrapper">
                <div class="content-container">
                    <?php include_once('leftnav.php'); ?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row breadcrumb-div">
                                <div class="col-sm-6">
                                    <ul class="breadcrumb">
            							<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
										<li><a href="users.php"> Users</a></li>
            							<li class="active"><?php echo $breadcum; ?></li>
            						</ul>
                                </div>
                            </div>
							<div class="row page-title-div">
                                <div class="col-sm-6">
                                    <h2 class="title"><?php echo $pagetitle; ?></h2>
                                </div>
                            </div>
                        
							<section class="section">
								<div class="container-fluid">
									<?php if($result != ''){ 
												if($result == 'usuccess'){
									?>
												<div class="alert alert-success">user updated successfully</div>
									<?php 
												}
												if($result == 'ufailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'isuccess'){
									?>
												<div class="alert alert-success">user added successfully</div>
									<?php 
												}
												if($result == 'ifailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'failed'){
									?>
												<div class="alert alert-danger">something went wrong</div>
									<?php 
												}
									?>
									<?php } ?>
									<div class="row">
										<div class="panel-body">
											<form class="form-horizontal" method="POST" action="useraction.php" enctype="multipart/form-data" onsubmit="return validate()">
												<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action ,$encrypt);?>">
												<input type="hidden" name="id" id="id" value="<?php echo encrypt($id ,$encrypt);?>">
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtname" name="txtname" placeholder="Fullname" value="<?php echo isset($fetch['_Fullname']) ? $fetch['_Fullname'] : '';?>">
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtusername" name="txtusername" placeholder="Username" value="<?php echo isset($fetch['_Username']) ? $fetch['_Username'] : '';?>">
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
													<div class="col-sm-10">
														<input type="email" class="form-control" id="txtemail" name="txtemail" placeholder="Email" value="<?php echo isset($fetch['_Email']) ? $fetch['_Email'] : '';?>" <?php echo $action == 'edit' ? 'readonly' : ''; ?>>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
													<div class="col-sm-10">
														<input type="password" class="form-control" id="txtpassword" name="txtpassword" placeholder="Password">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Confirm Password</label>
													<div class="col-sm-10">
														<input type="password" class="form-control" id="txtcpassword" name="txtcpassword" placeholder="Confirm Password">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Referral</label>
													<div class="col-sm-10">
														<select class="form-control" name="txtreferral" id="txtreferral">
															<option value="0">---Select Referral---</option>
															<?php if(sizeof($usersdata) > 0){
																	foreach($usersdata as $key=>$val){
															?>
															<option value="<?php echo $val['id']; ?>" <?php echo ($fetch['_Referal'] == $val['id']) ? 'SELECTED="SELECTED"' : '';?>><?php echo $val['name']; ?></option>
															<?php }} ?>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Account</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtaccount" name="txtaccount" value="<?php echo isset($fetch['_Account']) ? $fetch['_Account'] : ''; ?>">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10">
														<button type="submit" class="btn btn-primary" style="margin-top:20px;"><?php echo $btn; ?></button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</section>
						</div>
                    </div>
                    <?php include_once('rightsidebar.php'); ?>
                </div>
            </div>
        </div>
		<?php include_once('bottomscript.php'); ?>
		<script type="text/javascript">
			function validate(){
				var err = '';
				if(document.getElementById('txtname').value == ''){
					err += 'please enter name\n';
				}
				if(document.getElementById('txtusername').value == ''){
					err += 'please enter username\n';
				}
				if(document.getElementById('txtemail').value == ''){
					err += 'please enter email\n';
				}
				if(document.getElementById('txtaccount').value == ''){
					err += 'please enter account number\n';
				}
				if(document.getElementById('txtpassword').value != '' && document.getElementById('txtcpassword').value != ''){
					if(document.getElementById('txtpassword').value != document.getElementById('txtcpassword').value){
						err += "password not matched";
					}
				}
				if(err != ''){
					alert(err);
					return false;
				}
				return true;
			}
		</script>
    </body>
</html>