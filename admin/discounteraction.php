<?php
session_start();
include_once('../dbopen.php');

$action    = decrypt($_REQUEST['action'],$encrypt);
$baddress  = isset($_POST['txtbaddress']) ? replaceSpecialChar($_POST['txtbaddress']) : '';
$version   = $_REQUEST["txtversion"];

if($action == 'add'){

	$seluser = "Select * from ".$tbname."_users where _Baddress = '".$baddress."'";
	$rstuser = mysqli_query($con,$seluser);

	if(mysqli_num_rows($rstuser) > 0){

		$rowuser  = mysqli_fetch_assoc($rstuser);

			$selcheck = "Select * from ".$tbname."_discounters where _UserID = '".$rowuser['_ID']."'";
			$rstcheck = mysqli_query($con,$selcheck);
			$type = "0";
			if(mysqli_num_rows($rstcheck) > 0){
				$delthat = "delete from ".$tbname."_discounters where _UserID = '".$rowuser['_ID']."'";
				mysqli_query($con,$delthat);
			}

			$str = "INSERT INTO ".$tbname."_discounters (`_UserID` ,`_Type`, `_Version`,`_Datetime`) VALUES ( ";
			
			if($rowuser['_ID'] != ''){
				$str .= "'".$rowuser['_ID']."' ,";
			} else {
				$str .= " null , ";
			}

			if($type != ''){
				$str .= "'".$type."' ,";
			} else {
				$str .= " null , ";
			}

			if($version != ''){
				$str .= "'".$version."' ,";
			} else {
				$str .= " null , ";
			}
		
			if($today != ''){
				$str .= "'".$today."' ";
			} else {
				$str .= " null ";
			}

			$str .= " ) ";

			//echo $str;exit;

			$rstins = mysqli_query($con,$str);

			if($rstins){
				echo "<script type='text/javascript'>window.location = 'discounters.php?result=".encrypt('success',$encrypt)."';</script>";
				exit;
			}else{
				echo "<script type='text/javascript'>window.location = 'discounters.php?result=".encrypt('failed',$encrypt)."';</script>";
				exit;
			}

	}else{
		echo "<script type='text/javascript'>window.location = 'discounters.php?result=".encrypt('nouser',$encrypt)."';</script>";
		exit;
	}
}
if($action == 'delete'){

	$id = decrypt($_REQUEST['id'],$encrypt);
	$query = "delete from ".$tbname."_discounters where _ID= ".$id;

	if($run = mysqli_query($con , $query)){

		echo "<script type='text/javascript'>window.location = 'discounters.php?result=".encrypt('success',$encrypt)."';</script>";
		exit;

	} else {

		echo "<script type='text/javascript'>window.location = 'discounters.php?result=".encrypt('failed',$encrypt)."';</script>";
		exit;

	}
}

echo "<script type='text/javascript'>window.location = 'discounters.php?result=".encrypt('failed',$encrypt)."';</script>";
exit;
?>