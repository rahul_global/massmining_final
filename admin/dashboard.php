<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
	header("location:login.php");
	exit;
}
$title = $sitename." : Dashboard";
$result = decrypt($_GET['result'],$encrypt);
$sqltut = "select _ID, concat(_Firstname,' ',_Lastname) as tutor from ".$tbname."_tutormaster where _Status = '0' ";
$rsttut = mysqli_query($con,$sqltut);

$date_all = date('Y-m-d');
$date_before_ten = date('Y-m-d', strtotime('-10 days', strtotime($date_all)));

$period = date_range($date_before_ten,$date_all);
rsort($period);
$results = array();
foreach ($period as $value) {
    $user_data = array();
    $usr_query      = "select count(*) as total_users, _Created as created from bn_users WHERE _Created LIKE '%{$value}%' AND _Usertype = '0' ";
    $usr_res        = mysqli_query($con,$usr_query);
    $usr_row        = mysqli_fetch_assoc($usr_res);
    $user_data['total_users'] = $usr_row['total_users'];
    $user_data['created'] = strtotime($usr_row['created']);
    array_push($results, $user_data);
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();

                    $("#txtcheckall").change(function() {
                        if(this.checked) {
                            $("#txtcheck").val("checkall");
                            $("#frmdis").submit();
                        }else{
                            $("#txtcheck").val("cancelall");
                            $("#frmdis").submit();
                        }
                    });
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>        </ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Bitnex CRM Dashboard</h2>
                    </div>
                    <div class="row">
                            <?php   if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> All user is now discounters.</div>
                            <?php } 
                                if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'alluser'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> All user removed.</div>
                            <?php } if(isset($result) && $result == 'nouser'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Something Went Wrong.</div>
                            <?php } 
                            ?>
                    </div>
                    <div class="row">
                        <form name="frmdis" id="frmdis" action="alldiscountacion.php">
                            <div class="form-group">
                                <input type="hidden" name="txtcheck" id="txtcheck" />
                                <?php 
                                    $selrec = "Select _ID from ".$tbname."_discounters where _Type = '1'";
                                    $rstrec = mysqli_query($con,$selrec);
                                    $num = mysqli_num_rows($rstrec);
                                ?>
                                    <label for="inputEmail3" class="col-sm-4 control-label">Check To add Discount to all users:</label>
                                    <div class="col-sm-1">
                                        <input type="checkbox" <?php if($num > 0){ ?>checked <?php } ?> class="form-control" id="txtcheckall" name="txtcheckall">
                                    </div>
                                </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Users</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($results as $value) { ?>
                                    <tr>
                                        <td><?php echo date('d-m-Y',$value['created']); ?></td>
                                        <td><?php echo $value['total_users']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
