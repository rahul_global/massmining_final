<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Withdrawals";
$result = decrypt($_GET['result'],$encrypt);

$query = "select a.* , b._Baddress,b._Usertype,b._Version from ".$tbname."_wiithdrwal a LEFT JOIN ".$tbname."_users b ON a._UserID = b._ID where b._Usertype = '0' and a._Status = '0' ORDER BY _Datetime DESC";
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Withdrawals</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Withdrawals</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } ?>
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Bitcoin Address</th>
										<th>Version</th>
										<th>Amount</th>
										<th>Status</th>
										<th>Date</th>
										<th>Edit</th> 
										<th>Remove Users</th>
									</tr>
								</thead>
								<tbody>
										<?php
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
													switch($fetch['_Version']){
														case '1' :
															$version = 'Tire1';
															break;
														case '2' :
															$version = 'Tire2';
															break;
														case '3' :
															$version = 'Tire3';
															break;
														case '4' :
															$version = 'Tire4';
															break;
														default :
															$version = 'Free Tire';
													}
												
													$selref = "SELECT Count(_ID) as refferal_count FROM `bn_referral` WHERE _UserID = '".$fetch['_UserID']."'";
													$rowref = mysqli_fetch_assoc(mysqli_query($con,$selref));
													$reffralcount = $rowref['refferal_count'];
												?>
													<tr>
														<td><?php echo $fetch['_Baddress'];?><b><?php if($reffralcount > 0){ ?> Referrals (<?php echo $reffralcount; ?>) <?php } ?></b></td>
														<td><?php echo $version; ?></td>
														<td><?php echo number_format($fetch['_Amount'] - WITH_FEES,8); ?></td>
														<td>
															<?php switch($fetch['_Status']){
																case '0' : 
																	echo 'Pending';
																	break;
																case '1' :
																	echo 'Cancelled';
																	break;
																case '2' :
																	echo 'Approved';
																	break;
																default :
																	echo '';
															} ?>
														</td>
														<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Datetime'])); ?></td>
														<!-- <td><a href="upgrade.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>">View</td> -->
														<td><a href="edit-withdrawal.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>">Edit</td> 
														<td><a href="withdrawaction.php?id=<?php echo encrypt($fetch['_UserID'],$encrypt); ?>&e_action=<?php echo encrypt('removeuser',$encrypt); ?>">Remove User</a></td>
													</tr>
											<?php   
												}
											}
											?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
