<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Our Users";
$result = decrypt($_GET['result'],$encrypt);

$query 		= "select us.* from ".$tbname."_users as us WHERE us._Usertype = '0'"; 
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                /*yukon_datatables.p_plugins_tables_datatable({'aaSorting':[]});*/
				
				
				$('#datatable_demo').DataTable({
				     	"processing": true,
						"serverSide": true,
						"ajax": "get_users.php"
					});
            }) 
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Our Users</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Our Users</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Bitcoin Address</th>
										<th>References</th>
										<th>Created</th>
									</tr>
								</thead>
								<tbody>
										<?php/*
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php echo $fetch['_Baddress']; ?></td>
														<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Created'])); ?></td>
														<?php /* ?> <td>
															
															<a href="referrals.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>">View Referrals (<?php echo $fetch['total_referral']; ?>)</a>
															<?php } ?>
														</td> <?php  ?>
													</tr>
											<?php   
												}
											} */
											?>
									
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
