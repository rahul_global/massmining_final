<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Reviews";
$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';

 $query = "SELECT * FROM ".$tbname."_reviews ORDER BY _ID";

$run   = mysqli_query($con , $query);
$num   = mysqli_num_rows($run);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
        <script type="text/javascript">
        	function apporve()
			{
				if(checkSelected('CustCheckbox', document.frmcheck.cntCheck.value) == false)
				{
					alert("Please select at least one record.");
					document.frmcheck.AllCheckbox.focus();
					return false;
				}
				else
				{
					if(confirm('Are you sure you want to approve all the Reviews?') == true)	
					{
						//document.frmcheck.e_action.value = "approve";
						document.forms.frmcheck.action = "reviewaction.php";
						document.forms.frmcheck.submit();
					}
				}
			}

			function checkSelected(msgtype, count)
			{
				for(var i=1 ; i<=count; i++)
				{
					if(eval("document.frmcheck." + msgtype + i + ".type") == "checkbox")
					{
						if(eval("document.frmcheck." + msgtype + i + ".checked") == true)
						{
							return true;
						}
					}
				}
				return false;
			}
			
			function CheckUnChecked(msgType, count, chkbxName)
			{
				if (chkbxName.checked==true)
				{
					for (var i = 1; i<=count; i++)
					{
						 eval("document.frmcheck."+msgType+i+".checked = true");
					}
				}
				else
				{
					for (var i = 1; i<=count; i++)
					{
						 eval("document.frmcheck."+msgType+i+".checked = false");
					}
				}
			}
			
			function ClearAll()
			{
				for (var i=0;i<document.Search.elements.length;i++) {
					if (document.Search.elements[i].type == "text" || document.Search.elements[i].type == "textarea")
						document.Search.elements[i].value = "";  
					else if (document.Search.elements[i].type == "select-one")
						document.Search.elements[i].selectedIndex = 0;
					else if (document.Search.elements[i].type == "checkbox")
						document.Search.elements[i].checked = false;
				}
			}	
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Reviews</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Reviews</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'dsuccess'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Reviews deleted successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
									if(isset($result) && $result == 'dfailed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php }
                            		if(isset($result) && $result == 'asuccess'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Reviews approved successfully.</div>
                            <?php } 
	                            if(isset($result) && $result == 'afailed'){ ?>
	                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Review not updated successfully.</div>
	                            <?php }
							?>
					</div>
                    <div class="row">
                        <div class="col-md-12">
                        <form name="frmcheck" method="post" action="">
                        	<div class="row" style="margin-bottom: 20px;">
                        		<input type="button" value="Approve Reviews" onclick="return apporve();" class="btn btn-default" />
                        	</div>
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th><input name="AllCheckbox" type="checkbox" value="All" onclick="CheckUnChecked('CustCheckbox',document.frmcheck.cntCheck.value,this);" /></th>
										<th>Name</th>
										<th>Rating</th>
										<th>Title</th>
										<th>Status</th>
										<th>Review Date</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
										<?php
											//$num = mysqli_num_rows($run);
											if($num > 0)
											{
												$i = 1;
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><input name="CustCheckbox<?php echo $i; ?>" type="checkbox" value="<?php echo $fetch["_ID"]; ?>" /></td>
														<td><?php echo $fetch['_Name'];?></td>
														<td><?php echo $fetch['_Rating'];?></td>
														<td><?php echo $fetch['_Title'];?></td>
														<td><?php echo ($fetch['_Status'] == "0") ? "Pending":"Approve";?></td>
														<td><?php echo date("d-m-Y h:i a", strtotime($fetch['_Created']));?></td>
														<td><a href="editreview.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('edit' , $encrypt);?>">Edit</a></td>
														<td><a onclick="return confirm('are you sure want to delete this variable?');" href="reviewaction.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('delete' , $encrypt);?>">Delete</a></td>
													</tr>
											<?php  
										
											$i++; 
												}
											}
											?>
									
								</tbody>
							</table>
									<input type="hidden" name="cntCheck" value="<?php echo $i-1; ?>" />
									<input type="hidden" name="e_action" value="<?php echo encrypt('approve',$encrypt); ?>" />
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
