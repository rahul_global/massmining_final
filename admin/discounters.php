<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Discounters";
$result = decrypt($_GET['result'],$encrypt);

$query 		= "select ds.*,us._Baddress,us._Version as userversion,ds._Version as discountversion from ".$tbname."_discounters as ds left join ".$tbname."_users as us on us._ID = ds._UserID where ds._Type = '0' ";

$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Discounters</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Discounters</h2>
                    </div>
					<div class="row">
                        <?php 	if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } 
                            	if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
                            	if(isset($result) && $result == 'nouser'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Not valid Bitcoin Address.</div>
                            <?php } ?>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<a href="edit-discounter.php?action=<?php echo encrypt("add",$encrypt); ?>" class="btn btn-primary">Add Discounter</a>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Bitcoin Address</th>
										<th>Version</th>
										<th>Discount For Version</th>
										<th>Date</th>
										<th>Delete</th> 
									</tr>
								</thead>
								<tbody>
										<?php
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 

													switch($fetch['userversion']){
														case '1' :
															$version = 'Basic';
															break;
														case '2' :
															$version = 'Tire2';
															break;
														case '3' :
															$version = 'Tire3';
															break;
														case '4' :
															$version = 'Tire4';
															break;
														default :
															$version = 'Basis';
													}
													switch($fetch['discountversion']){
														case '3' :
															$version1 = 'Tire3';
															break;
														case '4' :
															$version1 = 'Tire4';
															break;
														default :
															$version1 = 'Nano';
													}
												?>

													<tr>
														<td><?php echo $fetch['_Baddress']; ?></td>
														<td><?php echo $version; ?></td>
														<td><?php echo $version1; ?></td>
														<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Datetime'])); ?></td>
														<td><a href="discounteraction.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&action=<?php echo encrypt('delete',$encrypt); ?>">Delete</td> 
													</tr>
											<?php   
												}
											}
											?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
