<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Variables";
$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';

$query = "SELECT * FROM ".$tbname."_variables ORDER BY _ID";
$run   = mysqli_query($con , $query);
$num   = mysqli_num_rows($run);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Variables</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Variables</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'dsuccess'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> variable deleted successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
									if(isset($result) && $result == 'dfailed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php }
							?>
					</div>
					<div class="row">
						<div class="col-md-12 text-right">
							<a class="btn btn-primary" href="editvariable.php?e_action=<?php echo encrypt('add',$encrypt); ?>">Add Variable</a>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Name</th>
										<th>Value</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
										<?php
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php echo $fetch['_Name'];?></td>
														<td><?php echo $fetch['_Value'];?></td>
														<td><a href="editvariable.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('edit' , $encrypt);?>">Edit</a></td>
														<td><a onclick="return confirm('are you sure want to delete this variable?');" href="variableaction.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('delete' , $encrypt);?>">Delete</a></td>
													</tr>
											<?php   
												}
											}
											?>
									
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
