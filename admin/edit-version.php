<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$id = isset($_GET['id']) ? decrypt($_GET['id'],$encrypt) : '';

if($id == ''){
	header('location:versions.php');
	exit;
}

$title      = $sitename." : Version";

$query 		= "select * from ".$tbname."_versions WHERE _ID = ".$id;
$run	 	= mysqli_query($con,$query);
$num        = mysqli_num_rows($run);
if($num > 0)
{
	$fetch = mysqli_fetch_assoc($run);
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="versions.php">Versions</a></li>
					<li class="active">Edit Version</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Edit Version</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">							
							<form class="form-horizontal" action="versionaction.php" method="post">
								<input type="hidden" name="id" id="id" value="<?php echo encrypt($id , $encrypt); ?>" >
								<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('edit' , $encrypt); ?>" >
								<div class="form-group">
								  <label class="control-label col-sm-2" for="email">Version:</label>
								  <div class="col-sm-5">
									<input type="text" class="form-control" id="txtversion" placeholder="Enter Version" name="txtversion" value="<?php echo $fetch['_Version'];?>" readonly>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">BTC Per Minute:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtbtcpm" placeholder="Enter BTC/Min" name="txtbtcpm" value="<?php echo $fetch['_Btcpermin'];?>" required>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">BTC Per Day:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtbtcpd" placeholder="Enter BTC/Day" name="txtbtcpd" value="<?php echo $fetch['_Btcperday'];?>" required>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Bonus:</label>
								  <div class="col-sm-5">          
									<input type="text" class="form-control" id="txtbonus" placeholder="Enter Bonus" name="txtbonus" value="<?php echo $fetch['_Bonus'];?>" required>
								  </div>
								</div>
								<div class="form-group">        
								  <div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="button" onclick="location.href='versions.php';" class="btn btn-primary">Back</button>
								  </div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
