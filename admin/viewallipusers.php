<?php
SESSION_START();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
    header('location:login.php');
    exit;
}
$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
$id = isset($_GET['id']) ? decrypt($_GET['id'] , $encrypt) : '';

$selvi = "select _Ip from ".$tbname."_visits where _ID = '".$id."'";
$rowvis = mysqli_fetch_assoc(mysqli_query($con,$selvi));
$ipadd  = $rowvis['_Ip'];

$query = "Select vs.*,us._ID as userid,us._Baddress,us._Version  from ".$tbname."_visits as vs left join ".$tbname."_users as us on us._ID = vs._UserID where vs._Ip = '".$ipadd."' group by _UserID";
$run   = mysqli_query($con , $query);
$num   = mysqli_num_rows($run);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <title>IP List</title>
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
            <header id="main_header">
                <?php include 'header.php'; ?>      
            </header>
            <script type="text/javascript">
                $(document).ready(function(){
                  //   $('#myTable').DataTable();
                  //   $('#myTable1').DataTable();
                  //   $('#Clienttable').DataTable();
                });
            </script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
                    <li class="active">Users From Same Ip</li>
                </ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Users From Same Ip</h2>
                    </div>
                    <div class="row">
                        <?php   if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'nouser'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Not valid Bitcoin Address.</div>
                            <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="edit-discounter.php?action=<?php echo encrypt("add",$encrypt); ?>" class="btn btn-primary">Add Discounter</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Bitcoin Address</th>
                                        <th>Version</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if($num > 0){
                                        while($fetch = mysqli_fetch_assoc($run)){
                                            switch($fetch['_Version']){
                                                case '1' :
                                                    $version = 'Basic';
                                                    break;
                                                case '2' :
                                                    $version = 'Tire2';
                                                    break;
                                                case '3' :
                                                    $version = 'Tire3';
                                                    break;
                                                case '4' :
                                                    $version = 'Tire4';
                                                    break;
                                                default :
                                                    $version = 'Basis';
                                            }
                                    ?>
                                    <tr>
                                        <td><?php echo $fetch['userid'];?></td>
                                        <td><?php echo $fetch['_Baddress']; ?></td>
                                        <td><?php echo $version; ?></td>
                                    </tr>
                                    <?php }} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function($) {
                $('#example').DataTable({"aaSorting":[]});

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
