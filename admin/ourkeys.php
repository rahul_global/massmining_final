<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Our Keys";
$result = decrypt($_GET['result'],$encrypt);

$query 		= "select * from ".$tbname."_ourkeys";
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Our Keys</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Our Keys</h2>
						<div class="row">
							<?php if(isset($result) && $result == 'success'){ ?>
										<div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
								<?php } ?>
								<?php if(isset($result) && $result == 'failed'){ ?>
										<div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
								<?php } ?>
						</div>
						<a href='add-ourkey.php' class="btn btn-primary" style="float:right;margin-right:20px;" >Add Key</a>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Key</th>
										<th>Created</th>
										<th>View</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
										<?php
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php echo $fetch['_Key']; ?></td>
														<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Created'])); ?></td>
														<td><a href="ourkey.php?id=<?php echo encrypt($fetch['_ID'],$encrypt);?>">View</a></td>
														<td><a href="add-ourkey.php?action=edit&id=<?php echo encrypt($fetch['_ID'],$encrypt);?>">Edit</a></td>
														<td><a onclick="return confirm('are you sure want to delete?');" href="ourkeyaction.php?e_action=<?php echo encrypt('delete',$encrypt);?>&id=<?php echo encrypt($fetch['_ID'],$encrypt);?>">Delete</a></td>
													</tr>
											<?php   
												}
											}
											?>
									
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>