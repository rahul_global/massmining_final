<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Payto";
$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';

$query = "SELECT _ID,_Payto FROM ".$tbname."_users where _Payto != ''";

$run   = mysqli_query($con , $query);
$num   = mysqli_num_rows($run);

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				    $('#datatable_demo').DataTable({
				     	"processing": true,
						"serverSide": true,
						"ajax": "get_payto.php",
						"order": [[ 0, "desc" ]]
					});
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Payto</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Payto</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'usuccess'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Payto Key Changed successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
									if(isset($result) && $result == 'ufailed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php }
							?>
					</div>
					<div class="row">
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>PayTo Key</th>
										<th>Change Address</th>
									</tr>
								</thead>
								<tbody>
										<?php/*
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php echo $fetch['_Payto'];?></td>
														<td><a href="changeaddress.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('change' , $encrypt);?>">Change Address</a></td>
													</tr>
											<?php   
												}
											}*/
											?>
									
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
