<?php
SESSION_START();
include_once('../dbopen.php');
if(!isset($_SESSION['adminuserid']) || $_SESSION['adminuserid'] == ''){
	header('location:login.php');
	exit;
}

$query = "SELECT * FROM ".$tbname."_adminusers WHERE _ID = ".$_SESSION['adminuserid'];
$run   = mysqli_query($con , $query);
$fetch = mysqli_fetch_assoc($run);


$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $sitename;?></title>
		<?php include_once('topscript.php'); ?>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <?php echo include_once('topnav.php'); ?>
            <div class="content-wrapper">
                <div class="content-container">
                    <?php include_once('leftnav.php'); ?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row breadcrumb-div">
                                <div class="col-sm-6">
                                    <ul class="breadcrumb">
            							<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
            							<li class="active">My Profile</li>
            						</ul>
                                </div>
                            </div>
							<div class="row page-title-div">
                                <div class="col-sm-6">
                                    <h2 class="title">My Profile</h2>
                                </div>
                            </div>
                        
							<section class="section">
								<div class="container-fluid">
									<?php if($result != ''){ 
												if($result == 'usuccess'){
									?>
												<div class="alert alert-success">profile updated successfully</div>
									<?php 
												}
												if($result == 'ufailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'dsuccess'){
									?>
												<div class="alert alert-success">image removed successfully</div>
									<?php 
												}
												if($result == 'dfailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
									?>
									<?php } ?>
									<div class="row">
										<div class="panel-body">
											<form class="form-horizontal" method="POST" action="myprofileaction.php" enctype="multipart/form-data" onsubmit="return validate()">
												<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('edit',$encrypt);?>">
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtname" name="txtname" placeholder="Fullname" value="<?php echo $fetch['_Fullname'];?>">
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtusername" name="txtusername" placeholder="Username" value="<?php echo $fetch['_Username'];?>">
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
													<div class="col-sm-10">
														<input type="email" class="form-control" id="txtemail" name="txtemail" placeholder="Email" value="<?php echo $fetch['_Email'];?>" readonly>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
													<div class="col-sm-10">
														<input type="password" class="form-control" id="txtpassword" name="txtpassword" placeholder="Password">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Confirm Password</label>
													<div class="col-sm-10">
														<input type="password" class="form-control" id="txtcpassword" name="txtcpassword" placeholder="Confirm Password">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Image</label>
													<div class="col-sm-10">
														<?php if($fetch['_Pic'] == '' ){ ?>
															<input type="file" class="form-control" id="profilepic" name="profilepic">
														<?php } else { ?>
															<img src="uploads/<?php echo $fetch['_Pic'];?>" height="100"><a style="vertical-align:top;position:relative;top:-7px;" href="myprofileaction.php?e_action=<?php echo encrypt('delimg',$encrypt); ?>&id=<?php echo encrypt($fetch['_ID'] , $encrypt); ?>"><strong>X</strong></a>
															<input type="hidden" name="profilepic" id="profilepic" value="<?php echo $fetch['_Pic'];?>"
														<?php } ?>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10">
														<button type="submit" class="btn btn-primary" style="margin-top:20px;">Update</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</section>
						</div>
                    </div>
                    <?php include_once('rightsidebar.php'); ?>
                </div>
            </div>
        </div>
		<?php include_once('bottomscript.php'); ?>
		<script type="text/javascript">
			function validate(){
				var err = '';
				if(document.getElementById('txtname').value == ''){
					err += 'please enter name\n';
				}
				if(document.getElementById('txtusername').value == ''){
					err += 'please enter username\n';
				}
				if(document.getElementById('txtemail').value == ''){
					err += 'please enter email\n';
				}
				if(document.getElementById('profilepic').value == ''){
					err += 'please select image\n';
				}
				if(document.getElementById('txtpassword').value != '' && document.getElementById('txtcpassword').value != ''){
					if(document.getElementById('txtpassword').value != document.getElementById('txtcpassword').value){
						err += "password not matched";
					}
				}
				if(err != ''){
					alert(err);
					return false;
				}
				return true;
			}
		</script>
    </body>
</html>