<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$id = isset($_GET['id']) ? decrypt($_GET['id'],$encrypt) : '';

if($id == ''){
	header('location:discountkeys.php');
	exit;
}

$title = $sitename." : Discount Key";

$query 		= "select * from ".$tbname."_discountourkeys WHERE _ID = ".$id;
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="discountkeys.php">Discount Keys</a></li>
					<li class="active">Discount Key</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Discount Key</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">							
							<?php
							$num = mysqli_num_rows($run);
							if($num > 0)
							{
								$fetch = mysqli_fetch_assoc($run);
							?>
								<div class="col-md-1 col-sm-1"><h3>Key: </h3></div>
								<div class="col-md-11 col-sm-11"><h3><?php echo $fetch['_Key']; ?></h3></div>
								<div class="clearfix"></div>
							<?php   
							}
							?>
							<br>
							<a class="btn btn-primary" style="margin-top:20px;" href="discountkeys.php">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
