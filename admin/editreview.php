<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}

$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
$id     = (isset($_GET['id']) && $_GET['id'] != '') ? decrypt($_GET['id'] , $encrypt) : ''; 
$action = (isset($_GET['e_action']) && $_GET['e_action'] != '') ? decrypt($_GET['e_action'] , $encrypt) : ''; 


if($id != '' && $action == 'edit'){
	$query     = "SELECT * FROM ".$tbname."_reviews WHERE _ID = ".$id;
	$run       = mysqli_query($con , $query);
	$fetch     = mysqli_fetch_assoc($run);
	$breadcum  = "Edit Reviews";
	$pagetitle = "Edit Reviews";
	$btn       = 'UPDATE';
	$title      = $sitename." : Edit Reviews";
} else {
	$pagetitle = "Add Reviews";
	$breadcum  = "Add Reviews";
	$btn       = 'ADD';
	$title      = $sitename." : Add Reviews";
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="reviews.php">Reviews</a></li>
					<li class="active"><?php echo $breadcum;?></li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;"><?php echo $pagetitle ;?></h2>
                    </div>
					<?php if($result != ''){ 
												if($result == 'usuccess'){
									?>
												<div class="alert alert-success">Review updated successfully</div>
									<?php 
												}
												if($result == 'ufailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'isuccess'){
									?>
												<div class="alert alert-success">Review added successfully</div>
									<?php 
												}
												if($result == 'ifailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'failed'){
									?>
												<div class="alert alert-danger">something went wrong</div>
									<?php 
												}
									?>
									<?php } ?>
                    <div class="row">
                        <div class="col-md-12">							
							<form class="form-horizontal" method="POST" action="reviewaction.php" onsubmit="return validate()">
								<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action ,$encrypt);?>">
								<input type="hidden" name="id" id="id" value="<?php echo encrypt($id ,$encrypt);?>">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
									<div class="col-sm-10">
										<p><?php echo $fetch['_Name']; ?></p>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Rating</label>
									<div class="col-sm-10">
										<p><?php echo $fetch['_Rating']; ?></p>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Title</label>
									<div class="col-sm-10">
										<p><?php echo $fetch['_Title']; ?></p>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Description</label>
									<div class="col-sm-10">
										<p><?php echo $fetch['_Description']; ?></p>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Added Date</label>
									<div class="col-sm-10">
										<p><?php echo date("d-m-Y h:i a",strtotime($fetch['_Created'])); ?></p>
									</div>
								</div>
								<div class="form-group">
									<label for="txtstatus" class="col-sm-2 control-label">Status</label>
									<div class="col-sm-10">
										<select name="txtstatus" id="txtstatus" class="form-control">
											<option value="">-Select Status-</option>
											<option <?php if($fetch['_Status'] == "0"){ ?> Selected <?php } ?> value="0">Pending</option>
											<option <?php if($fetch['_Status'] == "1"){ ?> Selected <?php } ?> value="1">Approve</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-primary" style="margin-top:20px;"><?php echo $btn; ?></button>
									</div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
	function validate(){
		var err = '';
		if(document.getElementById('txtstatus').value == ''){
			err += 'please enter value\n';
		}
		if(err != ''){
			alert(err);
			return false;
		}
		return true;
	}
</script>