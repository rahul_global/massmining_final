<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Our Users";
$result = decrypt($_GET['result'],$encrypt);

/*Pagination Calculation module*/
$PageSize = 50;
$StartRow = 0;
//Set the page no
if (empty($_GET['PageNo'])) {
    if ($StartRow == 0) {
        $PageNo = $StartRow + 1;
    }
} else {
    $PageNo = decrypt($_GET['PageNo'],$encrypt);
    $StartRow = ($PageNo - 1) * $PageSize;
}
//Set the counter start
if ($PageNo % $PageSize == 0) {
    $CounterStart = $PageNo - ($PageSize - 1);
} else {
    $CounterStart = $PageNo - ($PageNo % $PageSize) + 1;
}
/*Pagination Calculation module end */

$CounterEnd = $CounterStart + ($PageSize - 1);
$i = 1;

$sortBy = decrypt($_REQUEST['sortBy'],$encrypt);
$sortArrange = decrypt($_REQUEST['sortArrange'],$encrypt);

/*$query 		= "select us.* , rf.total_referral from ".$tbname."_users as us LEFT JOIN (SELECT _UserID,count(*) total_referral FROM ".$tbname."_referral GROUP BY _UserID) as rf ON rf._UserID = us._ID WHERE us._Usertype = '0' "; */
$query = "select us.* ,(SELECT count(rf._ID) total_referral FROM bn_referral as rf WHERE rf._UserID = us._ID) total_referral from bn_users as us WHERE us._Usertype = '0'";

if (trim($sortBy) != "" && trim($sortArrange) != ""){
        $query2 = $query . " ORDER BY ".trim($sortBy)." ".trim($sortArrange)." LIMIT $StartRow,$PageSize ";
}else{
        $query2 = $query . " ORDER BY rf.total_referral DESC LIMIT $StartRow,$PageSize ";
}

//echo $query2;
$run	 	= mysqli_query($con,$query2);

/*$sel2 = "select count(us._ID) as totalcount from ".$tbname."_users as us LEFT JOIN (SELECT _UserID,count(*) total_referral FROM ".$tbname."_referral GROUP BY _UserID) as rf ON rf._UserID = us._ID WHERE us._Usertype = '0'";*/
//$rowcount = mysqli_fetch_assoc(mysqli_query($con,$sel2));
$trRecord = mysqli_query($con,$query); 
$totalcount = mysqli_num_rows($trRecord);
//Total of record

$RecordCount = $totalcount;                            
//Set Maximum Page
$MaxPage = $RecordCount % $PageSize;
if ($RecordCount % $PageSize == 0) {
    $MaxPage = $RecordCount / $PageSize;
} else {
    $MaxPage = ceil($RecordCount / $PageSize);
}

function getSort($title) {
    $sortBy = $_REQUEST["sortBy"];
    $sortArrange = $_REQUEST["sortArrange"];
    if ($sortBy == $title) {
        if ($sortArrange == 'ASC') {
            return 'up-sort';
        } else {
            return 'down-sort';
        }
    } else {
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                /*yukon_datatables.p_plugins_tables_datatable({'aaSorting':[]});*/
				
				$('#datatable_demo').dataTable({
					/* Disable initial sort */
					"aaSorting": []
				});
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Our Users</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Our Users</h2>
                    </div>
                    <div class="row">
                        <!-- <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Bitcoin Address</th>
										<th>Created</th>
										<th>References</th>
									</tr>
								</thead>
								<tbody>
										<?php
										/*	$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php echo $fetch['_Baddress']; ?></td>
														<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Created'])); ?></td>
														<td>
															<?php if($fetch['total_referral'] > 0){ ?>
															<a href="referrals.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>">View Referrals (<?php echo $fetch['total_referral']; ?>)</a>
															<?php } ?>
														</td>
													</tr>
											<?php   
												}
											}*/
											?>
									
								</tbody>
							</table>
                        </div> -->
                    </div>
                    <div class="row">
                    		<?php 
                    			$QureyUrl = "&amp;sortBy=" . encrypt($sortBy, $encrypt) . "&amp;sortArrange=" . encrypt($sortArrange, $encrypt) . $urlString . "";
                                if($MaxPage > 1) echo "Page: ";
                                for ($i = 1; $i <= $MaxPage; $i++) {
                                    if ($i == $PageNo) {
                                        print "<a href='?PageNo=" . encrypt($i, $encrypt) . $QureyUrl . "' class='menulink'>[" . $i . "]</a> ";
                                    } else {
                                        print "<a href='?PageNo=" . encrypt($i, $encrypt) . $QureyUrl . "' class='menulink'>" . $i . "</a> ";
                                    }
                                }
                                if (trim($sortArrange) == "DESC")
                                    $sortArrange = "ASC";
                                elseif (trim($sortArrange) == "ASC")
                                    $sortArrange = "DESC";
                                else
                                    $sortArrange = "DESC";
                                ?>

	                    		<div class="col-md-12">
								<table class="table table-striped " cellspacing="0" width="100%">
									<thead>
										<tr>
											<th><a href="?PageNo=<?= encrypt($PageNo, $encrypt) ?>&amp;sortBy=<?= encrypt('_Baddress', $encrypt) ?>&amp;sortArrange=<?= encrypt($sortArrange, $encrypt) ?><?= $urlString ?>" class="sort <?php echo getSort('_Baddress'); ?> link1">Bitcoin Address</a></th>
											<th><a href="?PageNo=<?= encrypt($PageNo, $encrypt) ?>&amp;sortBy=<?= encrypt('_Created', $encrypt) ?>&amp;sortArrange=<?= encrypt($sortArrange, $encrypt) ?><?= $urlString ?>" class="sort <?php echo getSort('_Created'); ?> link1">Created</a></th>
											<th><a href="?PageNo=<?= encrypt($PageNo, $encrypt) ?>&amp;sortBy=<?= encrypt('total_referral', $encrypt) ?>&amp;sortArrange=<?= encrypt($sortArrange, $encrypt) ?><?= $urlString ?>" class="sort <?php echo getSort('total_referral'); ?> link1">References</a></th>
										</tr>
									</thead>
									<tbody>
											<?php
												$num = mysqli_num_rows($run);
												if($num > 0)
												{
													while ($fetch = mysqli_fetch_assoc($run))
													{ 
													?>
														<tr>
															<td><?php echo $fetch['_Baddress']; ?></td>
															<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Created'])); ?></td>
															<td>
																<?php if($fetch['total_referral'] > 0){ ?>
																<a href="referrals.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>">View Referrals (<?php echo $fetch['total_referral']; ?>)</a>
																<?php } ?>
															</td>
														</tr>
												<?php  
														$i++; 
													}
												}
												?>
										
									</tbody>
								</table>
	                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
