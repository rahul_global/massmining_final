<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Referrals";
$id    = isset($_GET['id']) ? decrypt($_GET['id'],$encrypt) : '';

if($id == ''){
	header("location:ourusers.php");
	exit;
}

$query 		= "SELECT a.* , b._Baddress FROM ".$tbname."_referral a LEFT JOIN ".$tbname."_users b ON a._UserID = b._ID WHERE a._UserID = ".$id." ORDER BY a._Datetime DESC";
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
               /*yukon_datatables.p_plugins_tables_datatable();*/
			   $('#datatable_demo').dataTable({
					/* Disable initial sort */
					"aaSorting": []
				});
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="ourusers.php">Our Users</a></li>
					<li class="active">Referrals</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Referrals</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Parent Bitcoin Address</th>
										<th>IP</th>
										<th>Wallet</th>
										<th>Created</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									while($fetch = mysqli_fetch_assoc($run)){
									?>
									<tr>
										<td><?php echo $fetch['_Baddress']; ?></td>
										<td><?php echo $fetch['_Ip']; ?></td>
										<td><?php echo $fetch['_Walletadd']; ?></td>
										<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Datetime'])); ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
