<?php
SESSION_START();
include_once('../dbopen.php');
if(!isset($_SESSION['adminuserid']) || $_SESSION['adminuserid'] == ''){
	header('location:login.php');
	exit;
}

$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
$id     = (isset($_GET['id']) && $_GET['id'] != '') ? decrypt($_GET['id'] , $encrypt) : ''; 
$action = (isset($_GET['e_action']) && $_GET['e_action'] != '') ? decrypt($_GET['e_action'] , $encrypt) : ''; 

if($id == ''){
	header('location:deposits.php');
	exit;
}

if($id != '' && $action == 'edit'){
	$query     = "SELECT a.* , b._Fullname as `username` , b._ID as `userid` , b._Email as `useremail` , c._ID as `planid` , c._Name as `planname` FROM ".$tbname."_deposite a LEFT JOIN ".$tbname."_users b ON a._UID = b._ID LEFT JOIN ".$tbname."_depositeplan c on a._PID = c._ID WHERE a._ID = ".$id;
	$run       = mysqli_query($con , $query);
	$fetch     = mysqli_fetch_assoc($run);
	$breadcum  = "Edit Deposit";
	$pagetitle = "Edit Deposit";
	$btn       = 'UPDATE';
} else {
	$pagetitle = "Add Deposit";
	$breadcum  = "Add Deposit";
	$btn       = 'ADD';
}

$uquery = "SELECT * FROM ".$tbname."_users";
$runu   = mysqli_query($con , $uquery);
$numu   = mysqli_num_rows($runu);

$pquery = "SELECT * FROM ".$tbname."_depositeplan";
$runp   = mysqli_query($con , $pquery);
$nump   = mysqli_num_rows($runp);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $sitename;?></title>
		<?php include_once('topscript.php'); ?>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <?php echo include_once('topnav.php'); ?>
            <div class="content-wrapper">
                <div class="content-container">
                    <?php include_once('leftnav.php'); ?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row breadcrumb-div">
                                <div class="col-sm-6">
                                    <ul class="breadcrumb">
            							<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
										<li><a href="deposits.php"> Deposits</a></li>
            							<li class="active"><?php echo $breadcum; ?></li>
            						</ul>
                                </div>
                            </div>
							<div class="row page-title-div">
                                <div class="col-sm-6">
                                    <h2 class="title"><?php echo $pagetitle; ?></h2>
                                </div>
                            </div>
                        
							<section class="section">
								<div class="container-fluid">
									<?php if($result != ''){ 
												if($result == 'usuccess'){
									?>
												<div class="alert alert-success">deposit updated successfully</div>
									<?php 
												}
												if($result == 'ufailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'isuccess'){
									?>
												<div class="alert alert-success">deposit added successfully</div>
									<?php 
												}
												if($result == 'ifailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'failed'){
									?>
												<div class="alert alert-danger">something went wrong</div>
									<?php 
												}
									?>
									<?php } ?>
									<div class="row">
										<div class="panel-body">
											<form class="form-horizontal" method="POST" action="depositaction.php" onsubmit="return validate()">
												<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action ,$encrypt);?>">
												<input type="hidden" name="id" id="id" value="<?php echo encrypt($id ,$encrypt);?>">
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">User</label>
													<div class="col-sm-10">
														<select class="form-control" name="txtuser" id="txtuser">
															<option value="">--- Select User ---</option>
															<?php 
															if($numu > 0){
																while($fetchu = mysqli_fetch_assoc($runu)){
															?>
															<option value="<?php echo $fetchu['_ID'];?>" <?php echo (isset($fetch['userid']) && $fetch['userid'] == $fetchu['_ID']) ? 'selected="selected"' : '';?> ><?php echo $fetchu['_Fullname'];?></option>
															<?php }} ?>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Plan</label>
													<div class="col-sm-10">
														<select class="form-control" name="txtplan" id="txtplan">
															<option value="">--- Select Plan ---</option>
															<?php 
															if($nump > 0){
																while($fetchp = mysqli_fetch_assoc($runp)){
															?>
															<option value="<?php echo $fetchp['_ID'];?>" <?php echo (isset($fetch['planid']) && $fetch['planid'] == $fetchp['_ID']) ? 'selected="selected"' : '';?> ><?php echo $fetchp['_Name'];?></option>
															<?php }} ?>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Amount</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" name="txtamount" id="txtamount" PLACEHOLDER="e.g.: 0.00010000" value="<?php echo isset($fetch['_Amount']) ? $fetch['_Amount'] : '';?>">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-2 control-label">Status</label>
													<div class="col-sm-10">
														<select class="form-control" name="txtstatus" id="txtstatus">
															<option value="">---Select Status---</option>
															<option value="0" <?php echo ($fetch['_Status'] == '0') ? 'SELECTED="SELECTED"' : '';?>><?php echo $status0; ?></option>
															<option value="1" <?php echo ($fetch['_Status'] == '1') ? 'SELECTED="SELECTED"' : '';?>><?php echo $status1; ?></option>
															<option value="2" <?php echo ($fetch['_Status'] == '2') ? 'SELECTED="SELECTED"' : '';?>><?php echo $status2; ?></option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10">
														<button type="submit" class="btn btn-primary" style="margin-top:20px;"><?php echo $btn; ?></button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</section>
						</div>
                    </div>
                    <?php include_once('rightsidebar.php'); ?>
                </div>
            </div>
        </div>
		<?php include_once('bottomscript.php'); ?>
		<script type="text/javascript">
			function validate(){
				var err = '';
				if(document.getElementById('txtuser').value == ''){
					err += 'please select user\n';
				}
				if(document.getElementById('txtplan').value == ''){
					err += 'please select plan\n';
				}
				if(document.getElementById('txtamount').value == ''){
					err += 'please enter amount\n';
				}
				if(document.getElementById('txtstatus').value == ''){
					err += 'please select status\n';
				}
				if(err != ''){
					alert(err);
					return false;
				}
				return true;
			}
		</script>
    </body>
</html>