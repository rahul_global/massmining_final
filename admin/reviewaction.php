<?php
session_start();
include_once('../dbopen.php');

$loginid   = $_SESSION['uid'];
$action    = decrypt($_REQUEST['e_action'],$encrypt);
$id1 	   = str_replace(" ", "+", $_REQUEST['id']);
$id 	   = isset($id1) ? decrypt($id1 , $encrypt) : '';
$status      = isset($_POST['txtstatus']) ? replaceSpecialChar($_POST['txtstatus']) : '';

if($action == 'edit'){
	
	$query = "UPDATE ".$tbname."_reviews SET ";
	
	if($status != ''){

		$query .= " _Status = '".$status."'";

	} else {

		$query .= " _Status = null ";

	}
	
	$query .= " WHERE _ID = ".$id;

	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'editreview.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('usuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = 'reviews.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('ufailed',$encrypt)."';</script>";
	}
}

if($action == 'delete'){
	$query = "delete from ".$tbname."_reviews where _ID= ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'reviews.php?result=".encrypt('dsuccess',$encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'reviews.php?result=".encrypt('dfailed',$encrypt)."';</script>";
		exit;
	}
}
if($action == "approve"){
		$ourstring = "";
		$memberemails = array();
		$cntCheck = $_POST["cntCheck"];
		for ($i=1; $i<=$cntCheck; $i++)
		{
			if ($_POST["CustCheckbox".$i] != "")
			{
				$ourstring = $ourstring . "_ID = '" . $_POST["CustCheckbox".$i] . "' OR ";
			}
		}
		$ourstring = substr($ourstring, 0, strlen($ourstring)-4);
		
		$updstatus = "Update ".$tbname."_reviews set _Status = '1' where  (" . $ourstring . ") ";
		$rststatus = mysqli_query($con,$updstatus);

		if($rststatus){
			echo "<script type='text/javascript'>window.location = 'reviews.php?result=".encrypt('asuccess',$encrypt)."';</script>";
			exit;
		}else{
			echo "<script type='text/javascript'>window.location = 'reviews.php?result=".encrypt('asuccess',$encrypt)."';</script>";
			exit;
		}
}

echo "<script type='text/javascript'>window.location = 'reviews.php?result=".encrypt('failed',$encrypt)."';</script>";
exit;
?>