<?php
session_start();
include_once('../dbopen.php');
include 'classresizeimage.php';

$loginid   = $_SESSION['adminuserid'];
$action    = decrypt($_REQUEST['e_action'],$encrypt);
$id 	   = isset($_REQUEST['id']) ? decrypt($_REQUEST['id'] , $encrypt) : '';
$name      = isset($_POST['txtname']) ? replaceSpecialChar($_POST['txtname']) : '';
$email     = isset($_POST['txtemail']) ? replaceSpecialChar($_POST['txtemail']) : '';
$username  = isset($_POST['txtusername']) ? replaceSpecialChar($_POST['txtusername']) : '';
$password  = isset($_POST['txtpassword']) ? encrypt($_POST['txtpassword'] , $encrypt) : '';
$cpassword = isset($_POST['txtcpassword']) ? encrypt($_POST['txtcpassword'] , $encrypt) : '';
$referral  = isset($_POST['txtreferral']) ? $_POST['txtreferral'] : '';
$account   = isset($_POST['txtaccount']) ? replaceSpecialChar($_POST['txtaccount']) : '';
//$pic       = isset($_POST['txtpic']) ? $_POST['txtpic'] : $_FILES['txtpic'];

if($action == 'add'){
	$query = "INSERT INTO ".$tbname."_users (`_Fullname` , `_Username` , `_Password` , `_Email` , `_Account` , `_Referal` , `_Created` ) VALUES ( ";
	
	if($name != ''){
		$query .= "'".$name."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($username != ''){
		$query .= "'".$username."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($password != ''){
		$query .= "'".$password."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($email != ''){
		$query .= "'".$email."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($account != ''){
		$query .= "'".$account."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($referral != ''){
		$query .= " ".$referral." ,";
	} else {
		$query .= " null , ";
	}
	
	$query .= " '".$today."' )";
	if($run = mysqli_query($con , $query)){
		$lid = mysqli_insert_id($con);
		echo "<script type='text/javascript'>window.location = 'edituser.php?id=".encrypt($lid , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('isuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = 'edituser.php?e_action=".encrypt('add' , $encrypt)."&result=".encrypt('ifailed',$encrypt)."';</script>";
	}
}

if($action == 'edit'){
	$query = "UPDATE ".$tbname."_users SET ";
	
	if($name != ''){
		$query .= " _Fullname = '".$name."' ,";
	} else {
		$query .= " _Fullname = null , ";
	}
	
	if($username != ''){
		$query .= " _Username = '".$username."' ,";
	} else {
		$query .= " _Username = null , ";
	}
	
	if($password != ''){
		$query .= " _Password = '".$password."' ,";
	}
	
	if($account != ''){
		$query .= " _Account = '".$account."' ,";
	} else {
		$query .= " _Account = null , ";
	}
	
	if($referral != ''){
		$query .= " _Referal = ".$referral." ";
	} else {
		$query .= " _Referal = null ";
	}
	
	$query .= " WHERE _ID = ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'edituser.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('usuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = 'edituser.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('ufailed',$encrypt)."';</script>";
	}
}

if($action == 'delete'){
	$query = "delete from ".$tbname."_users where _ID= ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'users.php?result=".encrypt('dsuccess',$encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'users.php?result=".encrypt('dfailed',$encrypt)."';</script>";
		exit;
	}
}

echo "<script type='text/javascript'>window.location = 'users.php?result=".encrypt('failed',$encrypt)."';</script>";
exit;
?>