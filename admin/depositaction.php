<?php
session_start();
include_once('../dbopen.php');
include 'classresizeimage.php';

$loginid   = $_SESSION['adminuserid'];
$action    = decrypt($_REQUEST['e_action'],$encrypt);
$id 	   = isset($_REQUEST['id']) ? decrypt($_REQUEST['id'] , $encrypt) : '';
$user      = isset($_POST['txtuser']) ? $_POST['txtuser'] : '';
$plan      = isset($_POST['txtplan']) ? $_POST['txtplan'] : '';
$amount    = isset($_POST['txtamount']) ? number_format(replaceSpecialChar($_POST['txtamount']) , 8) : '';
$status    = isset($_POST['txtstatus']) ? $_POST['txtstatus'] : '';

if($action == 'add'){
	$query = "INSERT INTO ".$tbname."_deposite (`_UID` , `_PID` , `_Amount` , `_Status` , `_Ref` , `_Created` ) VALUES ( ";
	
	if($user != ''){
		$query .= "'".$user."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($plan != ''){
		$query .= "'".$plan."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($amount != ''){
		$query .= "'".$amount."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($status != ''){
		$query .= "'".$status."' ,";
	} else {
		$query .= " null , ";
	}
	
	$query .= " '0' , '".$today."' )";
	if($run = mysqli_query($con , $query)){
		$lid = mysqli_insert_id($con);
		echo "<script type='text/javascript'>window.location = 'editdeposit.php?id=".encrypt($lid , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('isuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = 'editdeposit.php?e_action=".encrypt('add' , $encrypt)."&result=".encrypt('ifailed',$encrypt)."';</script>";
	}
}

if($action == 'edit'){
	$query = "UPDATE ".$tbname."_deposite SET ";
	
	if($user != ''){
		$query .= " _UID = '".$user."' ,";
	} else {
		$query .= " _UID = null , ";
	}
	
	if($plan != ''){
		$query .= " _PID = '".$plan."' ,";
	} else {
		$query .= " _PID = null , ";
	}
	
	if($amount != ''){
		$query .= " _Amount = '".$amount."' ,";
	} else {
		$query .= " _Amount = null , ";
	}
	
	if($status != ''){
		$query .= " _Status = '".$status."' , ";
	} else {
		$query .= " _Status = null ,";
	}
	
	$query .= " _Ref = '0' WHERE _ID = ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'editdeposit.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('usuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = 'editdeposit.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('ufailed',$encrypt)."';</script>";
	}
}

if($action == 'delete'){
	$query = "delete from ".$tbname."_deposite where _ID= ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'deposits.php?result=".encrypt('dsuccess',$encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'deposits.php?result=".encrypt('dfailed',$encrypt)."';</script>";
		exit;
	}
}

echo "<script type='text/javascript'>window.location = 'deposits.php?result=".encrypt('failed',$encrypt)."';</script>";
exit;
?>