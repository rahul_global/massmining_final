<nav class="navbar top-navbar bg-white box-shadow">
	<div class="container-fluid">
		<div class="row">
			<div class="navbar-header no-padding">
				<a class="navbar-brand" href="index.html">
					<div><?php echo $sitename;?></div>
				</a>
				<span class="small-nav-handle hidden-sm hidden-xs"><i class="fa fa-outdent"></i></span>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-ellipsis-v"></i>
				</button>
				<button type="button" class="navbar-toggle mobile-nav-toggle" >
					<i class="fa fa-bars"></i>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse-1">
				<ul class="nav navbar-nav" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
					<li class="hidden-sm hidden-xs"><a href="#" class="user-info-handle"><i class="fa fa-user"></i></a></li>
					<li class="hidden-sm hidden-xs"><a href="#" class="full-screen-handle"><i class="fa fa-arrows-alt"></i></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
					<!--<li class="dropdown">
						<a href="#" class="dropdown-toggle bg-primary tour-one" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus-circle"></i> Add New <span class="caret"></span></a>
						<ul class="dropdown-menu" >
							<li><a href="#"><i class="fa fa-plus-square-o"></i> Customer</a></li>
							<li><a href="#"><i class="fa fa-plus-square-o"></i> Employee</a></li>
							<li><a href="#"><i class="fa fa-plus-square-o"></i> Estimate</a></li>
							<li><a href="#"><i class="fa fa-plus-square-o"></i> Task</a></li>
							<li><a href="#"><i class="fa fa-plus-square-o"></i> Team Member</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Create Order</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Generate Report</a></li>
						</ul>
					</li>-->
					<!-- /.dropdown -->
					<!--<li><a href="#" class=""><i class="fa fa-bell"></i><span class="badge badge-danger">6</span></a></li>
					<li><a href="#" class=""><i class="fa fa-comments"></i><span class="badge badge-warning">2</span></a></li>-->
					<li class="dropdown tour-two" id="header_dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['adminfullname'];?><span class="caret"></span></a>
						<ul class="dropdown-menu profile-dropdown">
							<li class="profile-menu bg-gray">
								<div class="">
									<!--<img src="http://placehold.it/60/c2c2c2?text=User" alt="John Doe" class="img-circle profile-img">-->
									<img src="uploads/<?php echo $_SESSION['adminuserdata']['_Pic'];?>" alt="admin" class="img-circle profile-img">
									<div class="profile-name">
										<h6><?php echo $_SESSION['adminfullname'];?></h6>
										<a href="myprofile.php">View Profile</a>
									</div>
									<div class="clearfix"></div>
								</div>
							</li>
							<li role="separator" class="divider"></li>
							<li><a href="logout.php" class="color-danger text-center"><i class="fa fa-sign-out"></i>Logout</a></li>
						</ul>
					</li>
					<li><a href="#" class="hidden-xs hidden-sm open-right-sidebar"><i class="fa fa-ellipsis-v"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</nav>