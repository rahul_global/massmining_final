<?php 
//session_start();
session_start();
include_once('../dbopen.php');
/*echo "<pre>";
print_r($_FILES);
print_r($_REQUEST);exit;*/

$action = decrypt($_REQUEST['e_action'] , $encrypt);
$id = decrypt($_REQUEST['id'] , $encrypt);
$uploadpath = "uploads/member/";
$firstname 		= replaceSpecialChar($_POST['txtfname']); 
$lastname	 	= replaceSpecialChar($_POST['txtlname']);
$email 			= replaceSpecialChar($_POST['txtemail']);
$biography 		= replaceSpecialChar($_POST['txtbio']);


if($action == "add"){

	$filename = '';
	if(isset($_FILES['txtprofile']['name']) && $_FILES['txtprofile']['name'] != '')
	{
		$ext = explode('.',$_FILES['txtprofile']['name']);
		$filename = $_SESSION['userid'].'.'.$ext[1];
		move_uploaded_file($_FILES['txtprofile']['tmp_name'] , $uploadpath.$filename);
	}

	$str = "INSERT INTO " . $tbname . "_teammember (";
     
     	$str .= " _Firstname,_Lastname,_Email,_Biography,_Profile) VALUES(";
     
        if ($firstname != "")
            $str = $str . "'" . $firstname . "', ";
        else
            $str = $str . "null, ";

          if ($lastname != "")
              $str = $str . "'" . $lastname . "', ";
          else
              $str = $str . "null, ";
          if ($email != "")
              $str = $str . "'" . $email . "', ";
          else
              $str = $str . "null, ";

          if ($biography != "")
              $str = $str . "'" . $biography . "', ";
          else
              $str = $str . "null, ";

          if ($filename != "")
              $str = $str . "'" . $filename . "' ";
          else
              $str = $str . "null ";
        
          $str = $str . ") ";

          $rstins = mysqli_query($con,$str);

  	if($rstins){
		header("location:allteam.php?done=".encrypt("00",$encrypt));
		exit;
	}else{
		header("location:allteam.php?done=".encrypt("01",$encrypt));
		exit;	
	}

}else if($action == "edit"){

	$filename = '';
	if(isset($_FILES['txtprofile']['name']) && $_FILES['txtprofile']['name'] != '')
	{
		$ext = explode('.',$_FILES['txtprofile']['name']);
		$filename = $_SESSION['userid'].'.'.$ext[1];
		move_uploaded_file($_FILES['txtprofile']['tmp_name'] , $uploadpath.$filename);
	}else{
		$filename = $_REQUEST['txtprofile'];
	}

	$str = "UPDATE " . $tbname . "_teammember SET ";
	    
	    if ($firstname != "")
			$str = $str . "_Firstname = '" . $firstname . "', ";
		else
	        $str = $str . "_Firstname = null, ";
	
		if ($lastname != "")
	        $str = $str . "_Lastname = '" . $lastname . "', ";
	    else
			$str = $str . "_Lastname = null, ";
		
		if ($email != "")
	        $str = $str . "_Email = '" . $email . "', ";
	    else
	        $str = $str . "_Email = null, ";
         
		if ($biography != "")
	        $str = $str . "_Biography = '" . $biography . "', ";
	    else
	        $str = $str . "_Biography = null ";

	    if ($filename != "")
            $str = $str . "_Profile = '" . $filename . "' ";
        else
            $str = $str . "_Profile = null ";


	    $str = $str . " WHERE _ID = '" . $id . "' ";
		
		//echo $str;exit;		 
	
	$rst = mysqli_query($con,$str);

	if($rst){
		header("location:allteam.php?done=".encrypt("11",$encrypt));
		exit;
	}else{
		header("location:allteam.php?done=".encrypt("1",$encrypt));
		exit;	
	}



}else if($action == "deletepic"){

	//echo $orgtype;exit;
	$selimg = "select _Profile from ".$tbname."_teammember where _ID='".$id."'";
	$rowimg = mysqli_fetch_assoc(mysqli_query($con,$selimg));
	$background = $rowimg['_Profile'];

	
	if (file_exists($uploadpath . $background)) {
	    unlink($uploadpath . $background);
		$delclient = "update ".$tbname."_teammember set _Profile = '' where _ID = '".$id."'";
		$rstdel = mysqli_query($con,$delclient);
	}


	if($rstdel){
		
		header("location:edit-team.php?id=".encrypt($id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
		exit;
	}else{
		header("location:edit-team.php?id=".encrypt($id,$encrypt)."&e_action=".encrypt('edit',$encrypt));
		exit;	
	}

}else{

	if($action == "delete"){

		$delclient = "delete from ".$tbname."_teammember where _ID = '".$id."'";
		$rstdel = mysqli_query($con,$delclient); 


		if($rstdel){
			
			header("location:allteam.php?done=".encrypt("22",$encrypt));
			exit;
		}else{
			header("location:allteam.php?done=".encrypt("2",$encrypt));
			exit;	
		}

	}else{
		header("location:allteam.php?done=".encrypt("23",$encrypt));
	}

}


?>