<?php 
session_start();
include_once('../dbopen.php');

unset($_SESSION['uid']);
unset($_SESSION['uname']);
unset($_SESSION['udata']);
//session_destroy();

header("location:login.php");
exit;
?>