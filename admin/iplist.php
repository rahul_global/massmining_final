<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
    header("location:login.php");
    exit;
}
$title = $sitename." : Ip Addresses";
$result = decrypt($_GET['result'],$encrypt);

$query = "Select _ID,_Ip from ".$tbname."_visits group by _Ip";
$run   = mysqli_query($con , $query);
$num   = mysqli_num_rows($run);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
            <header id="main_header">
                <?php include 'header.php'; ?>      
            </header>
            <script type="text/javascript">
                $(document).ready(function(){
                  //   $('#myTable').DataTable();
                  //   $('#myTable1').DataTable();
                  //   $('#Clienttable').DataTable();

                  $('#example').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": "get_ip.php"
                    });

                });
            </script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
                    <li class="active">Ip Addresses</li>
                </ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Ip Addresses</h2>
                    </div>
                    <div class="row">
                        <?php   if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'nouser'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Not valid Bitcoin Address.</div>
                            <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>IP Address</th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php /* 
                                    if($num > 0){
                                        while($fetch = mysqli_fetch_assoc($run)){
                                    ?>
                                    <tr>
                                        <td><?php echo $fetch['_Ip'];?></td>
                                        <td><a href="viewallipusers.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt);?>&e_action=<?php echo encrypt('edit' , $encrypt);?>">Edit</a></td>
                                    </tr>
                                    <?php }}  */?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
