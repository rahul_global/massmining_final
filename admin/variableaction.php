<?php
session_start();
include_once('../dbopen.php');

$loginid   = $_SESSION['uid'];
$action    = decrypt($_REQUEST['e_action'],$encrypt);
$id1 	   = str_replace(" ", "+", $_REQUEST['id']);
$id 	   = isset($id1) ? decrypt($id1 , $encrypt) : '';
$name      = isset($_POST['txtname']) ? replaceSpecialChar($_POST['txtname']) : '';
$value     = isset($_POST['txtvalue']) ? replaceSpecialChar($_POST['txtvalue']) : '';

if($action == 'add'){
	$query = "INSERT INTO ".$tbname."_variables (`_Name` , `_Value`) VALUES ( ";
	
	if($name != ''){
		$query .= "'".$name."' ,";
	} else {
		$query .= " null , ";
	}
	
	if($value != ''){
		$query .= "'".$value."' ";
	} else {
		$query .= " null ";
	}
	
	$query .= " ) ";
	if($run = mysqli_query($con , $query)){
		$lid = mysqli_insert_id($con);
		echo "<script type='text/javascript'>window.location = 'editvariable.php?id=".encrypt($lid , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('isuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = 'editvariable.php?e_action=".encrypt('add' , $encrypt)."&result=".encrypt('ifailed',$encrypt)."';</script>";
	}
}

if($action == 'edit'){
	$query = "UPDATE ".$tbname."_variables SET ";
	
	if($name != ''){
		$query .= " _Name = '".$name."' ,";
	} else {
		$query .= " _Name = null , ";
	}
	
	if($value != ''){
		$query .= " _Value = '".$value."'";
	} else {
		$query .= " _Value = null ";
	}
	
	$query .= " WHERE _ID = ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'editvariable.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('usuccess',$encrypt)."';</script>";
	} else {
		echo "<script type='text/javascript'>window.location = '_variables.php?id=".encrypt($id , $encrypt)."&e_action=".encrypt('edit' , $encrypt)."&result=".encrypt('ufailed',$encrypt)."';</script>";
	}
}

if($action == 'delete'){
	$query = "delete from ".$tbname."_variables where _ID= ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'variables.php?result=".encrypt('dsuccess',$encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'variables.php?result=".encrypt('dfailed',$encrypt)."';</script>";
		exit;
	}
}

echo "<script type='text/javascript'>window.location = 'variables.php?result=".encrypt('failed',$encrypt)."';</script>";
exit;
?>