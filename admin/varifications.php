<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Verifications";
$result = decrypt($_GET['result'],$encrypt);

$query 		= "select us.*,vf._Name as username from ".$tbname."_users as us left join ".$tbname."_verifications as vf on vf._UserID = us._ID Where us._Verified IN ('1','2') ";
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  	$('.txtverified').change(function (){
						$(this).parent("form").submit();
					});
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Varifications</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Varifications</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } ?>
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Name</th>
										<th>Bitcoin Address</th>
										<th>Version</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
										<?php
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php 
														if($fetch['username'] != "") 
														{ 
															echo $fetch['username']; 
														} 
														
														?></td>
														<td><?php echo $fetch['_Baddress']; ?></td>
														<td><?php 
																switch($fetch['_Version']){
																	case '1' :
																		$version = 'Basic';
																		break;
																	case '2' :
																		$version = 'Tire2';
																		break;
																	case '3' :
																		$version = 'Tire3';
																		break;
																	case '4' :
																		$version = 'Tire4';
																		break;
																	default :
																		$version = 'Basis';
																}
														echo $version; 

														?></td>
														<td>
															<?php if($fetch['_Verified'] == '0' ){ ?>
															Not Verified
															<?php } else if($fetch['_Verified'] == '1'){ ?>
															Pending
															<?php } else if($fetch['_Verified'] == '2'){ ?>
															Verified
															<?php } ?>
														</td>
														<td><a href="edit-verification.php?id=<?php echo encrypt($fetch['_ID'] , $encrypt); ?>">Edit</a></td>
													</tr>
											<?php   
												}
											}
											?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
