<?php
SESSION_START();
include_once('../dbopen.php');
if(!isset($_SESSION['adminuserid']) || $_SESSION['adminuserid'] == ''){
	header('location:login.php');
	exit;
}

$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
$id     = (isset($_GET['id']) && $_GET['id'] != '') ? decrypt($_GET['id'] , $encrypt) : ''; 
$action = (isset($_GET['e_action']) && $_GET['e_action'] != '') ? decrypt($_GET['e_action'] , $encrypt) : ''; 

if($id != '' && $action == 'edit'){
	$query     = "SELECT * FROM ".$tbname."_variables WHERE _ID = ".$id;
	$run       = mysqli_query($con , $query);
	$fetch     = mysqli_fetch_assoc($run);
	$breadcum  = "Edit Variable";
	$pagetitle = "Edit Variable";
	$btn       = 'UPDATE';
} else {
	$pagetitle = "Add Variable";
	$breadcum  = "Add Variable";
	$btn       = 'ADD';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $sitename;?></title>
		<?php include_once('topscript.php'); ?>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">
            <?php echo include_once('topnav.php'); ?>
            <div class="content-wrapper">
                <div class="content-container">
                    <?php include_once('leftnav.php'); ?>
                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row breadcrumb-div">
                                <div class="col-sm-6">
                                    <ul class="breadcrumb">
            							<li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
										<li><a href="variables.php"> Variables</a></li>
            							<li class="active"><?php echo $breadcum; ?></li>
            						</ul>
                                </div>
                            </div>
							<div class="row page-title-div">
                                <div class="col-sm-6">
                                    <h2 class="title"><?php echo $pagetitle; ?></h2>
                                </div>
                            </div>
                        
							<section class="section">
								<div class="container-fluid">
									<?php if($result != ''){ 
												if($result == 'usuccess'){
									?>
												<div class="alert alert-success">variable updated successfully</div>
									<?php 
												}
												if($result == 'ufailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'isuccess'){
									?>
												<div class="alert alert-success">variable added successfully</div>
									<?php 
												}
												if($result == 'ifailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'failed'){
									?>
												<div class="alert alert-danger">something went wrong</div>
									<?php 
												}
									?>
									<?php } ?>
									<div class="row">
										<div class="panel-body">
											<form class="form-horizontal" method="POST" action="variableaction.php" onsubmit="return validate()">
												<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt($action ,$encrypt);?>">
												<input type="hidden" name="id" id="id" value="<?php echo encrypt($id ,$encrypt);?>">
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtname" name="txtname" placeholder="Name" value="<?php echo isset($fetch['_Name']) ? $fetch['_Name'] : '';?>">
													</div>
												</div>
												<div class="form-group">
													<label for="inputEmail3" class="col-sm-2 control-label">Value</label>
													<div class="col-sm-10">
														<input type="text" class="form-control" id="txtvalue" name="txtvalue" placeholder="Value" value="<?php echo isset($fetch['_Value']) ? $fetch['_Value'] : '';?>">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10">
														<button type="submit" class="btn btn-primary" style="margin-top:20px;"><?php echo $btn; ?></button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</section>
						</div>
                    </div>
                    <?php include_once('rightsidebar.php'); ?>
                </div>
            </div>
        </div>
		<?php include_once('bottomscript.php'); ?>
		<script type="text/javascript">
			function validate(){
				var err = '';
				if(document.getElementById('txtname').value == ''){
					err += 'please enter name\n';
				}
				if(document.getElementById('txtvalue').value == ''){
					err += 'please enter value\n';
				}
				if(err != ''){
					alert(err);
					return false;
				}
				return true;
			}
		</script>
    </body>
</html>