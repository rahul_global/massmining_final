<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$id = isset($_GET['id']) ? decrypt($_GET['id'],$encrypt) : '';

if($id == ''){
	header('location:versions.php');
	exit;
}

$title = $sitename." : Version";

$query 		= "select * from ".$tbname."_versions WHERE _ID = ".$id;
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="versions.php">Versions</a></li>
					<li class="active">Version</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Version</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">							
							<?php
							$num = mysqli_num_rows($run);
							if($num > 0)
							{
								$fetch = mysqli_fetch_assoc($run);
							?>
								<div class="col-md-3 col-sm-3"><h3>Version: </h3></div>
								<div class="col-md-9 col-sm-9"><h3><?php echo $fetch['_Version']; ?></h3></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h3>BTC Per Minute: </h3></div>
								<div class="col-md-9 col-sm-9"><h3><?php echo $fetch['_Btcpermin']; ?></h3></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h3>BTC Per Day: </h3></div>
								<div class="col-md-9 col-sm-9"><h3><?php echo $fetch['_Btcperday']; ?></h3></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h3>Bonus: </h3></div>
								<div class="col-md-9 col-sm-9"><h3><?php echo $fetch['_Bonus']; ?></h3></div>
							<?php   
							}
							?>
							<br>
							<a class="btn btn-primary" style="margin-top:20px;" href="versions.php">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
