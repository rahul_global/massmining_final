<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$id1 = str_replace(" ", "+", $_GET['id']);
$id = isset($id1) ? decrypt($id1,$encrypt) : '';

if($id == ''){
	header('location:varifications.php');
	exit;
}
$title      = $sitename." : Varification";

$query 		= "select us.*,vf._Name as username from ".$tbname."_users as us left join ".$tbname."_verifications as vf on vf._UserID = us._ID Where us._ID = ".$id;

$run	 	= mysqli_query($con,$query);
$num        = mysqli_num_rows($run);
if($num > 0)
{
	$fetch = mysqli_fetch_assoc($run);
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="varifications.php">Verifications</a></li>
					<li class="active">Edit Verification</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Edit Verification</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">							
							<form class="form-horizontal" action="changeaction.php" method="post">
								<input type="hidden" name="userid" id="userid" value="<?php echo encrypt($id , $encrypt); ?>" >
								<input type="hidden" name="e_action" id="e_action" value="<?php echo encrypt('edit' , $encrypt); ?>" >
								<div class="form-group">
								  <label class="control-label col-sm-2" for="email">Name:</label>
								  <div class="col-sm-5">
									<?php echo $fetch['username']; ?>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Bitcoin Address:</label>
								  <div class="col-sm-5">          
									<?php echo $fetch['_Baddress']; ?>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Version:</label>
								  <div class="col-sm-5">          
									<?php switch($fetch['_Version']){
												case '1' :
													$version = 'Basic';
													break;
												case '2' :
													$version = 'Tire2';
													break;
												case '3' :
													$version = 'Tire3';
													break;
												case '4' :
													$version = 'Tire4';
													break;
												default :
													$version = 'Basis';
											}
									echo $version;  ?>
								  </div>
								</div>
								<div class="form-group">
								  <label class="control-label col-sm-2" for="pwd">Status:</label>
								  <div class="col-sm-5">          
									<select name="txtverified" class="txtverified form-control">
										<option <?php if($fetch['_Verified'] == '0'){ ?> selected <?php } ?> value="0">Not Verified</option>
										<option <?php if($fetch['_Verified'] == '1'){ ?> selected <?php } ?> value="1">Pending</option>
										<option <?php if($fetch['_Verified'] == '2'){ ?> selected <?php } ?> value="2">Verified</option>
									</select>
								  </div>
								</div>
								<div class="form-group">        
								  <div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="button" onclick="location.href='varifications.php';" class="btn btn-primary">Back</button>
								  </div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
