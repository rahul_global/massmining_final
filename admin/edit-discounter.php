<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}

$result = isset($_GET['result']) ? decrypt($_GET['result'] , $encrypt) : '';
$id     = (isset($_GET['id']) && $_GET['id'] != '') ? decrypt($_GET['id'] , $encrypt) : ''; 
$action = (isset($_GET['action']) && $_GET['action'] != '') ? decrypt($_GET['action'] , $encrypt) : ''; 


if($id != '' && $action == 'edit'){
	$query     = "SELECT * FROM ".$tbname."_variables WHERE _ID = ".$id;
	$run       = mysqli_query($con , $query);
	$fetch     = mysqli_fetch_assoc($run);
	$breadcum  = "Edit Discounter";
	$pagetitle = "Edit Discounter";
	$btn       = 'UPDATE';
	$title      = $sitename." : Edit Discounter";
} else {
	$pagetitle = "Add Discounter";
	$breadcum  = "Add Discounter";
	$btn       = 'ADD';
	$title      = $sitename." : Add Discounter";
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="variables.php">Discounter</a></li>
					<li class="active"><?php echo $breadcum;?></li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;"><?php echo $pagetitle ;?></h2>
                    </div>
					<?php if($result != ''){ 
												if($result == 'usuccess'){
									?>
												<div class="alert alert-success">Discounter updated successfully</div>
									<?php 
												}
												if($result == 'ufailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'isuccess'){
									?>
												<div class="alert alert-success">Discounter added successfully</div>
									<?php 
												}
												if($result == 'ifailed'){
									?>
												<div class="alert alert-danger">error occurred</div>
									<?php 
												}
												if($result == 'failed'){
									?>
												<div class="alert alert-danger">something went wrong</div>
									<?php 
												}
									?>
									<?php } ?>
                    <div class="row">
                        <div class="col-md-12">							
							<form class="form-horizontal" method="POST" action="discounteraction.php" onsubmit="return validate()">
								<input type="hidden" name="action" id="action" value="<?php echo encrypt($action ,$encrypt);?>">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Discounter ID</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="txtbaddress" name="txtbaddress" placeholder="Enter Bitcoin Address" required>
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Select Version</label>
									<div class="col-sm-10">
										<select name="txtversion" class="form-control" required>
											<option value=""> -Select Version- </option>
											<option value="3"> Tire3 </option>
											<option value="4"> Tire4 </option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-primary" style="margin-top:20px;"><?php echo $btn; ?></button>
									</div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
	function validate(){
		var err = '';
		if(document.getElementById('txtbaddress').value == ''){
			err += 'please enter name\n';
		}
		
		if(err != ''){
			alert(err);
			return false;
		}
		return true;
	}
</script>