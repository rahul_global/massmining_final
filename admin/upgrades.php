<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Versions";
$result = decrypt($_GET['result'],$encrypt);

$query 		= "select a.* , b._Baddress,b._Usertype from ".$tbname."_upgrades a LEFT JOIN ".$tbname."_users b ON a._UserID = b._ID where b._Usertype = '0' or b._Usertype = '1'";

$run= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                //yukon_datatables.p_plugins_tables_datatable();

                $('#datatable_demo').DataTable({
					"bProcessing": true,
			        "serverSide": true,
			        "order": [[ 4, "desc" ]],
			         "ajax":{
			            url :"get_upgrades.php",
			            type: "post",

			            error: function(){
			              $("#datatable_demo").css("display","none");
			            }
			          },
                    "columns": [
				        { "data": "baddress" },
				        { "data": "version" },
				        { "data": "price" },
				        { "data": "status" },
				        { "data": "date_time" },
				        { "data": "edit_url" },
				    ],
				    "columnDefs": [
				        {
				            "targets": 5,
				            "render": function ( data, edit_url, row ) {
				                return '<a href="'+data+'">Edit</a>';
				            }
				        }
				    ]
				});
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Upgrades</li>
				</ul>
            </nav>
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Upgrades</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } ?>
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Bitcoin Address</th>
										<th>Version</th>
										<th>Price</th>
										<th>Status</th>
										<th>Date</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody>										
								</tbody>
								<tfoot>
									<tr>
										<th>Bitcoin Address</th>
										<th>Version</th>
										<th>Price</th>
										<th>Status</th>
										<th>Date</th>
										<th>Edit</th>
									</tr>
								</tfoot>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
