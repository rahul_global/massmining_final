<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$title = $sitename." : Withdrawals";
$result = decrypt($_GET['result'],$encrypt);

$query 		= "select a.* , b._Baddress,b._Usertype from ".$tbname."_wiithdrwal a LEFT JOIN ".$tbname."_users b ON a._UserID = b._ID where b._Usertype = '0' and a._Status != '0'";
$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
        <script>
            $(function() {
                // footable
                yukon_datatables.p_plugins_tables_datatable();
            })
        </script>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();
				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li class="active">Withdrawals</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Approved/Cancle Withdrawals</h2>
                    </div>
					<div class="row">
                        <?php if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Action Performed Successfully.</div>
                            <?php } ?>
                            <?php if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } ?>
					</div>
                    <div class="row">
                        <div class="col-md-12">
							<table id="datatable_demo" class="table table-striped " cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Bitcoin Address</th>
										<th>Withdrawal-id</th>
										<th>Amount</th>
										<th>Status</th>
										<th>Date</th>
										<!-- <th>View</th>-->
										<th>Edit</th> 
									</tr>
								</thead>
								<tbody>
										<?php
											$num = mysqli_num_rows($run);
											if($num > 0)
											{
												while ($fetch = mysqli_fetch_assoc($run))
												{ 
												?>
													<tr>
														<td><?php echo $fetch['_Baddress']; ?></td>
														<td><?php echo $fetch['_WithID']; ?></td>
														<td><?php echo $fetch['_Amount']; ?></td>
														<td>
															<?php switch($fetch['_Status']){
																case '0' : 
																	echo 'Pending';
																	break;
																case '1' :
																	echo 'Cancelled';
																	break;
																case '2' :
																	echo 'Approved';
																	break;
																default :
																	echo '';
															} ?>
														</td>
														<td><?php echo date('d-M-Y H:i:s',strtotime($fetch['_Datetime'])); ?></td>
														<!-- <td><a href="upgrade.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>">View</td> -->
														<td><a href="edit-withdrawal.php?id=<?php echo encrypt($fetch['_ID'],$encrypt); ?>&e_action=<?php echo encrypt('edit',$encrypt); ?>">Edit</td> 
													</tr>
											<?php   
												}
											}
											?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
