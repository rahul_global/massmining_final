<?php
include '../dbopen.php';
$table = 'bn_upgrades';

$requestData= $_REQUEST;

$columns = array( 
// datatable column index  => database column name
	0 =>'b._Baddress', 
	1 => '_Version',
	2=> '_Price',
	3=> '_Status',
	4=> '_Datetime',
);


$sql = "select a.* , b._Baddress,b._Usertype from ".$tbname."_upgrades a LEFT JOIN ".$tbname."_users b ON a._UserID = b._ID where b._Usertype = '0' or b._Usertype = '1'";

$query=mysqli_query($con, $sql) or die("not found: get upgrades");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;


$sql = "select a.* , b._Baddress,b._Usertype from ".$tbname."_upgrades a LEFT JOIN ".$tbname."_users b ON a._UserID = b._ID where ( b._Usertype = '0' or b._Usertype = '1' ) ";

if( !empty($requestData['search']['value']) ) { 
	$sql.=" AND ( b._Baddress = '".$requestData['search']['value']."' ";    
	$sql.=" OR a._Payto = '".$requestData['search']['value']."' ) ";
}
//echo $sql;
$query=mysqli_query($con, $sql) or die("not found: get upgrades");
$totalFiltered = mysqli_num_rows($query);

$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
$query=mysqli_query($con, $sql) or die("not found: get upgrades");


$data = array();
while( $row=mysqli_fetch_array($query) ) {
	$nestedData=array(); 

	$nestedData['baddress'] = $row["_Baddress"];
	$nestedData['version'] = $row["_Version"];
	$nestedData['price'] = $row["_Price"];
	$nestedData['status'] = $row["_Status"];
	$nestedData['date_time'] = date('d-m-Y h:i:s', strtotime($row["_Datetime"]));
	$nestedData['edit_url'] = "edit-upgrade.php?id=".encrypt($row['_ID'],$encrypt)."&e_action=".encrypt('edit',$encrypt)."";
	$data[] = $nestedData;
}

$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data,
			);

echo json_encode($json_data);