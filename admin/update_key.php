<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
	header("location:login.php");
	exit;
}

$title = $sitename." : Dashboard";
$result = decrypt($_GET['result'],$encrypt);
$sqltut = "select _ID, concat(_Firstname,' ',_Lastname) as tutor from ".$tbname."_tutormaster where _Status = '0' ";
/*echo $sqltut;
exit;*/
$rsttut = mysqli_query($con,$sqltut);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();

				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>        </ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Bitnex CRM Dashboard</h2>
                    </div>
                    <div class="row">
                            <?php   if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Key Updated.</div>
                            <?php } 
                                if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'alluser'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> All user removed.</div>
                            <?php } if(isset($result) && $result == 'nouser'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Something Went Wrong.</div>
                            <?php } 
                            ?>
                    </div>
                    <?php 
                    $selall = "Select _Key from ".$tbname."_statickey WHERE _ID = 1";
                   /* echo $selall;
                    exit;*/

                    $rstall = mysqli_query($con,$selall);
                    $row = mysqli_fetch_assoc($rstall);

                    $key = $row['_Key'] ? $row['_Key'] : '';
                    ?>
                    <div class="row">
                        <form name="frmdis" method="POST" id="frmdis" action="keyaction.php" class="form-horizontal">
                            <div class="form-group">
                                <input type="hidden" name="action" id="action" value="update_key" />
                                <label for="inputEmail3" class="col-sm-2 control-label">Key: </label>
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" value="<?php echo $key;?>" id="txtkey" name="txtkey">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label"> </label>
                                <div class="col-sm-10">
                                    <input type="submit" required class="btn btn-default" id="submit" name="submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
