<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == ''){
	header("location:login.php");
	exit;
}

$result = decrypt($_GET['result'],$encrypt);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
			<script type="text/javascript">
				$(document).ready(function(){
				  //   $('#myTable').DataTable();
				  //   $('#myTable1').DataTable();
				  //   $('#Clienttable').DataTable();

				});
			</script>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>        </ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Withdraw Limits</h2>
                    </div>
                    <div class="row">
                            <?php   if(isset($result) && $result == 'success'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> Withdraw limits Updated.</div>
                            <?php } 
                                if(isset($result) && $result == 'failed'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Action not Performed Successfully.</div>
                            <?php } 
                                if(isset($result) && $result == 'alluser'){ ?>
                                    <div role="alert" class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>Success!</strong> All user removed.</div>
                            <?php } if(isset($result) && $result == 'nouser'){ ?>
                                    <div role="alert" class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"></span><span class="sr-only">Close</span></button><strong>oops!</strong> Something Went Wrong.</div>
                            <?php } 
                            ?>
                    </div>
                    <?php 
                    $withdraw_limits_qry = mysqli_query($con,"SELECT _WithdrawLimits FROM {$tbname}_withdraw_limit");
                    if(mysqli_num_rows($withdraw_limits_qry) > 0){
                        $withdraw_limits_qry_res = mysqli_fetch_assoc($withdraw_limits_qry);
                    }

                    $withdraw_limits = json_decode($withdraw_limits_qry_res['_WithdrawLimits']);
                    ?>
                    <form name="frmdis" method="POST" id="frmdis" action="withdraw_action.php" class="form-horizontal">
                    <input type="hidden" name="action" value="withdraw_limits">
                    <div class="row">
                        <div class="col-sm-2 col-md-2 text-right">
                            <div class="form-group">
                                <label for="inputEmail3" class="control-label">Version</label>
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10">
                            <label for="inputEmail3" class="control-label">Min Value</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 col-md-2 text-right">
                            <div class="form-group">
                                <label for="inputEmail3" class="control-label">Free Tier</label>
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $withdraw_limits[0][0];?>" id="txtkey" name="version[0][]">
                                </div>
                            </div>
                        </div>
                    <?php
                    
                    $selall = "Select * from ".$tbname."_versions";
                    $rstall = mysqli_query($con,$selall);

                    $key = $row['_Key'] ? $row['_Key'] : '';
                    while ($row = mysqli_fetch_assoc($rstall)) {
                        if(array_key_exists($row['_ID'], $withdraw_limits)){
                            $min_price = $withdraw_limits[$row['_ID']][0];
                        }
                    ?>
                        <div class="col-sm-2 col-md-2 text-right">
                            <div class="form-group">
                                <label for="inputEmail3" class="control-label"><?php echo $row['_Version']; ?></label>
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <input type="text" required class="form-control" value="<?php echo $min_price;?>" id="txtkey" name="version[<?php echo $row['_ID']; ?>][]" >
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                        
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label"> </label>
                            <div class="col-sm-10">
                                <input type="submit" required class="btn btn-default" id="submit" name="submit" value="Submit">
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
