<?php 
session_start();
include_once('../dbopen.php');
$id          = decrypt($_REQUEST['id'] , $encrypt);
$action      = decrypt($_REQUEST['e_action'] , $encrypt);
$key	     = replaceSpecialChar($_REQUEST['txtkey']);

//print_r($_POST);exit;
if($action == 'add'){
	$query = "INSERT INTO ".$tbname."_discountourkeys (`_Key` , `_Created`) VALUES ( ";
	if($key != ''){
		$query .= " '".$key."' ,";
	} else {
		$query .= " null ,";
	}
	
	$query .= " '".$today."')";
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'discountkeys.php?result=".encrypt('success' , $encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'discountkeys.php?result=".encrypt('failed' , $encrypt)."';</script>";
		exit;
	}
} 
elseif($action == 'edit'){
	$query = "UPDATE ".$tbname."_discountourkeys SET ";
	if($key != ''){
		$query .= "`_Key` = '".$key."' , ";
	} else { 
		$query .= "`_Key` = null , ";
	}
	
	$query .= " _Created = '".$today."' WHERE _ID = ".$id;
	
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'discountkeys.php?result=".encrypt('success' , $encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'discountkeys.php?result=".encrypt('failed' , $encrypt)."';</script>";
		exit;
	}
}
else if($action == 'delete'){
	$query = "DELETE FROM ".$tbname."_discountourkeys WHERE _ID = ".$id;
	if($run = mysqli_query($con , $query)){
		echo "<script type='text/javascript'>window.location = 'discountkeys.php?result=".encrypt('success' , $encrypt)."';</script>";
		exit;
	} else {
		echo "<script type='text/javascript'>window.location = 'discountkeys.php?result=".encrypt('failed' , $encrypt)."';</script>";
		exit;
	}
}
?>