	<?php
	$current_file = basename($_SERVER['PHP_SELF']);
    ?>
    <nav id="main_menu">
                <div class="menu_wrapper">
                    <ul>
                        <li class="first_level <?php if ($current_file == "dashboard.php")  {  echo "section_active";  } ?>">
                            <a href="dashboard.php">
                                <span class="icon_house_alt first_level_icon"></span>
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="first_level">
                            <a href="javascript:void(0)">
                                <span class="el-icon-group first_level_icon"></span>
                                <span class="menu-title">Users</span>
                            </a>
                            <ul>
                                <li class="submenu-title">Users</li>
                                <li><a <?php if ($current_file == "ourusers.php")  { ?> class="act_nav" <?php } ?> href="ourusers.php">Our Users</a></li>
                                <!--<li><a <?php if ($current_file == "ournewusers.php")  { ?> class="act_nav" <?php } ?> href="ournewusers.php">New Users</a></li>-->
                                <li><a href="bitmierusers.php" <?php if ($current_file == "bitmierusers.php")  { ?> class="act_nav" <?php } ?>>Bitmier Users</a></li>
                            </ul>
                        </li>
						<li class="first_level <?php if ($current_file == "versions.php")  {  echo "section_active";  } ?>">
                            <a href="versions.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Versions</span>
                            </a>
                        </li>
						<li class="first_level <?php if ($current_file == "upgrades.php")  {  echo "section_active";  } ?>">
                            <a href="upgrades.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Upgrades</span>
                            </a>
                        </li>
                        <!--<li class="first_level <?php if ($current_file == "newupgrades.php")  {  echo "section_active";  } ?>">
                            <a href="newupgrades.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">New User's Upgrades</span>
                            </a>
                        </li>-->
                        <li class="first_level <?php if ($current_file == "varifications.php")  {  echo "section_active";  } ?>">
                            <a href="varifications.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Verifications</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "newwithdrawls.php")  {  echo "section_active";  } ?>">
                            <a href="newwithdrawls.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Withdrawals</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "approvewithdrawls.php")  {  echo "section_active";  } ?>">
                            <a href="approvewithdrawls.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Approved/Cancle Withdrawals</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "discounters.php")  {  echo "section_active";  } ?>">
                            <a href="discounters.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Discounters</span>
                            </a>
                        </li>
						<li class="first_level <?php if ($current_file == "variables.php")  {  echo "section_active";  } ?>">
                            <a href="variables.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Variables</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "allpayto.php")  {  echo "section_active";  } ?>">
                            <a href="allpayto.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Change Payto Address</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "reviews.php" || $current_file == "editreview.php")  {  echo "section_active";  } ?>">
                            <a href="reviews.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Reviews</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "iplist.php" || $current_file == "editreview.php")  {  echo "section_active";  } ?>">
                            <a href="iplist.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">All Ip Lists</span>
                            </a>
                        </li>
						<li class="first_level">
                            <a href="javascript:void(0)">
                                <span class="el-icon-key first_level_icon"></span>
                                <span class="menu-title">Keys</span>
                            </a>
                            <ul>
                                <li class="submenu-title">Keys</li>
								<li><a <?php if ($current_file == "discountkey.php" || $current_file == "discountkeys.php" || $current_file == "add-discountkey.php")  { ?> class="act_nav" <?php } ?> href="discountkeys.php">Discount Keys</a></li>
                                <li><a href="ourkeys.php" <?php if ($current_file == "ourkey.php" || $current_file == "ourkeys.php" || $current_file == "add-ourkey.php")  { ?> class="act_nav" <?php } ?>>Our Keys</a></li>
								<li><a <?php if ($current_file == "flashsalekey.php" || $current_file == "flashsalekeys.php" || $current_file == "add-flashsalekey.php")  { ?> class="act_nav" <?php } ?> href="flashsalekeys.php">Flash Sale Keys</a></li>
                            </ul>
                        </li>
                        <li class="first_level <?php if ($current_file == "withdraw_limits.php")  {  echo "section_active";  } ?>">
                            <a href="withdraw_limits.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Withdraw Limits</span>
                            </a>
                        </li>
                        <li class="first_level <?php if ($current_file == "update_key.php")  {  echo "section_active";  } ?>">
                            <a href="update_key.php" >
                                <span class="el-icon-tasks first_level_icon"></span>
                                <span class="menu-title">Update Key</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="menu_toggle">
                    <span class="icon_menu_toggle">
                        <i class="arrow_carrot-2left toggle_left"></i>
                        <i class="arrow_carrot-2right toggle_right" style="display:none"></i>
                    </span>
                </div>
            </nav>