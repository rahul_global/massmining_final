<?php 
session_start();
include_once('../dbopen.php');
if(!isset($_SESSION['uid']) || $_SESSION['uid'] == '')
{
	header("location:login.php");
	exit;
}
$id = isset($_GET['id']) ? decrypt($_GET['id'],$encrypt) : '';

if($id == ''){
	header('location:upgrades.php');
	exit;
}

$title = $sitename." : Upgrade";

$query 		= "select a.* , b._Baddress from ".$tbname."_upgrades a LEFT JOIN ".$tbname."_users b on a._UserID = b._ID WHERE a._ID = ".$id;

$run	 	= mysqli_query($con,$query);
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- favicon -->
        <?php include 'topscript.php'; ?>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- header -->
			<header id="main_header">
				<?php include 'header.php'; ?>		
			</header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="dashboard.php">Dashboard</a></li>
					<li><a href="upgrades.php">Upgrages</a></li>
					<li class="active">Upgrade</li>
				</ul>
            </nav>
            <!-- main content -->
            <!-- main menu -->
            <?php include 'leftnav.php'; ?>
            <!-- main menu -->
             <div id="main_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <h2 style="color: #20638F;margin: 0px;">Upgrade</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">							
							<?php
							$num = mysqli_num_rows($run);
							if($num > 0)
							{
								$fetch = mysqli_fetch_assoc($run);
							?>
								<div class="col-md-3 col-sm-3"><h5>Bitcoin Address: </h5></div>
								<div class="col-md-9 col-sm-9"><h5><?php echo $fetch['_Baddress']; ?></h5></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h5>Version: </h5></div>
								<div class="col-md-9 col-sm-9"><h5><?php echo $fetch['_Version']; ?></h5></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h5>Price: </h5></div>
								<div class="col-md-9 col-sm-9"><h5><?php echo $fetch['_Price']; ?></h5></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h5>Pay To: </h5></div>
								<div class="col-md-9 col-sm-9"><h5><?php echo $fetch['_Payto']; ?></h5></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h5>Status: </h5></div>
								<div class="col-md-9 col-sm-9"><h5>
									<?php switch($fetch['_Status']){
										case '0' : 
											echo 'Pending';
											break;
										case '1' :
											echo 'Approved';
											break;
										case '2' :
											echo 'Cancelled';
											break;
										default :
											echo '';
									} ?>
								</h5></div>
								<div class="clearfix"></div>
								<div class="col-md-3 col-sm-3"><h5>Date: </h5></div>
								<div class="col-md-9 col-sm-9"><h5><?php echo DATE('d-M-Y H:i:s',strtotime($fetch['_Datetime'])); ?></h5></div>
							<?php   
							}
							?>
							<br>
							<a class="btn btn-primary" style="margin-top:20px;" href="upgrades.php">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
