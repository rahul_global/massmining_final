<?php
header("location:login.php");
exit; 
$error = $_REQUEST['err'];

?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Admin Login Page</title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

       <?php include 'topscript.php'; ?>

    </head>
    <body class="login_page">

        <div class="login_header">
            <img src="assets/img/logo.png" alt="site_logo">
        </div>
        <div class="login_register_form">
            <div class="form_wrapper animated-short" id="login_form">
                <h3 class="text-center"><span>Login</span></h3>
                <?php 
                    if($error == "1"){
                    ?>
                        <div role="alert" class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <strong>login Failed.</strong>Please enter correct Credential.
                        </div>
                    <?php
                    }
                ?>
                <form action="loginaction.php" method="post">
                    <div class="input-group input-group-lg sepH_a">
                        <span class="input-group-addon"><span class="icon_profile"></span></span>
                        <input type="text" class="form-control" name="txtusername" id="txtusername"  placeholder="Username" required>
                    </div>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><span class="icon_key_alt"></span></span>
                        <input type="password" name="txtpassword" id="txtpassword" class="form-control" placeholder="Password" required>
                        <input type="hidden" name="e_action" value="login">
                    </div>
                    <div class="text-right">
                        <a href="javascript:void(0)" class="small">Forgot password?</a>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="btnlogin" id="btnlogin" class="btn btn-lg btn-primary btn-block" value="Log in" style="margin-top: 10px;" />
                    </div>
                </form>
            </div>
        </div>

        
    </body>
</html>