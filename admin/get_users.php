<?php
include '../dbopen.php';
$table = 'bn_users';

// Table's primary key
$primaryKey = '_ID';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => '_Baddress', 'dt' => 0 ),
	array(
		'db'        => '_ID',
		'dt'        => 1,
		'formatter' => function( $d, $row ) {
			$d_link='<a href = "viewdepender.php?id='.encrypt($d,$GLOBALS['encrypt']).'&e_action='.encrypt('change',$GLOBALS['encrypt']).'" > View Dependers</a>';
			return $d_link;
		}
	),
	array(
		'db'        => '_Created',
		'dt'        => 2,
		'formatter' => function( $d, $row ) {
			return date( 'd-M-Y H:i:s', strtotime($d));
		}
	)
);


// SQL server connection information
$sql_details = array(
	'user' => USERNAME,
	'pass' => PASSWORD,
	'db'   => DATABASE,
	'host' => HOST
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);


