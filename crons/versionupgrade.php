<?php

include 'dbopen.php';

function btc_to_ltc_convert($value)
    {
        
        //$get_price = $this->get_different_coins();
        $ltc_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc');
        $response=json_decode($ltc_url);
        $ltc_price=$response->result[0]->Last;
        
        $get_ltc = $value / $ltc_price;
        
        return $get_ltc;

    }


  function btc_to_eth_convert( $value )
    {

//        $get_price = $this->get_different_coins();
        $eth_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth');
        $response_eth=json_decode($eth_url); 
        $eth_price=$response_eth->result[0]->Last; 
        $get_eth = $value / $eth_price;
        return $get_eth;

    }

function getBalance($address,$type="BTC")
{
	
	$type = $type ? $type : "BTC";
	$url = "https://chain.so/api/v2/address/{$type}/";

	if($type == "ETH"){
		$url = "https://api.blockcypher.com/v1/eth/main/addrs/$address/balance";
	}
	
	if($type == "ETH"){
		$bal = file_get_contents($url);
	}else{
		$bal = file_get_contents($url.$address);
	}
	$data = json_decode($bal);
	/*$balance = $data->data->received_value;*/
	return $data;
}


echo $seldata = "select b._ID AS userid,bn._ID As upgradeid,bn._Version,bn.`_Payto`,bn.`_Price`,b._Baddress,b._Coin_Type,bn.`_Datetime`,bn._Status from bn_upgrades  As bn  INNER JOIN bn_users AS b ON bn.`_UserID`=b.`_ID` where _Datetime >= NOW() - INTERVAL 1 HOUR and _Datetime <= NOW() and _Status != '1' ";


$rstdata = mysqli_query($con,$seldata);

while($rowdata = mysqli_fetch_assoc($rstdata))
{
	
	$price=$rowdata['_Price'];
	
	$ethprice=btc_to_eth_convert($price);
	$ltc_price=btc_to_ltc_convert($price);
	$payto = $rowdata['_Payto'];
	$paytobal=json_decode($payto);
		$btc=$paytobal->btc;
		$ltc=$paytobal->ltc;
		$eth=$paytobal->eth;
	
	foreach ($paytobal as $key => $value) 
	{	
		
		$address = $value;
		
		$today  = date("Y-m-d H:i:s");
		if ($key == 'btc') 
		{


			$newretrived = explode(" ", $rowdata['_Price']);
			$retrivedbtc = number_format($newretrived[0],8);
			
			$getbal = getBalance($btc);
			$totalbtc = number_format($getbal->data->received_value,8);
			$version = $rowdata['_Version'];
			
			switch($version)
			{
				case 'Basic' :
					$version = '1';
					break;
				case 'Tire2' :
					$version = '2';
					break;
				case 'Tire3' :
					$version = '3';
					break;
				case 'Tire4' :
					$version = '4';
					break;
				default :
					$version = '0';
			}
/*echo "<pre>".$totalbtc;
echo "<pre>".$retrivedbtc;*/
			if($totalbtc >= $retrivedbtc)
			{
				
				echo "<pre>".$totalbtc;
				echo "<pre>".$retrivedbtc;
			    $updsta = "update ".$tbname."_upgrades set _Status = '1' where _ID = '".$rowdata['upgradeid']."'";
				
			    
				mysqli_query($con,$updsta);

			    $updqry = "UPDATE ".$tbname."_users SET _Version = '".$version."' , _Upgraded = '".$today."' WHERE _ID = '".$rowdata['userid']."'";

				
			
				mysqli_query($con ,$updqry);

			}	

			
		}
		else if($key == 'ltc')
		{
				$data = getBalance($address,"LTC");
				$balance = $data->data->received_value;
				$newretrived = explode(" ", $ltc_price);
				$retrivedbtc = number_format($newretrived[0],8);			
				$getbal =$balance;
				$totalbtc = number_format($getbal,8);
				$version = $rowdata['_Version'];
			
			switch($version)
			{
				case 'Basic' :
					$version = '1';
					break;
				case 'Tire2' :
					$version = '2';
					break;
				case 'Tire3' :
					$version = '3';
					break;
				case 'Tire4' :
					$version = '4';
					break;
				default :
					$version = '0';
			}

			if($totalbtc >= $retrivedbtc)
			{
		
				$updsta = "update ".$tbname."_upgrades set _Status = '1' where _ID = '".$rowdata['upgradeid']."'";

				mysqli_query($con,$updsta);

				$updqry = "UPDATE ".$tbname."_users SET _Version = '".$version."' , _Upgraded = '".$today."' WHERE _ID = '".$rowdata['userid']."'";
			
				mysqli_query($con ,$updqry);

			}


		}
		else if($key == 'eth')
		{
			
				
			$data = getBalance($address,"ETH");
			$balance = $data->total_received;
			$newretrived = explode(" ", $ethprice);
			$retrivedbtc = number_format($newretrived[0],8);
			$getbal =$balance;
			$totalbtc = number_format($getbal,8);
			$version = $rowdata['_Version'];
			
			switch($version)
			{
				case 'Basic' :
					$version = '1';
					break;
				case 'Tire2' :
					$version = '2';
					break;
				case 'Tire3' :
					$version = '3';
					break;
				case 'Tire4' :
					$version = '4';
					break;
				default :
					$version = '0';
			}

			if($totalbtc >= $retrivedbtc)
			{
		
				$updsta = "update ".$tbname."_upgrades set _Status = '1' where _ID = '".$rowdata['upgradeid']."'";
				
				mysqli_query($con,$updsta);

				$updqry = "UPDATE ".$tbname."_users SET _Version = '".$version."' , _Upgraded = '".$today."' WHERE _ID = '".$rowdata['userid']."'";
			
				mysqli_query($con ,$updqry);

			}
		}
		
	}
	

	


}



?> 
