 <div class="slider" id="scroll-top">
      <div class="container">
        <div class="slider-title">
          <div class="row">
            <div class="col-sm-8">
              <h1 class="page-title">PAYOUTS</h1>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb pull-right">
                <li><a href="<?php echo base_url();?>account/dashboard">Home</a></li>
                <li class="active">Payouts</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
            <?php
if(isset($this->session->userdata['loginuser']['usertype']))
{

 $this->load->view('theme/balancemodel');

} ?>
    </div>

    <div class="section-1">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="buy-model">
                 <div class="row">
                <div class="board">
                  <div class="board-inner">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                          <a href="#home" data-toggle="tab" title="BITCOIN">
                            <span class="round-tabs one">
                                <img src="<?php echo base_url();?>assets/images/btc2.png">
                            </span> 
                          </a>
                        </li>
                        <li>
                          <a href="#profile" data-toggle="tab" title="LITECOIN">
                            <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/ltc2.svg">
                            </span> 
                          </a>
                        </li>
                        <li>
                          <a href="#messages" data-toggle="tab" title="ETHEREUM">
                            <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/eth2.svg">
                            </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                     <div class="tab-content">
                      <div class="tab-pane fade in active" id="home">
                        <div class="btc-pay-title">
                          <h3>Bitcoin Payouts</h3>
                        </div>
                        <div class="table-responsive">
                          <input type="hidden" name="offset" id="offset" value="0">
                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>BTC</th>
                                <th>Address</th>
                                <th>TXID</th>
                              </tr>
                            </thead>
                            <tbody id="tablebody">
                              
                            </tbody>
                          </table>
                        </div>
                        <div class="load-more">
                          <a href="javascript:void(0)" class="reflink loadmore-btn " style="text-decoration:none" onclick="loadpayouts(); return false;"><div class="reflink loadmore-btn" id="loaddiv" style="display: block;">
                            <div class="copytext">Load more</div>
                          </div></a>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="profile">
                        <div class="btc-pay-title">
                          <h3>Litecoin Payouts</h3>
                        </div>
                        <div class="table-responsive">
                           <input type="hidden" name="offset" id="offsetltc" value="0">
                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>BTC</th>
                                <th>Address</th>
                                <th>TXID</th>
                              </tr>
                            </thead>
                            <tbody id="tablebodyltc"> 
                              
                            </tbody>
                          </table>
                        </div>
                        <div class="load-more">
                          <a href="javascript:void(0)" style="text-decoration:none" onclick="loadData(); return false;"> <div class="reflink loadmore-btn" id="loaddivltc" style="display: block;">
                            <div class="copytext">Load more</div>
                          </div></a>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="messages">
                        <div class="btc-pay-title">
                          <h3>Ethereum Payouts</h3>
                        </div>
                       <div class="table-responsive">
                         <input type="hidden" name="offset" id="offseteth" value="0"> 
                        <table class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>Date</th>
                              <th>BTC</th>
                              <th>Address</th>
                              <th>TXID</th>
                            </tr>
                          </thead>
                          <tbody id="tablebodyeth">
                            
                          </tbody>
                        </table>
                      </div>
                      <div class="load-more">
                        <a href="javascript:void(0)" style="text-decoration:none" onclick="loadDataeth(); return false;"> <div class="reflink loadmore-btn" id="loaddiveth" style="display: block;">
                            <div class="copytext">Load more</div>
                          </div></a>
                      </div>
                      </div>                      
                    </div>
                  </div>
                </div>
               </div>
            
          </div>
        </div>
      </div>
    </div>