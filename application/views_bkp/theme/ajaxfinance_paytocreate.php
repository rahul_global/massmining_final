
 <?php $sitename=$this->App_model->getdata('bn_variables','_Name','sitename'); ?>
  <div class="row">
                <div class="board">
                  <div class="board-inner">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                          <a href="#home" data-toggle="tab" title="BITCOIN">
                            <span class="round-tabs one">
                                <img src="<?php echo base_url();?>assets/images/btc2.png">
                            </span> 
                          </a>
                        </li>
                        <li>
                          <a href="#profile" data-toggle="tab" title="LITECOIN">
                            <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/ltc2.svg">
                            </span> 
                          </a>
                        </li>
                        <li>
                          <a href="#messages" data-toggle="tab" title="ETHEREUM">
                            <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/eth2.svg">
                            </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                     <div class="tab-content">

                      <div class="tab-pane fade in active" id="home">
                        <p class="narrow text-center">
                           Please Upgrade or Pay maintenance fees of 0.001 BTC to   <a href="<?php echo  $url_btc; ?>" target="_blank"><h4 class="btc-id"><?php echo $paytoaddress['btc']; ?></h4></a> <br>and to process withdrawals. (Maintenance fees are only applicable to free Users)</p>
                        </p>
                        <img style="height: 180px; width: 200px;" src="<?php echo base_url('assets/uploads/qr_image/'.$img_url_btc); ?>" alt="QRCode Image">
                        
                      </div>

                      <div class="tab-pane fade" id="profile">
                        <p class="narrow text-center">
                           Please Upgrade or Pay maintenance fees of <?php echo $ltc_Convert;?> LTC to   <a href="<?php echo  $url_btc; ?>" target="_blank"><h4 class="btc-id"><?php echo $paytoaddress['ltc']; ?></h4></a> <br>and to process withdrawals. (Maintenance fees are only applicable to free Users)</p>
                        </p>
                        <img style="height: 180px; width: 200px;" src="<?php echo base_url('assets/uploads/qr_image/'.$img_url_ltc); ?>" alt="QRCode Image">
                      </div>

                      <div class="tab-pane fade" id="messages">
                       <p class="narrow text-center">
                           Please Upgrade or Pay maintenance fees of <?php echo $eth_Convert; ?> ETH  to <h4 class="btc-id"><a href="<?php echo $url_eth; ?>" target="_blank"><?php echo $paytoaddress['eth']; ?></a></h4> <br>and to process withdrawals. (Maintenance fees are only applicable to free Users)</p>
                        
                        <img style="height: 180px; width: 200px;" src="<?php echo base_url('assets/uploads/qr_image/'.$img_url_eth); ?>" alt="QRCode Image">

                      </div>                      
                    </div>
                  </div>
                </div>












