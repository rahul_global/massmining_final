<div class="slider" id="scroll-top">
      <div class="container">
        <div class="slider-title">
          <div class="row">
            <div class="col-sm-8">
              <h1 class="page-title">REVIEWS</h1>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb pull-right">
                <li><a href="<?php echo base_url();?>dashboard.php">Home</a></li>
                <li class="active">Reviews</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <?php
      if(isset($this->session->userdata['loginuser']['usertype']))
        {

          $this->load->view('theme/balancemodel');

        } ?>
    </div>

    <div class="reviews-page">
      <div class="container">
        <div class="reviews-row">
           <?php 
                if(isset($this->session->userdata['loginuser']['usertype'])){
                  ?>
              <form method="post" action="<?php echo base_url();?>home/AddReview" onSubmit="return validateform();" >
            <?php } ?>
          <div class="review-title">
            <h3>Your Rating</h3>
          </div>

          <div class="rating">

            <input id="star5" name="star" type="radio" value="5" class="radio-btn hide" />
            <label for="star5" >☆</label>
            <input id="star4" name="star" type="radio" value="4" class="radio-btn hide" />
            <label for="star4" >☆</label>
            <input id="star3" name="star" type="radio" value="3" class="radio-btn hide" />
            <label for="star3" >☆</label>
            <input id="star2" name="star" type="radio" value="2" class="radio-btn hide" />
            <label for="star2" >☆</label>
            <input id="star1" name="star" type="radio" value="1" class="radio-btn hide" />
            <label for="star1" >☆</label>
            <div class="clear"></div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="subject" name="subject" placeholder="Title" required>
          </div>
          <div class="form-group">
            <textarea class="form-control" type="textarea" placeholder="Enter your review" maxlength="140" rows="7" name="comment"></textarea>
          </div>
           <div class="form-group">
              <span class="msg-error error"></span>
              <div id="recaptcha" class="g-recaptcha" data-sitekey="6LfEgVkUAAAAAOZaRuCNvTt4XAfrpNU7_7jpxe8A"></div>
              <div id="captcha_error"></div>
            </div>
             <?php 
                if(empty($this->session->userdata['loginuser']['usertype'])){
                  ?>
                           <div class="aleart-reviwe"></div><?php }?>
          <div class="mt-4">
            <button class="btn btn-unique" type="submit" id="btn-validate">Submit</button>
          </div>

        </form>
        </div>
        <div class="old-reviews">
          <div class="title-review">
            <h1>Reviews</h1>
          </div>

          <div class="rev1">
            <div id="reviewdata">
          <input type="hidden" name="offset" id="offsetreview" value="0">
            <!--  -->
          </div>
            
            
            
            <div class="load-more">
             <a href="javascript:void(0)" class="reflink loadmore-btn " style="text-decoration:none" id="loaddivreview" onclick="reviewDetail();return false;"><div class="reflink loadmore-btn" id="loaddiv" style="display: block;">
                            <div class="copytext">Load more</div>
                          </div></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" language="javascript">   
  function validateform(){
    var captcha_response = grecaptcha.getResponse();      
    if(captcha_response.length == 0){
      document.getElementById('captcha_error').innerHTML = 'Please Select Captcha';
      document.getElementById('captcha_error').style.color = '#ff0000';
      return false;
    }else{
      return true;
    }
  } 

    
</script>
    