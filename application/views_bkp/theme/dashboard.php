    <div class="slider" id="scroll-top">
      <div class="container">
          
          <div class="tabs">
            <div class="col-md-4 col-md-offset-4" role="tabpanel">
              <ul class="nav nav-justified" id="nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#dustin" aria-controls="dustin" role="tab" data-toggle="tab">
                    <img class="img-circle" src="<?php echo base_url();?>assets/images/btc.png" />
                    <h4>BITCOIN</h4>
                  </a>
                </li>
                <li role="presentation">
                  <a href="#daksh" aria-controls="daksh" role="tab" data-toggle="tab">
                    <img class="img-circle" src="<?php echo base_url();?>assets/images/ltc.png" />
                    <h4>LITECOIN</h4>
                  </a>
                </li>
                <li role="presentation">
                  <a href="#anna" aria-controls="anna" role="tab" data-toggle="tab">
                    <img class="img-circle" src="<?php echo base_url();?>assets/images/etc.png" />
                    <h4>ETHEREUM</h4>
                  </a>
                </li>
              </ul>
            </div>
          <!-- Tab panes -->
          <div class="tab-content" id="tabs-collapse">
            <div role="tabpanel" class="tab-pane fade in active" id="tab-14" data-tabname="addrbtcbtn">
                <div class="tab-inner addres-btc">
                <h2>BITCOIN MINING</h2>                    
                    <p class="lead">Multicoin is designed to be an online system used to allow any cryptocurrency enthusiast to buy hash power and begin reaping the benefits of bitcoin and altcoin mining. Once a user purchases one of the offered mining contracts, the user is entitled to immediately begin free cloud mining for their chosen coins. Just enter your address in the field below and then click on the start mining button and then see your balance growing while your computer works for you.</p>
                    <div class="slider-form">
                     <div id="div-errobtc" class="login-error" style="display: none;">
                         </div>  <br>
                        <label class="label-img"><img src="<?php echo base_url();?>assets/images/btc.png"></label>
                        <input type="hidden" name="txtreffer" id="txtreffer" value="<?php echo $_GET['referral']; ?>">
                        <input id="addrbtc" type="text" name="addrbtc" placeholder="Enter your Bitcoin address here...">
                        <button type="button" id="addrbtcbtn" onclick="signtest(addrbtc.value,this.id,this.parentNode.parentNode.parentNode.getAttribute('data-tabname'));">START MINING</button>
                     
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="daksh" data-tabname = 'addrltcbtn'>
                <div class="tab-inner">
                  <h2>LITECOIN MINING</h2>
                    <p class="lead">Multicoin is designed to be an online system used to allow any cryptocurrency enthusiast to buy hash power and begin reaping the benefits of bitcoin and altcoin mining. Once a user purchases one of the offered mining contracts, the user is entitled to immediately begin free cloud mining for their chosen coins. Just enter your address in the field below and then click on the start mining button and then see your balance growing while your computer works for you.</p>
                    <div class="slider-form">
                        <div id="div-errorltc" class="login-error" style="display: none;">
                        </div>  <br>
                        <label class="label-img"><img src="<?php echo base_url();?>assets/images/ltc.png"></label>
                        <input type="hidden" name="txtreffer" id="txtreffer" value="<?php echo $_GET['referral']; ?>"> 
                        <input type="text" name="addr" id="addrltc" placeholder="Enter your Litecoin address here...">
                        <button type="button" id="addrltcbtn" onclick="signtest(addrltc.value,this.id,this.parentNode.parentNode.parentNode.getAttribute('data-tabname'));">START MINING</button>
                     
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="anna" data-tabname = 'addrethbtn'>
                <div class="tab-inner">
                  <h2>ETHEREUM MINING</h2>
                    <p class="lead">Multicoin is designed to be an online system used to allow any cryptocurrency enthusiast to buy hash power and begin reaping the benefits of bitcoin and altcoin mining. Once a user purchases one of the offered mining contracts, the user is entitled to immediately begin free cloud mining for their chosen coins. Just enter your address in the field below and then click on the start mining button and then see your balance growing while your computer works for you.</p>
                    <div class="slider-form">
                        <div id="div-erroeth" class="login-error" style="display: none;"></div><br>
                        <label class="label-img"><img src="<?php echo base_url();?>assets/images/etc.png"></label>
                         <input type="hidden" name="txtreffer" id="txtreffer" value="<?php echo $_GET['referral']; ?>">
                        <input type="text"  name="addr" id="addreth"  placeholder="Enter your Ethereum address here...">
                        <button type="button" id="addrethbtn" onclick="signtest(addreth.value,this.id,this.parentNode.parentNode.parentNode.getAttribute('data-tabname'));">START MINING</button>
                     
                    </div>
                </div> 
            </div>
          </div>  
        </div>
      </div>
    </div>


    <section class="section-1 hideme">
      <div class="container">
          <div class="col-sm-3">
            <div class="features">
              <i class="fa fa-get-pocket" aria-hidden="true"></i>
              <h3>Ref. Commission</h3>
              <p>Referral program is tiered in 3 levels 5%-2%-1%</p>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="features">
              <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
              <h3>Withdraw as little</h3>
              <p>as 0.01 coin Profit straight to your wallet.</p>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="features">
              <i class="fa fa-server" aria-hidden="true"></i>
              <h3>Protected Servers</h3>
              <p>We use powerful dedicated servers and protected</p>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="features">
              <i class="fa fa-cloud" aria-hidden="true"></i>
              <h3>Free Start Mining</h3>
              <p>Get power 30 GH/S (Lifetime) for new joining member.</p>
            </div>
          </div>
      </div>
    </section>

    <section class="section-2 hideme">
      <div class="container">
          <div class="connection">
            <h2>YOUR PERSONAL CONNECTION TO A STRONGER FINANCIAL FUTURE</h2>
            <p>If you want to mine the latest crypto currencies and invest in an exploding market Multicoin is a long term, secure and profitable investment program. One of the company's most prioritized activities is cryptocurrency and Altcoin Mining. Profits from this activity are used to enhance our program and increase its stability for the long term.</p>
            <p>In order to discuss altcoin mining we need to first understand the difference between regular Bitcoin mining. As you may know, altcoi is a term referred to any cryptocurrency which isn’t Bitcoin. Some examples of altcoins include: Ethereum, Litecoin, Dash, and much much more. When we say “altcoin cloud mining” we mean a way of mining where you constantly switch the coin you are mining on a daily weekly or Monthly basis, unlike Bitcoin mining where you would continuously mine the same cryptocurrency.</p>
            <p>The modern market is actively developing on the basis of Blockchain, which opens up wide opportunities for creating innovative solutions and autonomous, decentralized economic systems. In the digital age, crypto-economics takes one of the main roles, the introduction of a new business model that allows you to manage assets based on a completely new algorithm (SHA-256) or Scrypt algorithm.</p>
          </div>
      </div>
    </section>
   <script type="text/javascript">
  function signtest(address,main_id,data_tab_name){
   /* alert(main_id);
     var tab = '';  
    console.log('main_id '+main_id);
    console.log('data_tab_name '+ data_tab_name);
    if(main_id == data_tab_name && main_id == 'addrbtcbtn')
    {
      tab = 'btc';
      console.log('tab-14 yes got it '+main_id);
    }else if(main_id == data_tab_name && main_id == 'addrltcbtn')
    {
      tab = 'ltc';
      console.log('tab-15 yes got it' +main_id);
    }else if(main_id == data_tab_name && main_id == 'addrethbtn')
    {
      tab = 'eth';
      console.log('tab-16 yes got it '+ main_id);
    }else
    {
      alert('Enter address in only your coin tab.');
    }*/
   // var addresscheck;
    var reffid = document.getElementById('txtreffer').value;
    stat = document.getElementById('div-error');
    ltc=document.getElementById('div-errorltc');
    eth=document.getElementById('div-erroeth');

    var validadd = checkAddress(address);
    

    var nurl11="<?php echo base_url('Account/dashboard');?>";
     if(validadd === true && main_id == data_tab_name && main_id == 'addrbtcbtn')
     {
      /*console.log('btc');
      console.log('if true  then ans : '+validadd);
      console.log('tab-14 yes got it '+main_id);*/
        if(validadd === true){
      
          var dataString = "address="+address+"&reffid="+reffid;

            $.ajax({
                url : '<?php echo base_url('Account/getlogin');?>',
                data : dataString,
                type : 'POST',
                dataType : 'json',
                success : function(res){
                  if(res == '1'){
                    window.location.href=nurl11;
                  }else{
                    stat.innerText = 'OOops Something Went Wrong Please Try after some time.';
                    
                  }
                  
                }
              });
        }else{
          $("#div-errorbtc").show();
          stat.innerText = address+'is invalid Bitcoin address! Enter correct Bitcoin address';
         /* $('#div-errorltc').show();
           ltc.innerText = address+'is invalid Litecoin address! Enter correct Litecoin address'; */
          /*$('#div-erroeth').show();
          eth.innerText = address+'is invalid Ethereum address! Enter correct Ethereum address';   */ 
        }
     }else if( validadd === 0 && main_id == data_tab_name && main_id == 'addrltcbtn')
     {
      /*console.log('ltc');
      console.log('if validadd 0 then ans : '+validadd);
      console.log('tab-15 yes got it' +main_id);*/
        if(validadd === 0){
      
            var dataString = "address="+address+"&reffid="+reffid;

              $.ajax({
                  url : '<?php echo base_url('Account/getlogin');?>',
                  data : dataString,
                  type : 'POST',
                  dataType : 'json',
                  success : function(res){
                    if(res == '1'){
                      window.location.href=nurl11;
                    }else{
                      stat.innerText = 'OOops Something Went Wrong Please Try after some time.';
                      
                    }
                    
                  }
                });
          }else{
            /*$("#div-error").show();
            stat.innerText = address+'is invalid Bitcoin address! Enter correct Bitcoin address';*/
            $('#div-errorltc').show();
             ltc.innerText = address+'is invalid Litecoin address! Enter correct Litecoin address'; 
           /* $('#div-erroeth').show();
            eth.innerText = address+'is invalid Ethereum address! Enter correct Ethereum address';   */ 
          }
     }else if(validadd === 1 && main_id == data_tab_name && main_id == 'addrethbtn')
     {
      /*console.log('eth');
      console.log('if validadd 1 then ans : '+validadd);
      console.log('tab-16 yes got it '+ main_id);*/
          if(validadd === 1){
      
              var dataString = "address="+address+"&reffid="+reffid;

                $.ajax({
                    url : '<?php echo base_url('Account/getlogin');?>',
                    data : dataString,
                    type : 'POST',
                    dataType : 'json',
                    success : function(res){
                      if(res == '1'){
                        window.location.href=nurl11;
                      }else{
                        stat.innerText = 'OOops Something Went Wrong Please Try after some time.';
                        
                      }
                      
                    }
                  });
            }else{
              /*$("#div-error").show();
              stat.innerText = address+'is invalid Bitcoin address! Enter correct Bitcoin address';
              $('#div-errorltc').show();
               ltc.innerText = address+'is invalid Litecoin address! Enter correct Litecoin address'; */
              $('#div-erroeth').show();
              eth.innerText = address+'is invalid Ethereum address! Enter correct Ethereum address';    
            }
     }else
      {
        //alert('Enter address in only your coin tab.');
        if(main_id == data_tab_name && main_id == 'addrbtcbtn')
        {
          tab = 'btc';
         
          $('#div-errobtc').show();
          $('#div-errobtc').text('Enter Your Valid BTC address.');
        }else if(main_id == data_tab_name && main_id == 'addrltcbtn')
        {
          tab = 'ltc';
         
          $('#div-errorltc').show();
          $('#div-errorltc').text('Enter Your Valid LTC address.');
        }else if(main_id == data_tab_name && main_id == 'addrethbtn')
        {
          tab = 'eth';
     
          $('#div-erroeth').show();
          $('#div-erroeth').text('Enter Your Valid ETH address.');
          
        }        
      }
    }

    
   </script>