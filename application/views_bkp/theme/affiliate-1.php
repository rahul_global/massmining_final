  <div class="slider" id="scroll-top">
      <div class="container">
        <div class="slider-title">
          <div class="row">
            <div class="col-sm-8">
              <h1 class="page-title">AFFILIATE</h1>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb pull-right">
                <li><a href="<?php echo base_url();?>dashboard.php">Home</a></li>
                <li class="active">Affiliate</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="section-1">
      <div class="container">
        <div class="affi-prog">
          <div class="row">
            <div class="col-sm-1 col-xs-2 pdn0rit">
              <div class="commi">
                <span>5%</span>
              </div>
            </div>
            <div class="col-sm-11 col-xs-10">
              <div class="affiliate-program">
                <h2>Standard Commission</h2>
                <p>Not much work is needed from you; just share your referral link, which is made available in your account, with friends, and you’ll earn 5% from their active deposit.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="affi-prog">
          <div class="row">
            <div class="col-sm-1 col-xs-2 pdnrit0">
              <div class="commi">
                <span>10%</span>
              </div>
            </div>
            <div class="col-sm-11 col-xs-10">
              <div class="affiliate-program">
                <h2>Representatives Commission</h2>
                <p>This program is very similar to the normal affiliate program. The only difference is that the earning potential is larger - 10%, compared to the 5% of the normal affiliate program.</p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="become-rep">
          <h3>Become a Representative</h3>
          <p>However, the Representative program is not open to everybody. For you to become a Representative of Multimining, you need to have the ability to promote and support our investment project in your region through various means, like online/offline presentations, meetings with clients, a personal blog, etc. However, note that we never support SPAM nor any form of illegal promotion of our project.</p>
          <a href="<?php echo base_url();?>contact-us.php">CONTACT US</a>
        </div>
      </div>
    </div>