 <div class="slider" id="scroll-top">
      <div class="container">
        <div class="slider-title">
          <div class="row">
            <div class="col-sm-8">
              <h1 class="page-title">FAQ</h1>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb pull-right">
                <li><a href="<?php echo base_url();?>dashboard">Home</a></li>
                <li class="active">FAQ</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
       <?php
      if(isset($this->session->userdata['loginuser']['usertype']))
        {

          $this->load->view('theme/balancemodel');

        } ?>
    </div>
    
    <div class="section-1">
      <div class="container">
        <div class="faq">
          <div class="accordion-title">
            <h3>General Questions</h3>
          </div>
          <div class="panel-group feature-accordion brand-accordion icon angle-icon" id="basic">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a data-toggle="collapse" data-parent="#basic" href="#basic-one" aria-expanded="false" class="collapsed">
                            01. What is Multicoin Miner?
                          </a>
                        </h3>
                      </div>
                      <div id="basic-one" class="panel-collapse collapse in" aria-expanded="false">
                        <div class="panel-body">
                          <p>MulticoinMiner is an industry leading coin mining pool. All of the mining power is backed up by physical miners. Mining with the latest algorithms allows to make as much Bitcoin, Etherum and Litecoin as possible. We aim to provide you with the easiest possible way to make money without having to do any of the hard stuff.</p>
                          <p>With data centers around the globe, we aim to keep bills down and mining power high, meaning you can make more in a shorter amount of time than what it would take to mine from your home for instance. Our data centers are located in Europe, USA and China with dedicated Up-Links and 99% uptime!</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#basic" href="#basic-two" aria-expanded="false">
                            02. What is the main idea behind the establishment of Multicoin? Is the company for ‘real’? And is there anything that really proves this?
                          </a>
                        </h3>
                      </div>
                      <div id="basic-two" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                          <p>Multicoinis a cryptocurrency investment company established in 2018. The wide array of services we offer were specifically designed to assist coin holders in making good investment decisions. We are a London-based company that possesses a group of investment professionals covering many industries, ranging from engineering to advanced computer technology. Our group of professionals are widely versed in cryptocurrency techniques as well and can expertly answer any question you might have concerning cryptocurrency investment. </p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#basic" href="#basic-three" aria-expanded="false">
                            03. I want to become an active member by investing my Coins. Are investment returns guaranteed? Or are there certain risks I should be aware of?
                          </a>
                        </h3>
                      </div>
                      <div id="basic-three" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                          <p>You already made a good decision by deciding to join our investor community. Multicoins is a completely risk-free Bitcoin investment company, all thanks to our overall ideology and company concept. In addition, our company is made up of certified professionals in various fields such as Bitcoin mining, blockchain technology, cryptocurrency finance and security. Our goal is to provide a seamless investment experience based on expert project management. We aim to make Bitcoin cloud mining available to anybody that has no experience with cryptocurrency mining.</p>
                          <p>As a security measure, we secure your data with EV SSL, thereby making sure that your data is well encrypted and will never be made available to any third party. In addition, our servers are dedicated and DDoS protected, so you won’t have any problem using our services anytime from wherever you are in the world.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#basic" href="#basic-four" aria-expanded="false">
                            04. You seem to provide a quite lucrative interest rate for a long period of time. Where is the funds coming from?
                          </a>
                        </h3>
                      </div>
                      <div id="basic-four" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                          <p>As a result of the volume of Coin mining and our cutting-edge mining technology, we have the ability to provide an interest rate that’s higher than what’s usually obtainable in the average market. Moreover, the fact that we are always spreading our reach as far as we can globally helps us in reaching new heights. Plus, we have to stick only to concepts that have to do with modern-day infrastructure if we want to maintain our status as one of the best Bitcoin mining companies.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="" data-toggle="collapse" data-parent="#basic" href="#basic-five" aria-expanded="true">
                            05. What condition is required before one can become a member of your investment project? And is membership limited to certain countries?
                          </a>
                        </h3>
                      </div>
                      <div id="basic-five" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                          <p>You have to be at least 18 years old before you’re allowed to become a member. In addition to this, you must agree to our Terms of Service. Anybody from anywhere in the world can join. Even if you live in the desert, as long as you have an internet connection, you’re free to join.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-title">
                    <h3>INVESTMENT QUESTIONS</h3>
                  </div>
                  <div class="panel-group feature-accordion brand-accordion icon angle-icon" id="installation">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a data-toggle="collapse" data-parent="#installation" href="#installation-one">
                            01. How long does it take for my account to start generating Coinsafter I make a deposit?
                          </a>
                        </h3>
                      </div>
                      <div id="installation-one" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <p>Your account will start receiving Coins from the elapse of the first hour after a deposit is made. For example, if your deposit is made at 2:35pm, the first amount will be added at 2:35pm on next day, the second one by 2:35pm on second day and so on.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#installation" href="#installation-two">
                            02. Where will I receive the Coins from my active Plans?
                          </a>
                        </h3>
                      </div>
                      <div id="installation-two" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>All your coins are immediately applied to your account balance and are available for instant withdrawal. You can always go to the withdrawal section of your account to withdraw your funds.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#installation" href="#installation-three">
                            03. How many accounts are allowed per person? What about sharing my IP address with co-workers or family members?
                          </a>
                        </h3>
                      </div>
                      <div id="installation-three" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>No more than one account is allowed per person. Multiple account openings is against our internal users policy and might result in membership termination.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#installation" href="#installation-four">
                            04. How long each plans are valid for?
                          </a>
                        </h3>
                      </div>
                      <div id="installation-four" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>All plans are valid for 4 months from plan purchase date.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-title">
                    <h3>WITHDRAWALS QUESTIONS</h3>
                  </div>
                  <div class="panel-group feature-accordion brand-accordion icon angle-icon" id="support">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a data-toggle="collapse" data-parent="#support" href="#support-one">
                            01. I need to withdraw my funds, where do I go?
                          </a>
                        </h3>
                      </div>
                      <div id="support-one" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <p>To withdraw your funds, please go to the withdrawal section of your personal account and request a withdrawal.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#support" href="#support-two">
                            02. How long does it take for withdrawals to be processed?
                          </a>
                        </h3>
                      </div>
                      <div id="support-two" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>All withdrawals are instant. However, if for any reason it wasn’t processed instantly, then it will be processed before the elapse of 24 hours. You don’t have to contact us about your pending withdrawal, as you’ll always get paid.</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="accordion-title">
                    <h3>PARTNERSHIP QUESTIONS</h3>
                  </div>
                  <div class="panel-group feature-accordion brand-accordion icon angle-icon" id="partnership">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a data-toggle="collapse" data-parent="#partnership" href="#partnership-one">
                            01. What does affiliate program mean and can anyone benefit from it?
                          </a>
                        </h3>
                      </div>
                      <div id="partnership-one" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <p>The idea behind our affiliate program is to provide more earning opportunity to members who refer other people to our investment project. Not much work is needed from you; just share your referral link, which is made available in your account, with friends, and you’ll earn 5% from their active deposit. You can even earn more than 5% when you apply for a Representative status with our company.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#partnership" href="#partnership-two">
                            02. Are active deposits required from me before I can earn affiliate commission?
                          </a>
                        </h3>
                      </div>
                      <div id="partnership-two" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>No, no active deposit is required to earn regular 5% commission as an affiliate. In order to join a Representatives and earn 10% affiliate commission, you have to have total active deposit of 1 BitCoin or more.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#partnership" href="#partnership-three">
                            03. How does the Representative program work? I want to increase my earnings by applying for it.
                          </a>
                        </h3>
                      </div>
                      <div id="partnership-three" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>The Representative program is very similar to the normal affiliate program. The only difference is that the earning potential is larger - 10%, compared to the 5% of the normal affiliate program. However, the Representative program is not open to everybody. For you to become a Representative of Multicoins, you need to have the ability to promote and support our investment project in your region through various means, like online/offline presentations, meetings with clients, a personal blog, etc. However, notes that we never support SPAM nor any form of illegal promotion of our project.</p>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">
                          <a class="collapsed" data-toggle="collapse" data-parent="#partnership" href="#partnership-four">
                            04. What is the main difference between a Regional Representative and the standard affiliate program?
                          </a>
                        </h3>
                      </div>
                      <div id="partnership-four" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>The main difference is that Regional Representatives earn twice as much (10%) compared to standard affiliates. Plus, as a Regional Representative, you must have the ability to promote our project in your region.</p>
                        </div>
                      </div>
                    </div>
                  </div>
          
        </div>
      </div>
    </div>