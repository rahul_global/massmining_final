<?php
if($this->session->userdata['loginuser']['baddress'][0]=='3' || $this->session->userdata['loginuser']['baddress'][0]=='1' )
{

    $b_address = 'BTC';
   
}else if($this->session->userdata['loginuser']['baddress'][0] == '0')
{
    $b_address = 'ETH';

   
}else if($this->session->userdata['loginuser']['baddress'][0] == 'L')
{
    $b_address = 'LTC';
    
} ?>
 <?php $sitename=$this->App_model->getdata('bn_variables','_Name','sitename'); ?>
  <div class="row">
                <div class="board">
                  <div class="board-inner">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                          <a href="#home" data-toggle="tab" title="BITCOIN">
                            <span class="round-tabs one">
                                <img src="<?php echo base_url();?>assets/images/btc2.png">
                            </span> 
                          </a>
                        </li>
                        <li>
                          <a href="#profile" data-toggle="tab" title="LITECOIN">
                            <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/ltc2.svg">
                            </span> 
                          </a>
                        </li>
                        <li>
                          <a href="#messages" data-toggle="tab" title="ETHEREUM">
                            <span class="round-tabs one">
                              <img src="<?php echo base_url();?>assets/images/eth2.svg">
                            </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                     <div class="tab-content">

                      <div class="tab-pane fade in active" id="home">
                        <p class="narrow text-center">
                          In order to upgrade <?php echo $sitename[0]['_Value']; ?> to  <?php echo $version; ?> please send <?php echo $price; ?> <?php echo $btc;?> to:
                        </p>
                        <h4 class="btc-id"><?php echo $paytoaddress[btc]; ?></h4>
                        <img src="<?php echo base_url('assets/uploads/qr_image/'.$img_url_btc); ?>" class="bar-code">
                        <p class="wait">Waiting For Your Deposit. Status: <?php echo $price; ?> <?php echo $btc;?> received</p>
                        <span class="payment-notic">Your <?php echo $sitename[0]['_Value']; ?> will be upgraded automatically after payment is received and confirmed.</span>
                      </div>
                      <div class="tab-pane fade" id="profile">
                        <p class="narrow text-center">
                          In order to upgrade <?php echo $sitename[0]['_Value']; ?> to <?php echo $version; ?> please send <?php echo $ltc_Convert;?><?php echo $btc;?> to:
                        </p>
                        <h4 class="btc-id"><?php echo $paytoaddress[ltc]; ?></h4>
                        <img src="<?php echo base_url('assets/uploads/qr_image/'.$img_url_ltc); ?>" class="bar-code">
                        <p class="wait">Waiting For Your Deposit. Status:<?php echo $ltc_Convert;?> <?php echo $btc;?> received</p>
                        <span class="payment-notic">Your <?php echo $sitename[0]['_Value']; ?> will be upgraded automatically after payment is received and confirmed.</span>
                      </div>
                      <div class="tab-pane fade" id="messages">
                       <p class="narrow text-center">
                          In order to upgrade <?php echo $sitename[0]['_Value']; ?> to <?php echo $version; ?> please send <?php echo $eth_Convert;?> <?php echo $btc;?> to:
                        </p>
                        <h4 class="btc-id"><?php echo $paytoaddress[eth]; ?></h4>
                        <img src="<?php echo base_url('assets/uploads/qr_image/'.$img_url_eth); ?>" class="bar-code">
                        <p class="wait">Waiting For Your Deposit. Status: <?php echo $eth_Convert;?> <?php echo $btc;?> received</p>
                        <span class="payment-notic">Your <?php echo $sitename[0]['_Value']; ?> will be upgraded automatically after payment is received and confirmed.</span>
                        

                      </div>                      
                    </div>
                  </div>
                </div>
