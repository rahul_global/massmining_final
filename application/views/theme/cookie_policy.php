

 <div class="slider" id="scroll-top">

      <div class="container">

        <div class="slider-title">

          <div class="row">

            <div class="col-sm-8">

              <h1 class="page-title">COOKIE POLICY</h1>

            </div>

            <div class="col-sm-4">

              <ol class="breadcrumb pull-right">

                <!-- <li><a href="index.html">Home</a></li>

                <li class="active">FAQ</li> -->

              </ol>

            </div>

          </div>

        </div>

      </div>

      <?php

      if(isset($this->session->userdata['loginuser']['usertype']))

{ 

$this->load->view('theme/balancemodel');}?>



    </div>
<?php $this->load->view('theme/all_add'); ?>
    

    <div class="about-section">
      <div class="container">
        <div class="cookies1">
          <h3>What Are Cookies</h3>
          <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>
          <p>For more general information on cookies see the Wikipedia article on HTTP Cookies.</p>
        </div>
        
        <div class="cookies1">
          <h3>How We Use Cookies</h3>
          <p>We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.</p>
        </div>
        <div class="cookies1">
          <h3>Disabling Cookies</h3>
          <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site. Therefore it is recommended that you do not disable cookies.</p>
        </div>
        <div class="cookies1">
          <h3>The Cookies We Set</h3>
          <ul>
            <li>
              <p>Account related cookies</p>
              <p>If you create an account with us then we will use cookies for the management of the signup process and general administration. These cookies will usually be deleted when you log out however in some cases they may remain afterwards to remember your site preferences when logged out.</p>
            </li>
            <li>
              <p>Orders processing related cookies</p>
              <p>This site offers e-commerce or payment facilities and some cookies are essential to ensure that your order is remembered between pages so that we can process it properly.</p>
            </li>
          </ul>
        </div>
        <div class="cookies1">
          <h3>Third Party Cookies</h3>
          <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.</p>
          <ul>
            <li>
              <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.</p>
              <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>
            </li>
            <li>
              <p>The Google AdSense service we use to serve advertising uses a DoubleClick cookie to serve more relevant ads across the web and limit the number of times that a given ad is shown to you.</p>
              <p>For more information on Google AdSense see the official Google AdSense privacy FAQ.</p>
            </li>
          </ul>
        </div>
        <div class="cookies1">
          <h3>More Information</h3>
          <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does interact with one of the features you use on our site. </p>
          <p>However if you are still looking for more information then you can contact us through one of our preferred contact methods:</p>
          <ul>
            <li>
              <p>Email: admin@massmining.website</p>
            </li>
            
          </ul>
        </div>
      </div>
    </div>