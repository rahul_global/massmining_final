

 <div class="slider" id="scroll-top">

      <div class="container">

        <div class="slider-title">

          <div class="row">

            <div class="col-sm-8">

              <h1 class="page-title">DISCLAIMER</h1>

            </div>

            <div class="col-sm-4">

              <ol class="breadcrumb pull-right">

                <!-- <li><a href="index.html">Home</a></li>

                <li class="active">FAQ</li> -->

              </ol>

            </div>

          </div>

        </div>

      </div>

      <?php

      if(isset($this->session->userdata['loginuser']['usertype']))

{ 

$this->load->view('theme/balancemodel');}?>



    </div>
<?php $this->load->view('theme/all_add'); ?>
    

    <div class="about-section">
      <div class="container">
        <div class="cookies1">
          
          <p>This disclaimer ("Disclaimer", "Agreement") is an agreement between Website Operator ("Website Operator", "us", "we" or "our") and you ("User", "you" or "your"). This Disclaimer sets forth the general guidelines, terms and conditions of your use of the https://massmining.io/ and any of its products or services (collectively, "Website" or "Services").</p>
        </div>
        
        <div class="cookies1">
          <h3>Representation</h3>
          <p>Any views or opinions represented in this Website are personal and belong solely to Website Operator and do not represent those of people, institutions or organizations that the owner may or may not be associated with in professional or personal capacity unless explicitly stated. Any views or opinions are not intended to malign any religion, ethnic group, club, organization, company, or individual.</p>
        </div>
        <div class="cookies1">
          <h3>Content and postings</h3>
          <p>You may not modify, print or copy any part of the Website. Inclusion of any part of this Website in another work, whether in printed or electronic or another form or inclusion of any part of the Website in another website by embedding, framing or otherwise without the express permission of Website Operator is prohibited.</p>
        </div>
        <div class="cookies1">
          <h3>Indemnification and warranties</h3>
          <p>Website Operator guarantees the accuracy, reliability and completeness of the information and content on, distributed through or linked, downloaded or accessed from this Website. Furthermore, information contained on the Website and any pages linked to from it are subject to change at any time and without warning.</p>
          <p>We reserve the right to modify this Disclaimer at any time, effective upon posting of an updated version of this Disclaimer on the Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Website after any such changes shall constitute your consent to such changes.</p>
        </div>
        <div class="cookies1">
          <h3>Acceptance of this disclaimer</h3>
          <p>You acknowledge that you have read this Disclaimer and agree to all its terms and conditions. By accessing the Website you agree to be bound by this Disclaimer. If you do not agree to abide by the terms of this Disclaimer, you are not authorized to use or access the Website.</p>
         
        </div>
        <div class="cookies1">
          <h3>Contacting us</h3>
          <p>If you have any questions about this Agreement, please contact us.</p>
          <p>This document was last updated on June 6, 2018</p>
          
        </div>
      </div>
    </div>