<div class="slider" id="scroll-top">

      <div class="container">

        <div class="slider-title">

          <div class="row">

            <div class="col-sm-8">

              <h1 class="page-title">LEADER BOARD</h1>

            </div>

            <div class="col-sm-4">

              <ol class="breadcrumb pull-right">

                <li><a href="<?php echo base_url();?>account/dashboard">Home</a></li>

                <li class="active">Leader Board</li>

              </ol>

            </div>

          </div>

        </div>

      </div>

       <?php

      if(isset($this->session->userdata['loginuser']['usertype']))

        {



          $this->load->view('theme/balancemodel');



        } ?>

    </div>

<?php $this->load->view('theme/all_add'); ?>

    <div class="section-1">

      <div class="container">

        <div class="row">

          <div class="col-xs-12">

            <div class="buy-model">

                 <div class="row">

                <div class="board">

                  <div class="board-inner">

                    <ul class="nav nav-tabs" id="myTab">

                        <li class="active">

                          <a href="#home" data-toggle="tab" title="BITCOIN">

                            <span class="round-tabs one">

                                <img src="<?php echo base_url();?>assets/images/btc2.png">

                            </span> 

                          </a>

                        </li>

                        <li>

                          <a href="#profile" data-toggle="tab" title="LITECOIN">

                            <span class="round-tabs one">

                              <img src="<?php echo base_url();?>assets/images/ltc2.svg">

                            </span> 

                          </a>

                        </li>

                        <li>

                          <a href="#messages" data-toggle="tab" title="ETHEREUM">

                            <span class="round-tabs one">

                              <img src="<?php echo base_url();?>assets/images/eth2.svg">

                            </span>

                          </a>

                        </li>

                      </ul>

                    </div>

                     <div class="tab-content">

                      <div class="title-leaderboard">

                        <h2>This Week's Top 5 Miners</h2>

                        <p>We can assure you that our leader board is always updated. You can always check the Top Miners anytime.</p>

                      </div>

                      <div class="tab-pane fade in active" id="home">

                        <div class="table-responsive">

                          <table class="table table-bordered table-hover">

                            <thead>

                              <tr>

                                <th>Miner</th>

                                <th>WALLET ID</th>

                                <th>BTC</th>

                              </tr>

                            </thead>

                            <tbody>

                          

                                  <?php if(!empty($result)) { $i='1'; foreach($result as $res) {

                          $userinfo=$this->App_model->getdata('bn_users','_ID',$res['_UserID']);

                    

                    

                           ?>

                              <tr>

                                <td><?php echo $i; ?></td>

                                <td><a href="#"><?php echo $userinfo[0]['_Baddress']; ?></a></td>

                                <td class="pay-btc"><?php echo number_format($res['total'],2) ;?>BTC</td>

                              </tr>

                              <?php  $i++ ;} } ?>   

                              

                            </tbody>

                          </table>

                        </div>

                        

                      </div>

                      <div class="tab-pane fade" id="profile">

                        <input type="hidden" name="offset" id="offset" value="0"> 

                        <div class="table-responsive">

                          <table class="table table-bordered table-hover">

                            <thead>

                              <tr>

                                <th>Miner</th>

                                <th>WALLET ID</th>

                                <th>LTC</th>

                              </tr>

                            </thead>

                            <tbody>

                               <?php if(!empty($ltc)) { $i='1'; foreach($ltc as $res) {

                             

                             $userinfo=$this->App_model->getdata('bn_users','_ID',$res['_UserID']);

                    

                    

                            ?>

                              <tr>



                                <td><?php echo $i; ?></td>

                                <td><a href="#"><?php echo $userinfo[0]['_Baddress']; ?></a></td>

                                <td class="pay-btc"><?php echo number_format($res['total'],2) ;?>LTC</td>

                              </tr>

                              <?php  $i++ ;} } ?> 

                              

                            </tbody>

                          </table>

                        </div>

                        

                      </div>

                      <div class="tab-pane fade" id="messages">

                        

                       <div class="table-responsive">

                        <table class="table table-bordered table-hover" id="tablebodyeth">

                            <thead>

                              <tr>

                                <th>Miner</th>

                                <th>WALLET ID</th>

                                <th>ETH</th>

                              </tr>

                            </thead>

                            <tbody>

                          <?php if(!empty($eth)) { $i='1'; foreach($eth as $res) {

                              $userinfo=$this->App_model->getdata('bn_users','_ID',$res['_UserID']);

                        

                        

                               ?>

                              <tr>

                                <td><?php echo $i; ?></td>

                                <td><a href="#"><?php echo $userinfo[0]['_Baddress']; ?></a></td>

                                <td class="pay-btc"><?php echo number_format($res['total'],2) ;?>ETH</td>

                              </tr>

                              <?php  $i++ ;} } ?> 

                              

                            </tbody>

                          </table>

                      </div>

                      

                      </div>                      

                    </div>

                  </div>

                </div>

               </div>

            

          </div>

        </div>

      </div>

    </div>