<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Account extends MY_Controller {

	public $btc = '';
	public $is_hookable = FALSE;

function __construct() { 
	error_reporting(0);
   parent::__construct();
   /*$this->clear_cache();*/
   //$data=parent::get_different_coins();
   
  /* parent::header_price_convert();*/
   $this->load->helper(array('form', 'url'));
   $this->load->helper('url');
   $this->load->library('form_validation');
   $this->load->database(); 
   $this->load->library('session');
   $this->load->model('App_model');
   $this->load->library('encrypt');
   $this->load->library('curl');

   	if($this->session->userdata['loginuser']['usertype']==1){
	    $this->btc = 'BTC';
	    $usertype1='Bitcoin';
	} else if($this->session->userdata['loginuser']['usertype']==2) {
	    $this->btc = 'LTC';
	     $usertype1='Litcoin';
	} else if($this->session->userdata['loginuser']['usertype']==3) {
	    $this->btc = 'ETH';
	    $usertype1='Ethereum';
	}
} 

	public function refresh_page()
	{
		$result = array();
		$post = $this->input->post();
		$curr_url = $post['curr_url'];
		$function_reload =  substr($curr_url, strrpos($curr_url, '/') + 1);
		if($post['refreshstatus'] === 'ON')
		{
			$this->session->set_userdata(array('cookie_status'  => 'ON'));
			$page_id_session=$this->randLetter().time().$this->randLetter().rand(0,9999).$this->randLetter().rand(0,99999);
			
			$visitoruser="siteuser";
			$visitoruser_id=$page_id_session;
			$set_cookie =  setcookie($visitoruser, $visitoruser_id, time() + (86400 * 30));
		}
		if($post['refreshstatus'] === 'OFF')
		{
			$visitoruser="siteuser";
			$this->session->set_userdata('cookie_status','OFF');
			$visitoruser_id=$_COOKIE['siteuser'];
			setcookie($visitoruser, $visitoruser_id, time() - (86400 * 31));
			//header('Location:profile.php');
		}
		if($this->session->userdata('cookie_status') == 'ON')
  		{
  			$result['msg'] = 1; 
  		}else if($this->session->userdata('cookie_status') == 'OFF')
  		{
  			$result['msg'] = 0; 
  		}
  		echo json_encode($result);
  		//redirect($curr_url.'/'.$data['cookie_status']);
	}

	public function randLetter() {
			$int = rand(0,51);
			$a_z = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$rand_letter = $a_z[$int];
			return $rand_letter;
	}

   public function getlogin()
   {
   	 
   		 $address=$this->input->post('address'); 

   		 $reffurl=$this->input->post('reffid'); 
   	 
	   if(preg_match("/^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$/",$address))
	   {
			   error_reporting(0);

	  //$address=$this->input->post('address'); 
			 
				    if($reffurl!="") 
				    {
					  $reffid= base_url().'?referral='.$reffurl;
				    }
				    else
				    {
					 $reffid= base_url();	
					}
				 
				     $usertblresult=$this->App_model->getdata('bn_users','_Baddress',$address);
				     $userid=$usertblresult[0]['_ID'];
							$b_address=$usertblresult[0]['_Baddress'];
							$usertype=$usertblresult[0]['_Coin_Type'];
				      if(empty($usertblresult))
				      {
				      $userarray=array(
						    '_Username'=>'',
							'_Baddress'=>$address,
							'_Usertype'=>'0',
							'_Version'=>'0',
							'_Oldbtc'=>'',
							'_Verified'=>'2',
							'_Payto'=>'',
							'_Coin_Type'=>'1',
							'_Created'=>date('Y-m-d H:i:s'),
							'_Upgraded'=>'',
							'_Maincreated'=>date('Y-m-d H:i:s')
							); 
						
							$userid=$this->App_model->form_insertdata('bn_users',$userarray);	
				        }
				        else
				        {		
							$userid=$usertblresult[0]['_ID'];
							$b_address=$usertblresult[0]['_Baddress'];
							$usertype=$usertblresult[0]['_Coin_Type'];
						}
				
				        $visits=$this->App_model->getlastrecords('bn_visits');
				        $visitearray=array(
						    '_VisitID'=>$visits[0]['_ID']+1,
							'_UserID'=>$userid,
							'_Datetime'=>date('Y-m-d H:i:s'),
							'_Ip'=>$this->input->ip_address(),
							'_Refurl'=>$reffid
							); 
						
							$visitsid=$this->App_model->form_insertdata('bn_visits',$visitearray);	
				            
				            
				            
				          if($reffurl!="") 
				          {  
							  
							  $usertblresults=$this->App_model->getdata('bn_users','_ID',$reffurl);
							   if(!empty($usertblresults))
							   {
								 $referralarray=array(
								'_UserID'=>$reffurl,
								'_Datetime'=>date('Y-m-d H:i:s'),
								'_Ip'=>$this->input->ip_address(),
								'_Walletadd	'=>$address
								); 
								$referralid=$this->App_model->form_insertdata('bn_referral',$referralarray);	
						       }
						  }
						 
						if($usertype == '0'){
							$upd_user = array('_Coin_Type' => '1');
							$this->App_model->update_data('bn_users','_ID',$userid,$upd_user);

						}
						  
				        $logindata = array(
								     'userid'=>$userid,
								     'baddress'=>$b_address,
								     'usertype'=>'1',
								     'type' => 'bit'
									  );

				   		/*$cookie= array('name'   => 'userid', 'value'  => $userid);
			 			$this->input->set_cookie($cookie);*/
			 			/*$cookie = array(
						        'name'   => 'userid',
						        'value'  => $userid,
						        'expire' => '86400'
						);

			      	   set_cookie($cookie);*/
			         $this->session->set_userdata('loginuser',$logindata);
						
			  /*          echo $abc;exit;*/
			            
				  
				        
				       echo json_encode(1);
				   /*	}*/

			

			}
			else if(preg_match("/^L[a-zA-Z0-9]{26,33}$/",$address))
			{
			    /*$reffurl=$this->input->post('reffid'); */

				    if($reffurl!="") 
				    {
					  $reffid= base_url().'?referral='.$reffurl;
					  
					
				    }
				    else
				    {
					 $reffid= base_url();	
					}
				 	
				     $usertblresult=$this->App_model->getdata('bn_users','_Baddress',$address);
				     $userid=$usertblresult[0]['_ID'];
							$b_address=$usertblresult[0]['_Baddress'];
							$usertype=$usertblresult[0]['_Coin_Type'];
				      if(empty($usertblresult))
				      {
				      $userarray=array(
						    '_Username'=>'',
							'_Baddress'=>$address,
							'_Usertype'=>'0',
							'_Version'=>'0',
							'_Oldbtc'=>'',
							'_Verified'=>'2',
							'_Payto'=>'',
							'_Coin_Type'=>'2',
							'_Created'=>date('Y-m-d H:i:s'),
							'_Upgraded'=>'',
							'_Maincreated'=>date('Y-m-d H:i:s')
							); 
						
							$userid=$this->App_model->form_insertdata('bn_users',$userarray);
							

				        }
				        else
				        {
							$userid=$usertblresult[0]['_ID'];
							$b_address=$usertblresult[0]['_Baddress'];
							$usertype=$usertblresult[0]['_Coin_Type'];
							
								
								
					
						}
				       
				        $visits=$this->App_model->getlastrecords('bn_visits');
				        $visitearray=array(
						    '_VisitID'=>$visits[0]['_ID']+1,
							'_UserID'=>$userid,
							'_Datetime'=>date('Y-m-d H:i:s'),
							'_Ip'=>$this->input->ip_address(),
							'_Refurl'=>$reffid
							); 
						
							$visitsid=$this->App_model->form_insertdata('bn_visits',$visitearray);	
				            
				            
				            
				          if($reffurl!="") 
				          {  
							  
							  $usertblresults=$this->App_model->getdata('bn_users','_ID',$reffurl);
							   if(!empty($usertblresults))
							   {
								 $referralarray=array(
								'_UserID'=>$reffurl,
								'_Datetime'=>date('Y-m-d H:i:s'),
								'_Ip'=>$this->input->ip_address(),
								'_Walletadd	'=>$address
								); 
								$referralid=$this->App_model->form_insertdata('bn_referral',$referralarray);	
						       }
						  }

						if($usertype == '0'){
							$upd_user = array('_Coin_Type' => '2');
							$this->App_model->update_data('bn_users','_ID',$userid,$upd_user);
						}

				        $logindata = array(
								     'userid'=> $userid,
								     'baddress'=> $b_address,
								     'usertype'=> '2',
								     'type' => 'lite'
									  );
				   		/*$cookie = array(
						        'name'   => 'userid',
						        'value'  => $userid,
						        'expire' => '86400'
						);

			      	   set_cookie($cookie);*/
			            $this->session->set_userdata('loginuser',$logindata);
			            
			            
				  
				        
				       echo json_encode(1);

			   
			}
			else if(preg_match("/^(0x)?[0-9a-fA-F]{40}$/",$address))
			{
			  /* $reffurl=$this->input->post('reffid'); */

			  	
				    if($reffurl!="") 
				    {
					  $reffid= base_url().'?referral='.$reffurl;
				    }
				    else
				    {
					 $reffid= base_url();	
					}
				 
				     $usertblresult=$this->App_model->getdata('bn_users','_Baddress',$address);
		
				     $userid=$usertblresult[0]['_ID'];
					 $b_address=$usertblresult[0]['_Baddress'];
					 $usertype=$usertblresult[0]['_Coin_Type'];

				      if(empty($usertblresult)) {
				      $userarray=array(
						    '_Username'=>'',
							'_Baddress'=>$address,
							'_Usertype'=>'0',
							'_Version'=>'0',
							'_Oldbtc'=>'',
							'_Verified'=>'2',
							'_Payto'=>'',
							'_Coin_Type'=>'3',
							'_Created'=>date('Y-m-d H:i:s'),
							'_Upgraded'=>'',
							'_Maincreated'=>date('Y-m-d H:i:s')
							); 
						
							$userid=$this->App_model->form_insertdata('bn_users',$userarray);
							
						

				        } else {
				        	
							$userid=$usertblresult[0]['_ID'];
							$b_address=$usertblresult[0]['_Baddress'];
						    $usertype=$usertblresult[0]['_Coin_Type'];
						    

						   
						}

				        $visits=$this->App_model->getlastrecords('bn_visits');

				        $visitearray=array(
						    '_VisitID'=>$visits[0]['_ID']+1,
							'_UserID'=>$userid,
							'_Datetime'=>date('Y-m-d H:i:s'),
							'_Ip'=>$this->input->ip_address(),
							'_Refurl'=>$reffid
							); 
						
							$visitsid=$this->App_model->form_insertdata('bn_visits',$visitearray);	
				            
				            
				            
				          if($reffurl!="") 
				          {  
							  
							  $usertblresults=$this->App_model->getdata('bn_users','_ID',$reffurl);

							   if(!empty($usertblresults))
							   {
								 $referralarray=array(
								'_UserID'=>$reffurl,
								'_Datetime'=>date('Y-m-d H:i:s'),
								'_Ip'=>$this->input->ip_address(),
								'_Walletadd	'=>$address
								); 
								$referralid=$this->App_model->form_insertdata('bn_referral',$referralarray);	
						       }
						  }

						if($usertype == '0'){
							$upd_user = array('_Coin_Type' => '3');
							$this->App_model->update_data('bn_users','_ID',$userid,$upd_user);
						}

				        $logindata = array(
								     'userid'=>$userid,
								     'baddress'=>$b_address,
								     'usertype'=>'3',
								     'type' => 'ethereum'
									  );
				    /**/
				   		/*$cookie = array(
						        'name'   => 'userid',
						        'value'  => $userid,
						        'expire' => '86400'
						);

			      	   set_cookie($cookie);*/
			            $this->session->set_userdata('loginuser',$logindata);
			            
			            
				  
				        
				       echo json_encode(1);
			    
			}
			else{
				  //echo json_encode(0);
				
			} 
				       
   }
   
   
   
  
   
   public function dashboard()
   {  
   		error_reporting(0);
   		$this->is_hookable = TRUE;

	    $l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
				  
		$userid=$this->session->userdata['loginuser']['userid'];
			
		$b_address=$this->session->userdata['loginuser']['baddress'];
		
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];
		
		if($this->input->post('email'))
		{

			$notifyemail=array(
				'_Email'=>$this->input->post('email'),
				'_EmailStatus'=>'1'
			);
			/*echo "<pre>";
			print_r($notifyemail);exit;*/
		 	$result=$this->App_model->update_data('bn_users','_ID',$userid,$notifyemail);
		 	$data['succmsg']="You Have SuccessfullY Subscribe";
		 	
		}
		else
		{
			$data['succmsg']="You Have Failed Subscribe";
		}
		
		

		$data['resultbn_versions']=$this->App_model->getrecords('bn_versions');
		
		$current_user_version    = $loginuserinfo[0]['_Version'];

		$current_version_result=$this->App_model->getdata('bn_versions','_ID',$current_user_version);
		
		
		$current_version_btcperday = $current_version_result[0]['_Btcperday'];

		$current_version_btcpermin = $current_version_result[0]['_Btcpermin']; 
		
		$data['cur_version_earningperday'] = $current_version_btcperday;
	
		$data['cur_version_earningpermin'] = $current_version_btcpermin;
 
		$version_Btcpermin1 = $data['resultbn_versions'][0]['_Btcpermin'];
		$version_Btcpermin2 = $data['resultbn_versions'][1]['_Btcpermin'];
		$version_Btcpermin3 = $data['resultbn_versions'][2]['_Btcpermin'];
		$version_Btcpermin4 = $data['resultbn_versions'][3]['_Btcpermin'];

		$version_Btcperday1 = $data['resultbn_versions'][0]['_Btcperday'];
		$version_Btcperday2 = $data['resultbn_versions'][1]['_Btcperday'];
		$version_Btcperday3 = $data['resultbn_versions'][2]['_Btcperday'];
		$version_Btcperday4 = $data['resultbn_versions'][3]['_Btcperday'];

		$version_Btcprice1 = $data['resultbn_versions'][0]['_Price'];
		$version_Btcprice2 = $data['resultbn_versions'][1]['_Price'];
		$version_Btcprice3 = $data['resultbn_versions'][2]['_Price'];
		$version_Btcprice4 = $data['resultbn_versions'][3]['_Price'];
		
		/*echo $version_Btcpermin2;exit;*/

		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
*/		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{

			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcperday)), 8, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

			$data['ltc_data']= array(
				'ver_rate' => array(
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcprice1)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcprice2)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcprice3)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcprice4)), 10, '.', '')
				),
				'ver_day' => array(
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcperday1)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcperday2)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcperday3)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcperday4)), 10, '.', '')
				),
				'ver_min' => array(
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcpermin1)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcpermin2)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcpermin3)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($version_Btcpermin4)), 10, '.', '')
				)
			);
			

		}		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcperday)), 8, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');

			$data['eth_data']= array(
				'ver_rate' => array(
					parent::btc_to_eth_convert($version_Btcprice1),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcprice2)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcprice3)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcprice4)), 10, '.', '')
				),
				'ver_day' => array(
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcperday1)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcperday2)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcperday3)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcperday4)), 10, '.', '')
				),
				'ver_min' => array(
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcpermin1)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcpermin2)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcpermin3)), 10, '.', ''),
					number_format((float)$this->exp2dec(parent::btc_to_eth_convert($version_Btcpermin4)), 10, '.', '')
				)
			);	
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		$data['earnings_diff_coins'] = $earnings_diff_coin;

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		
     	$this->load->view('theme/include/header',$data);
		$this->load->view('theme/home',$data);
		$this->load->view('theme/include/footer',$data);   
	   
   }
   
   public function accounts()
   {  
	   
	   $l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}



		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
	    $data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);
		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;
		/*echo "<pre>";
		print_r($data);
		exit;*/
	    $this->load->view('theme/include/header');
		$this->load->view('theme/account',$data);
		$this->load->view('theme/include/footer');   
	   
   }
   
   
   
   	public function ajaxgetaccounts()
	{
		$offset11=$this->input->post('offset');
		 
		$userid=$this->session->userdata['loginuser']['userid'];
		 
			// print_r($offset11); die;
		$withresult=$this->App_model->getdata('bn_visits','_UserID',$userid);
		  $countres=count($withresult);

		  // print_r($countres); die;
		  if( $offset11 < $countres)
		  {
			  
			     if($offset11=='0')
		         {
			     $offset='0'; 
		         }
				 else
				 {
					$offset=$offset11;  
				 }
		  	$data['visitesresult']=$this->App_model->getaccountdatawithlimitoffset('bn_visits','_UserID',$userid,$offset);
	      }
	      else
	      {
			$data['errmsg']="no more records Found !";  
		  }
		$this->load->view('theme/ajaxgetaccounts',$data);
		
	}
   public function refferal()
   {  
	   
	   $l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);
		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;
	    $this->load->view('theme/include/header');
		$this->load->view('theme/refferal',$data);
		$this->load->view('theme/include/footer');   
	   
   }
   public function finance()
   {  

	   
	   $l_id=$this->App_model->authentication_login();

	   $userid=$this->session->userdata['loginuser']['userid'];
			
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		if($l_id=='0')
		{
			redirect('');
		}

		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$current_user_version    = $loginuserinfo[0]['_Version'];

		$current_version_result=$this->App_model->getdata('bn_versions','_ID',$current_user_version);

		$current_version_btcperday = $current_version_result[0]['_Btcperday'];

		$current_version_btcpermin = $current_version_result[0]['_Btcpermin']; 
			
		$data['cur_version_earningperday'] = $current_version_btcperday;

		$data['cur_version_earningpermin'] = $current_version_btcpermin;

		$data['minimum_paymnet_limit'] = 0.08;
		$payment_limit = 0.08;
		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
		/*
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$data['btc_earning'] = $earnings['_Amount'];

		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];
        
		


		if($btc == 'LTC')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');
         	/*$data['ltc_earning'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 10, '.', '');*/
			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_eth_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		/*	$data['eth_earning'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 10, '.', '');*/
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
			/*echo "<pre>";
		print_r($data);exit;*/
		$userid=$this->session->userdata['loginuser']['userid'];
		$networkID=$this->App_model->getDetail('bn_userinfo','_UserID');
		$query = $this->db->get_where('bn_userinfo', array('_UserID =' => $userid))->result();

		if(count($query) == 0) {
			
			
			$ip=$_SERVER["HTTP_CF_CONNECTING_IP"] ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER["REMOTE_ADDR"];

				$url = "http://ip-api.com/json/?ip=".$ip;
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$curlData = curl_exec($ch);
				/*echo "<pre>";
				print_r($curlData);
				echo "</pre>";*/
				$response_a = json_decode($curlData,true);
				/*echo "<pre>";
				print_r($response_a);echo "</pre>";
				exit;*/
				$userData=array(
								'_UserID'=>$userid,
								'_Ipaddress'=>$response_a['query'],
								'_ISP'=>$response_a['isp'],
								'_Country'=>$response_a['country'],
								'_CountryCode'=>$response_a['countryCode'],
								'_Timezone'=>$response_a['timezone'],
								'_Region'=>$response_a['regionName'],
								'_Zip'=>$response_a['zip'],

				 				);
				
				curl_close($ch);

				
				$data['insert']=$this->App_model->insertData('bn_userinfo',$userData);
			
						
		}else
		{
			/*$ip ='182.70.80.129';
			$ip=$_SERVER['REMOTE_ADDR'];*/
			$ip=$_SERVER["HTTP_CF_CONNECTING_IP"] ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER["REMOTE_ADDR"];

			$url = "http://ip-api.com/json/?ip=".$ip;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$curlData = curl_exec($ch);
			$response_a = json_decode($curlData,true);
			
			curl_close($ch);
		
		}
			
		  	$data['response_a']=$response_a;
		  	/*echo "<pre>";
		  	print_r($data);exit;*/
			$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
			$data['networkData']=$this->App_model->getdata('bn_userinfo','_UserID',$userid);
			
			if($this->input->post('email')){
				$updateEmail=array(
					'_Email'=>$this->input->post('email'));
				/*echo "<pre>";
				print_r($updateEmail);exit;*/
				$result=$this->App_model->update_data('bn_users','_ID',$userid,$updateEmail);
			}

			$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
			$data['upgrades']=$this->App_model->getdata('bn_upgrades','_UserID',$userid);
			$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);
			$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

			$current_user_version    = $loginuserinfo[0]['_Version'];
			$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
			$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

			$current_user_version    = $loginuserinfo[0]['_Version'];
			$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
			$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

			if(array_key_exists($current_user_version, $w_limits)){
				$min_value = $w_limits[$current_user_version][0];
			}

			if($this->btc == "ETH"):
				$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
			elseif($this->btc == "LTC"):
				$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
			endif;

			$data['min_value'] = $min_value;
		    
		    $this->load->view('theme/include/header',$data);
			$this->load->view('theme/account',$data);
			$this->load->view('theme/include/footer',$data);   
		   
   }
   public function upgrades()
   {  
	   $l_id=$this->App_model->authentication_login();

	   $userid=$this->session->userdata['loginuser']['userid'];
			
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		if($l_id=='0')
		{
			redirect('');
		}

		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$current_user_version    = $loginuserinfo[0]['_Version'];

		$current_version_result=$this->App_model->getdata('bn_versions','_ID',$current_user_version);

		$current_version_btcperday = $current_version_result[0]['_Btcperday'];

		$current_version_btcpermin = $current_version_result[0]['_Btcpermin']; 
			
		$data['cur_version_earningperday'] = $current_version_btcperday;

		$data['cur_version_earningpermin'] = $current_version_btcpermin;

		$data['minimum_paymnet_limit'] = 0.08;
		$payment_limit = 0.08;
		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
		/*
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$data['btc_earning'] = $earnings['_Amount'];

		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];
        
		


		if($btc == 'LTC')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');
         	/*$data['ltc_earning'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 10, '.', '');*/
			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_eth_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		/*	$data['eth_earning'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 10, '.', '');*/
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
			/*echo "<pre>";
		print_r($data);exit;*/
		$userid=$this->session->userdata['loginuser']['userid'];
		$networkID=$this->App_model->getDetail('bn_userinfo','_UserID');
		$query = $this->db->get_where('bn_userinfo', array('_UserID =' => $userid))->result();

		if(count($query) == 0) {
			
			
			$ip=$_SERVER["HTTP_CF_CONNECTING_IP"] ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER["REMOTE_ADDR"];

				$url = "http://ip-api.com/json/?ip=".$ip;
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$curlData = curl_exec($ch);
				/*echo "<pre>";
				print_r($curlData);
				echo "</pre>";*/
				$response_a = json_decode($curlData,true);
				/*echo "<pre>";
				print_r($response_a);echo "</pre>";
				exit;*/
				$userData=array(
								'_UserID'=>$userid,
								'_Ipaddress'=>$response_a['query'],
								'_ISP'=>$response_a['isp'],
								'_Country'=>$response_a['country'],
								'_CountryCode'=>$response_a['countryCode'],
								'_Timezone'=>$response_a['timezone'],
								'_Region'=>$response_a['regionName'],
								'_Zip'=>$response_a['zip'],

				 				);
				
				curl_close($ch);

				
				$data['insert']=$this->App_model->insertData('bn_userinfo',$userData);
			
						
		}else
		{
			/*$ip ='182.70.80.129';
			$ip=$_SERVER['REMOTE_ADDR'];*/
			$ip=$_SERVER["HTTP_CF_CONNECTING_IP"] ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER["REMOTE_ADDR"];

			$url = "http://ip-api.com/json/?ip=".$ip;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$curlData = curl_exec($ch);
			$response_a = json_decode($curlData,true);
			
			curl_close($ch);
		
		}
			
		  	$data['response_a']=$response_a;
		  	/*echo "<pre>";
		  	print_r($data);exit;*/
			$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
			$data['networkData']=$this->App_model->getdata('bn_userinfo','_UserID',$userid);
			
			if($this->input->post('email')){
				$updateEmail=array(
					'_Email'=>$this->input->post('email'));
				/*echo "<pre>";
				print_r($updateEmail);exit;*/
				$result=$this->App_model->update_data('bn_users','_ID',$userid,$updateEmail);
			}

			$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
			$data['upgrades']=$this->App_model->getdata('bn_upgrades','_UserID',$userid);
			$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);
			$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);
	   $l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['upgradesresult']=$this->App_model->getdata('bn_upgrades','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;
		
	    $this->load->view('theme/include/header',$data);
		$this->load->view('theme/upgrades',$data);
		$this->load->view('theme/include/footer',$data);   
	   
   }
   
   public function createpayto_finance()
   {
	  
		    error_reporting(0);
	        $data['img_url']="";
		
			$this->load->library('ciqrcode');
			$qr_image_btc=rand().'.png';
			$qr_image_ltc=rand().'.png';
			$qr_image_eth=rand().'.png';
			$userid=$this->session->userdata['loginuser']['userid'];
	        $loginuserinfo=$this->App_model->getdata('bn_users','_ID',$userid);
	     
	        $versionid=$loginuserinfo[0]['_Version'];
	 
			$versioninfo=$this->App_model->getdata('bn_versions','_ID',$versionid);
			 /*echo "<pre>";
			print_r($loginuserinfo);exit;*/
			$price=0.001;
		
	    $userid=$this->session->userdata['loginuser']['userid'];
        $loginuserinfo=$this->App_model->getdata('bn_users','_ID',$userid);
        $spare_address=$this->App_model->getdata('bn_spare_address','_Status','1');
        $paytoaddress=$spare_address[0]['_Address'];
	    $address=$loginuserinfo[0]['_Baddress'];
	    $payto=$loginuserinfo[0]['_Payto'];
	    
	    $this->load->library('guzzle');

	   	$client = new GuzzleHttp\Client();

	   	$url = 'http://localhost/miner_api/api.php/bn_spare_address/?filter[]=_UserID,eq,'.$address;
   		$response = $client->request( 'GET',  $url );
		    	//if($response->getStatusCode() == 200)
		$result = json_decode($response->getBody());

		/*echo "<pre>";
		print_r()*/
		//$update_address = $result->bn_spare_address->records[0][1];

		$update_address['btc'] = $result->bn_spare_address->records[0][1];
		$update_address['eth'] = $result->bn_spare_address->records[0][2];
		$update_address['ltc'] = $result->bn_spare_address->records[0][3];
		/*echo "<pre>";
		print_r($update_address);exit;*/
		    if(empty($result->bn_spare_address->records))
		    {
			     $urladdress='http://localhost/miner_api/api.php/bn_spare_address/?filter[]=_Status,eq,1&filter[]=_UserID,is,NULL&order=_ID&page=1,1';

				   	$getResponse=$client->request('GET',$urladdress);
					#guzzle repose for future use
				  
					$getResponse->getStatusCode(); // 200
					$getResponse->getReasonPhrase(); // OK
					$getResponse->getProtocolVersion(); // 1.1
					$getResponse->getBody();

					$final_result = json_decode($getResponse->getBody());
					/*echo "<pre>";
					print_r($final_result);exit;*/
					$update_id = $final_result->bn_spare_address->records[0][0];
					$update_address['btc'] = $final_result->bn_spare_address->records[0][1];
					$update_address['eth'] = $final_result->bn_spare_address->records[0][2];
					$update_address['ltc'] = $final_result->bn_spare_address->records[0][3];
					
					/*echo "<pre>";
			     	print_r($update_address);
			     	echo "</pre>";*/

					$update = array();
					$update['_ID'] = $update_id;
					$update['_Address'] = $update_address;
					$update['_UserID'] = $address;
					$update['_Status'] = 2;

					$update_url = "http://localhost/miner_api/api.php/bn_spare_address/".$update_id;
					
					$updatespare_address=array(
		          		'_Payto'=> json_encode($update_address)
		           	);



					
				  	$result=$this->App_model->update_data('bn_users','_Baddress',$address,$updatespare_address);
				   	$update_request = $client->request('PUT', $update_url, array('json' => [$update]));

			}
			else
			{
				$updatespare_address=array(
		          		'_Payto'=> json_encode($update_address)
		           	);
				/*echo "string";exit;*/
				 $result=$this->App_model->update_data('bn_users','_Baddress',$address,$updatespare_address);
			}

	
	     if(!empty($update_address))
         {
         	
			   
		 	$btc_priceAPI = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth";
	        $data1 = file_get_contents($btc_priceAPI);
			$result=json_decode($data1);
			$ethprice=number_format((float)$this->exp2dec($price/($result->result[0]->Last)), 10, '.', '');
			$data['eth_Convert']= $ethprice; 
		
			$ltc_priceAPI = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc";
	        $ltc_data = file_get_contents($ltc_priceAPI);
			$ltc_result=json_decode($ltc_data);
			$ltcprice=number_format((float)$this->exp2dec($price/($ltc_result->result[0]->Last)), 10, '.', '');
		
			$data['ltc_Convert']=$ltcprice; 
			/*	echo "<pre>";
			print_r($versioninfo[0]['_Price']);exit;*/
          	$url_btc = "bitcoin:".$update_address['btc']."?amount=".$price."&message=Deposit+to+".$_SERVER['HTTP_HOST']."+User+".$userid;

          	$url_ltc = "litecoin:".$update_address['ltc']."?amount=".$ltcprice."&message=Deposit+to+".$_SERVER['HTTP_HOST']."+User+".$userid;

          	$url_eth = "ethereum:".$update_address['eth']."?amount=".$ethprice."&message=Deposit+to+".$_SERVER['HTTP_HOST']."+User+".$userid;

		    $params_btc['data'] = $url_btc;
		    $params_ltc['data'] = $url_ltc;
		    $params_eth['data'] = $url_eth;

			$params_btc['level'] = 'H';
			$params_ltc['level'] = 'H';
			$params_eth['level'] = 'H';
			$params_btc['size'] = 8;
			$params_ltc['size'] = 8;
			$params_eth['size'] = 8;
			$params_btc['savename'] =FCPATH."assets/uploads/qr_image/".$qr_image_btc;
			$params_ltc['savename'] =FCPATH."assets/uploads/qr_image/".$qr_image_ltc;
			$params_eth['savename'] =FCPATH."assets/uploads/qr_image/".$qr_image_eth;
			/*echo "<pre>";
			print_r($params_eth);exit;*/

			if($this->ciqrcode->generate($params_btc))
			{
				$data['img_url_btc']=$qr_image_btc;
			}

			if($this->ciqrcode->generate($params_ltc))
			{	
				$data['img_url_ltc']=$qr_image_ltc;
			}

			if($this->ciqrcode->generate($params_eth))
			{
				$data['img_url_eth']=$qr_image_eth;
			}
       		$data['paytoaddress']=$update_address;
            $data['version']=$versioninfo[0]['_Version'];
            $data['price']=$versioninfo[0]['_Price'];
          
         }
         /*echo "<pre>";
         print_r($data);exit();*/
        
	    $this->load->view('theme/ajaxfinance_paytocreate',$data);
   }
   
   
   public function versionupgrad()
   { 
	        error_reporting(1);
	        ini_set('memory_limit', '-1');
	        $data['img_url']="";

			$this->load->library('ciqrcode');

			$qr_image_btc=rand().'.png';
			$qr_image_ltc=rand().'.png';
			$qr_image_eth=rand().'.png';

			if($_REQUEST['ver'] == 'ram'){
				$versionid=$this->input->get('versionid');
			}else{
				$versionid=$this->input->post('versionid');
			}
			
			
		    $userid=$this->session->userdata['loginuser']['userid'];
	        $loginuserinfo=$this->App_model->getdata('bn_users','_ID',$userid);

	        $spare_address=$this->App_model->getdata('bn_spare_address','_Status','1');
	        $paytoaddress=$spare_address[0]['_Address'];
		    $address=$loginuserinfo[0]['_Baddress'];

		    $versioninfo=$this->App_model->getdata('bn_versions','_ID',$versionid);	

		    /*echo '<pre>';
		    print_r($versioninfo);
		    echo '</pre>';exit;*/
	 		/*echo "<pre>";
	 		print_r($versioninfo);
	 		exit;*/
		    $payto=$loginuserinfo[0]['_Payto'];
			
			/*   
		    $this->load->library('guzzle');
		   	$client = new GuzzleHttp\Client();
		
			$url = 'http://localhost/miner_api/api.php/bn_spare_address/?filter[]=_UserID,eq,'.$address;
	   		$response = $client->request( 'GET',  $url );
			    
			$result = json_decode($response->getBody());*/
			if($payto != ''){

				$result = json_decode($payto,true);

				$update_address['btc'] = $result['btc'];
				$update_address['eth'] = $result['eth'];
				$update_address['ltc'] = $result['ltc'];
			}else{

				$result = $this->App_model->getPayto($address);

				if(empty($result))
			    {
				        $result2 = $this->App_model->getPayto('');

				        $update_id = $result2[0]['_ID'];

				        $update_address['btc'] = $result2[0]['_Address_BTC'];
						$update_address['eth'] = $result2[0]['_Address_ETH'];
						$update_address['ltc'] = $result2[0]['_Address_LTC'];

					   	/*$getResponse=$client->request('GET',$urladdress);
						#guzzle repose for future use
					  
						$getResponse->getStatusCode(); // 200
						$getResponse->getReasonPhrase(); // OK
						$getResponse->getProtocolVersion(); // 1.1
						$getResponse->getBody();

						$final_result = json_decode($getResponse->getBody());
				
						$update_id = $final_result->bn_spare_address->records[0][0];
						
						$update_address['btc'] = $final_result->bn_spare_address->records[0][1];
						$update_address['eth'] = $final_result->bn_spare_address->records[0][2];
						$update_address['ltc'] = $final_result->bn_spare_address->records[0][3];*/
					
						$update = array();

						$update['_ID'] = $update_id;
						$update['_UserID'] = $address;
						$update['_Status'] = 2;

						/*$update_url = "http://localhost/miner_api/api.php/bn_spare_address/".$update_id;*/
						
						$updatespare_address=array(
			          		'_Payto'=> json_encode($update_address)
			           	);
							

					  	$result=$this->App_model->update_data('bn_users','_Baddress',$address,$updatespare_address);
					  	
					  	$result=$this->App_model->update_data('bn_spare_address','_ID',$update_id,$update);
					  	/*$update_request = $client->request('PUT', $update_url, array('json' => [$update]));*/
					  
				}
				else
				{
					$updatespare_address=array(
			          		'_Payto'=> json_encode($update_address)
			           	);
				
					 $result=$this->App_model->update_data('bn_users','_Baddress',$address,$updatespare_address);
				}

			}
	
	     if(!empty($update_address))
         {
         	

         	$upgradsarray=array(
			    '_UserID'=>$userid,
				'_Version'=>$versioninfo[0]['_Version'],
				'_VersionID' => $versionid,
				'_Price'=>$versioninfo[0]['_Price'],
				'_Payto'=>json_encode($update_address),
				'_Status'=>'0',
				'_Datetime'=>date('Y-m-d H:i:s')
				); 
         
           

			$result_upd=$this->App_model->form_insertdata('bn_upgrades',$upgradsarray);	

			$btc_priceAPI = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth";
	        $data1 = file_get_contents($btc_priceAPI);
			$result=json_decode($data1);
			$ethprice=number_format((float)$this->exp2dec(($versioninfo[0]['_Price'])/($result->result[0]->Last)), 8, '.', '');
			$data['eth_Convert']=$ethprice; 

			$ltc_priceAPI = "https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc";
	        $ltc_data = file_get_contents($ltc_priceAPI);
			$ltc_result=json_decode($ltc_data);
			$ltcprice=number_format((float)$this->exp2dec(($versioninfo[0]['_Price'])/($ltc_result->result[0]->Last)), 8, '.', '');
			$data['ltc_Convert']=$ltcprice;  


			$update_prices_address=array(
          		'_LTCPrice'=> $ltcprice,
          		'_ETHPrice'=> $ethprice,
           	);

			$result=$this->App_model->update_data('bn_upgrades','_ID',$result_upd,$update_prices_address);

			//$price_btc= ($versioninfo[0]['_Price'])/($ltc_result->result[0]->Last);
			/*echo "<pre>";
			echo $price_btc;exit;*/
		 	$url_btc = "bitcoin:".$update_address['btc']."?amount=".$versioninfo[0]['_Price']."&message=Deposit+to+".$_SERVER['HTTP_HOST']."+User+".$userid;

          	$url_ltc = "litecoin:".$update_address['ltc']."?amount=".$ltcprice."&message=Deposit+to+".$_SERVER['HTTP_HOST']."+User+".$userid;

          	$url_eth = "ethereum:".$update_address['eth']."?amount=".$ethprice."&message=Deposit+to+".$_SERVER['HTTP_HOST']."+User+".$userid;

		    $params_btc['data'] = $url_btc;
		    $params_ltc['data'] = $url_ltc;
		    $params_eth['data'] = $url_eth;

			$params_btc['level'] = 'H';
			$params_ltc['level'] = 'H';
			$params_eth['level'] = 'H';
			$params_btc['size'] = 8;
			$params_ltc['size'] = 8;
			$params_eth['size'] = 8;
			$params_btc['savename'] =FCPATH."assets/uploads/qr_image/".$qr_image_btc;
			$params_ltc['savename'] =FCPATH."assets/uploads/qr_image/".$qr_image_ltc;
			$params_eth['savename'] =FCPATH."assets/uploads/qr_image/".$qr_image_eth;
/*			echo "<pre>";
			print_r($params_eth);exit;*/

			if($this->ciqrcode->generate($params_btc))
			{
				$data['img_url_btc']=$qr_image_btc;
			}

			if($this->ciqrcode->generate($params_ltc))
			{	
				$data['img_url_ltc']=$qr_image_ltc;
			}

			if($this->ciqrcode->generate($params_eth))
			{
				$data['img_url_eth']=$qr_image_eth;
			}
       
          
        
          $data['paytoaddress']=$update_address;
          $data['version']=$versioninfo[0]['_Version'];
          $data['price']=$versioninfo[0]['_Price'];
         /* echo "<pre>";
          print_r($data);exit;*/
         

        }
        
	   $this->load->view('theme/ajaxversinupgred',$data);   
   }
   
   
   public function withdrawamount()
   {
   		
   	   if($this->session->userdata['loginuser']['usertype']==1)
		    {
			    $btc = 'BTC';
			   // $limit = 0.005;
			}
			else if($this->session->userdata['loginuser']['usertype']==3)
			{
			    $btc = 'ETH';
			    //$limit = $data['min_withdraw_bal'][0];
			}else if($this->session->userdata['loginuser']['usertype']==2)
			{
			    $btc = 'LTC';
			    //$limit = $data['min_withdraw_bal'][1];
			}

			

		
	
	 $amount =$this->input->post('reqamount');
	 /*echo "<pre>";
	 echo $amount;exit;*/
	 $result=$this->App_model->getrecordswithlimit_desc('bn_wiithdrwal');
	 /*echo "<pre>";
	 print_r($result);
	 echo "</pre>";exit;*/
	 
	 if(!empty($result)){$withdrawalid = count($result)+1;}else{$withdrawalid = "1";}
	 /*echo "<pre>";
	 print_r($withdrawalid);
	 echo "</pre>";exit;*/

	   $userid=$this->session->userdata['loginuser']['userid'];
	   $loginuserinfo=$this->App_model->getdata('bn_users','_ID',$userid);
       $usertype = $loginuserinfo[0]['_Usertype'];
       $today = date("Y-m-d H:i:s");
        
	    $version = $loginuserinfo[0]['_Version'];
		$upgraded = $loginuserinfo[0]['_Upgraded'];
		$oldearning = $loginuserinfo[0]['_Oldbtc'];
		$rankey = $loginuserinfo[0]['_Payto'];
		$created = $loginuserinfo[0]['_Created'];

         if($usertype == "0")
         {
			 
			    if($version == "0")
			    { 

			    	$earningpermin = earning_p_min;

					if($btc == 'LTC')
					{
					 	$earningpermin= number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(earning_p_min)), 8, '.', '');	
					}

					if($btc == 'ETH')
					{
					 	$earningpermin= number_format((float)$this->exp2dec(parent::btc_to_eth_convert(earning_p_min)), 8, '.', '');
					}


					 

					 $to_time = strtotime($today);
					 $from_time = strtotime($created);
	                 $minute = round(abs($to_time - $from_time) / 60);

	                 $earnings = $minute * $earningpermin;
	                 $versiontxt = "v1.0";
	                 $earningperday =earning_p_day;

	                 if($btc == 'LTC')
					{
					 	$earningperday= number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(earning_p_day)), 8, '.', '');	
					}

					if($btc == 'ETH')
					{
					 	$earningperday= number_format((float)$this->exp2dec(parent::btc_to_eth_convert(earning_p_day)), 8, '.', '');
					}
		   			
				}
				else
				{
				     $versionresult=$this->App_model->getdata('bn_versions','_ID',$version); 
					 $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
					 $earningperday = $versionresult[0]['_Btcperday']; 	


					if($btc == 'LTC')
					{
					 	$earningpermin= number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($versionresult[0]['_Btcpermin'])), 8, '.', '');

					 	$earningperday = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($versionresult[0]['_Btcperday'])), 8, '.', '');	
					}

					if($btc == 'ETH')
					{
					 	$earningpermin= number_format((float)$this->exp2dec(parent::btc_to_eth_convert($versionresult[0]['_Btcpermin'])), 8, '.', '');

					 	$earningperday = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($versionresult[0]['_Btcperday'])), 8, '.', '');
					}

					  if($version == "1")
					  {
						  $from_time  = strtotime($upgraded);
						  $to_time    = strtotime($today);
						  $minute     = round(abs($to_time - $from_time) / 60);
						  $newearning = $minute * $earningpermin;
						  $earnings   = $newearning + $oldearning;  
						  $versiontxt = "v1.1";   
					  }
					  else if($version == "2")
					  {
						  $from_time  = strtotime($upgraded);
						  $to_time    = strtotime($today);
						  $minute     = round(abs($to_time - $from_time) / 60);
						  $newearning = $minute * $earningpermin;
						  $earnings   = $newearning + $oldearning;
						  $versiontxt = "v1.2";
						  
					  }
					  else if($version == "3")
					  {

						  $from_time  = strtotime($upgraded);
						  $to_time    = strtotime($today);
						  $minute     = round(abs($to_time - $from_time) / 60);
						  $newearning = $minute * $earningpermin;
						  $earnings   = $newearning + $oldearning;
						  $versiontxt = "v1.3";
	        
	                  }
	                  else if($version == "4")
	                  {

						  $from_time  = strtotime($upgraded);
						  $to_time    = strtotime($today);
						  $minute     = round(abs($to_time - $from_time) / 60);
						  $newearning = $minute * $earningpermin;
						  $earnings   = $newearning + $oldearning;
						  $versiontxt = "v1.4";
	        
	                  }
					
				}
			 
		 }
		 else
		 {
			           
			           
			        
			        /* $versionresult=$this->App_model->getdata('bn_versions','_ID',$version); 
			         
					 $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
					 $earningperday = $versionresult[0]['_Btcperday']; 
					 
					 $to_time = strtotime($today);
	                 $from_time = strtotime($created);
	                 $minute = round(abs($to_time - $from_time) / 60);
                     $earnings = $minute * $earningpermin; */
				  $earnings =$amount;
				  
				  
		 }
		 
		 
		 $earnings = number_format($earnings,8);
	     $checkearning = (float)$earnings;
	  	 /*$get_Min_withdraw_bal=$earnings;
	     if($btc == 'LTC')
	     {
	     	$get_Min_withdraw_bal = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	
	     }

	     if($btc == 'ETH')
	     {
	     	$get_Min_withdraw_bal = number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 
	     }*/
	
		

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$get_Min_withdraw_bal = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$get_Min_withdraw_bal = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($get_Min_withdraw_bal)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$get_Min_withdraw_bal = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($get_Min_withdraw_bal)), 8, '.', '');
		endif;

		$data['min_withdraw_bal'] = $get_Min_withdraw_bal;

		  /*echo "<pre>";
		  print_r($data['min_withdraw_bal']);exit;
*/
		  if($checkearning >= $get_Min_withdraw_bal)
		  {
			  
			   $withdrawarray=array(
			    '_UserID'=>$userid,
				'_WithID'=>$withdrawalid,
				'_Amount'=>$earnings,
				'_Source'=>'Withdraw',
				'_Status'=>'0',
				'_Datetime'=>date('Y-m-d H:i:s')
				); 
			/*	echo '<pre>';
				print_r($withdrawarray); exit;*/
			    $this->App_model->form_insertdata('bn_wiithdrwal',$withdrawarray);	
			    $res = array('result' => "success");
		  }
		  else
		  {
			  $res = array('result' => "Failed");
			  
		  }
		 
		echo json_encode($res);
		 
   }
   

   function exp2dec($number) 
	{
        $occurence = strpos($number,"E");

        if($occurence)
        {
			$char = '+';
			
			$pos = strpos($number, $char);
			
			if($pos == TRUE)
			{
				
				$float  = (float) $number;
					
				return $float;
			
			}else
			{
				
				preg_match('/(.*)E-(.*)/', str_replace(".", "", $number), $matches);
				
				$num = "0.";
				
				while ( $matches[2] > 0 ) 
				{
				
					$num .= "0";
				
					$matches[2]--;
				
				}
				
				$result =  $num . $matches[1];
			}
        }else
        {
        	$result = $number;
        }

        return $result;
    }
  	
  	public function leaderboard()
	{
		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];*/
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	

	}
			
				$tbname='bn';
				$q=$this->db->query("Select wl.*,Sum(wl._Amount)as total,us._Baddress from bn_wiithdrwal as wl left join bn_users as us on us._ID = wl._UserID WHERE wl._Taxid  IS NOT NULL AND
		 us._Baddress LIKE '3%'  OR
		 us._Baddress LIKE '1%' 

		group by us._Baddress 
		HAVING Sum(wl._Amount) < 2
		order by total desc limit 5");

				$ethdata=$this->db->query("Select wl.*,Sum(wl._Amount)as total,us._Baddress from bn_wiithdrwal as wl left join bn_users as us on us._ID = wl._UserID WHERE wl._Taxid  IS NOT NULL AND
		 us._Baddress LIKE '0x%'

		group by us._Baddress 
		HAVING Sum(wl._Amount) < 5
		order by total desc limit 5");

				$ltcdata=$this->db->query("Select wl.*,Sum(wl._Amount)as total,us._Baddress from bn_wiithdrwal as wl left join bn_users as us on us._ID = wl._UserID WHERE wl._Taxid  IS NOT NULL AND
		 us._Baddress LIKE 'L%'

		group by us._Baddress 
		HAVING Sum(wl._Amount) < 5
		order by total desc limit 5");

			    $data['result']=$q->result_array();
			   	$data['ltc']=$ltcdata->result_array();
			   	$data['eth']=$ethdata->result_array();
			   	/*echo "<pre>";
			   	print_r($data);
			   	exit;*/

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/leaderboard',$data);
		$this->load->view('theme/include/footer',$data);
	}

	public function visit()
	{
		$l_id=$this->App_model->authentication_login();

	   $userid=$this->session->userdata['loginuser']['userid'];
			
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		if($l_id=='0')
		{
			redirect('');
		}

		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$current_user_version    = $loginuserinfo[0]['_Version'];

		$current_version_result=$this->App_model->getdata('bn_versions','_ID',$current_user_version);

		$current_version_btcperday = $current_version_result[0]['_Btcperday'];

		$current_version_btcpermin = $current_version_result[0]['_Btcpermin']; 
			
		$data['cur_version_earningperday'] = $current_version_btcperday;

		$data['cur_version_earningpermin'] = $current_version_btcpermin;

		$data['minimum_paymnet_limit'] = 0.08;
		$payment_limit = 0.08;
		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
		/*
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$data['btc_earning'] = $earnings['_Amount'];

		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];
        
		


		if($btc == 'LTC')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');
         	/*$data['ltc_earning'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 10, '.', '');*/
			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_eth_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		/*	$data['eth_earning'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 10, '.', '');*/
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		
		
		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;
	    
	    $this->load->view('theme/include/header',$data);
		$this->load->view('theme/visits',$data);
		$this->load->view('theme/include/footer',$data); 
	}
	public function finance1()
	{

	   $l_id=$this->App_model->authentication_login();

	   $userid=$this->session->userdata['loginuser']['userid'];
			
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		if($l_id=='0')
		{
			redirect('');
		}

		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$current_user_version    = $loginuserinfo[0]['_Version'];

		$current_version_result=$this->App_model->getdata('bn_versions','_ID',$current_user_version);

		$current_version_btcperday = $current_version_result[0]['_Btcperday'];

		$current_version_btcpermin = $current_version_result[0]['_Btcpermin']; 
			
		$data['cur_version_earningperday'] = $current_version_btcperday;

		$data['cur_version_earningpermin'] = $current_version_btcpermin;

		$data['minimum_paymnet_limit'] = 0.08;
		$payment_limit = 0.08;
		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
		/*
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$data['btc_earning'] = $earnings['_Amount'];

		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];
        
		


		if($btc == 'LTC')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');
         	/*$data['ltc_earning'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 10, '.', '');*/
			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_eth_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		/*	$data['eth_earning'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 10, '.', '');*/
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		

		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['upgrades']=$this->App_model->getdata('bn_upgrades','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);
		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

	    
	    $this->load->view('theme/include/header',$data);
		$this->load->view('theme/finance',$data);
		$this->load->view('theme/include/footer',$data);   
	}
	
	public function general_setting()
	{
		$l_id=$this->App_model->authentication_login();

	   $userid=$this->session->userdata['loginuser']['userid'];
			
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		if($l_id=='0')
		{
			redirect('');
		}

		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$current_user_version    = $loginuserinfo[0]['_Version'];

		$current_version_result=$this->App_model->getdata('bn_versions','_ID',$current_user_version);

		$current_version_btcperday = $current_version_result[0]['_Btcperday'];

		$current_version_btcpermin = $current_version_result[0]['_Btcpermin']; 
			
		$data['cur_version_earningperday'] = $current_version_btcperday;

		$data['cur_version_earningpermin'] = $current_version_btcpermin;

		$data['minimum_paymnet_limit'] = 0.08;
		$payment_limit = 0.08;
		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
		/*
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$data['btc_earning'] = $earnings['_Amount'];

		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];
        
		


		if($btc == 'LTC')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');
         	/*$data['ltc_earning'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 10, '.', '');*/
			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['cur_version_earningpermin'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcpermin)), 8, '.', '');

			$data['cur_version_earningperday'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($current_version_btcperday)), 8, '.', '');

			$data['minimum_paymnet_limit'] =number_format((float)$this->exp2dec(parent::btc_to_eth_convert($payment_limit)), 10, '.', '');

			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		/*	$data['eth_earning'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 10, '.', '');*/
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
			/*echo "<pre>";
		print_r($data);exit;*/
		
		if($this->input->post('email')){
			$updateEmail=array(
				'_Email'=>$this->input->post('email'));
			/*echo "<pre>";
			print_r($updateEmail);exit;*/
			$result=$this->App_model->update_data('bn_users','_ID',$userid,$updateEmail);
		}

		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['upgrades']=$this->App_model->getdata('bn_upgrades','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);
		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;
	    
	    $this->load->view('theme/include/header',$data);
		$this->load->view('theme/general-setting',$data);
		$this->load->view('theme/include/footer',$data);  
	}





}
