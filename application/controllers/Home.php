<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

public $btc = '';

function __construct() { 
			error_reporting(0);
            parent::__construct(); 
         
           $this->load->helper(array('form', 'url'));
           $this->load->helper('url');
           $this->load->library('form_validation');
           $this->load->database(); 
           $this->load->library('session');
           $this->load->model('App_model');
           $this->load->library('encrypt');
           /*parent::check_cookie();*/
           /*parent::get_different_coins();*/

            if($this->session->userdata['loginuser']['usertype']==1){
			    $this->btc = 'BTC';
			    $usertype1='Bitcoin';
			} else if($this->session->userdata['loginuser']['usertype']==2) {
			    $this->btc = 'LTC';
			     $usertype1='Litcoin';
			} else if($this->session->userdata['loginuser']['usertype']==3) {
			    $this->btc = 'ETH';
			    $usertype1='Ethereum';
			}
         
           } 


	public function index()
	{
		
    	if(isset($this->session->userdata['loginuser']))
    	{
			
			redirect('Account/dashboard');

		}else
		{
			
			error_reporting(0);
			/*$data['rate_ltc']= parent::header_price_convert();*/
			$data['resultbn_versions']=$this->App_model->getrecords('bn_versions');
			
			$this->load->view('theme/include/header');
			$this->load->view('theme/dashboard',$data);
			$this->load->view('theme/include/footer');
		}
	}
	
	public function affiliate()
	{

		$this->load->view('theme/include/header');
		$this->load->view('theme/affiliate-1');
		$this->load->view('theme/include/footer');
	}
	/*
	public function refresh_page()
	{

		$post = $this->input->post();
		echo '<pre>';
		print_r($post);
		echo '</pre>';

		$data['resultbn_versions']=$this->App_model->getrecords('bn_versions');
			
		$this->load->view('theme/include/header');
		$this->load->view('theme/dashboard',$data);
		$this->load->view('theme/include/footer');
	}
	*/
	public function faq()
	{	

		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];*/
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
		
		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;


			$this->load->view('theme/include/header',$data);
			$this->load->view('theme/faq',$data);
			$this->load->view('theme/include/footer',$data);
			return;
	}
		
		$this->load->view('theme/include/header');
			$this->load->view('theme/faq');
			$this->load->view('theme/include/footer');
			
			
		

	}

	public function payouts()
	{  
		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		$rate = earning_p_min;
		$per_day=earning_p_day;
		  
		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
	/*	
		$earnings = parent::earnings();		

		$data['earnings_diff_coins'] = $earnings['_Amount'];*/
	/*	echo "<pre>";
		print_r($data);exit;*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];

        $data['user_earningparday_version'] = $earningperday;
        $data['user_earningpermin_version'] = $earningpermin;

		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	/*$data['earnings_diff_coins'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');*/

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	/*$data['earnings_diff_coins']= number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');*/

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		/*$data['earnings_diff_coins'] = $earnings_diff_coin;*/
		
		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

	   $this->load->view('theme/include/header',$data);
		$this->load->view('theme/payout',$data);
		$this->load->view('theme/include/footer',$data);
		return;
		}


		

		$data['payoutsresult']=$this->App_model->getdata('bn_wiithdrwal','_Status','2');
		/*echo "<pre>";
		print_r($data);exit;*/
		$this->load->view('theme/include/header');
		$this->load->view('theme/payout',$data);
		$this->load->view('theme/include/footer');
		
	}

    public function logout()
   {
	       $logindata = array(
           'userid'=>'',
					     'baddress'=>'',
					      'usertype'=>''
           );
         /*  delete_cookie('userid'); */
           $this->session->unset_userdata('loginuser', $logindata); 
	        /*$this->session->sess_destroy();*/
	       redirect('');
   }
	
    public  function review()
    {
    	if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];

		$rate = earning_p_min;
		$per_day=earning_p_day;
		  
		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		

		$data['earnings_diff_coins'] = $earnings['_Amount'];
	
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];

        $data['user_earningparday_version'] = $earningperday;
        $data['user_earningpermin_version'] = $earningpermin;

		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	$data['earnings_diff_coins'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	$data['earnings_diff_coins']= number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
	}
		$offset11=$this->input->post('offset');
		$review=$this->App_model->getdata('bn_reviews','_Status','1');

		 $countres=count($review);
		
		  if( $offset11 < $countres)
		  {
			  
			     if($offset11=='0')
		         {
			     $offset='0'; 
		         }
				 else
				 {
					$offset=$offset11;  
				 }
				  $data['review']=$this->App_model->getaccountdatawithlimitoffset('bn_reviews','_Status','1',$offset);
				  /*echo "<pre>";
				  print_r($data);
				  exit;*/
		  }
		  else
		  {
		  	$data['errmsg']="no more records Found !";  
		  }
	 /*  $data['review']=$this->App_model->getdata('bn_reviews','_Status','1');*/
		/*echo "<pre>";
		print_r($data);exit;*/

			$current_user_version    = $loginuserinfo[0]['_Version'];
			$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
			$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

			if(array_key_exists($current_user_version, $w_limits)){
				$min_value = $w_limits[$current_user_version][0];
			}

			if($this->btc == "ETH"):
				$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
			elseif($this->btc == "LTC"):
				$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
			endif;

    	    $this->load->view('theme/include/header',$data);
			$this->load->view('theme/reviews',$data);
			$this->load->view('theme/include/footer',$data);
    }
    public function AddReview()
    {	
		if($this->input->post('g-recaptcha-response') && !empty($this->input->post('g-recaptcha-response'))){

		$captcha = $this->input->post('g-recaptcha-response');
		$secretKey = '6LeAiVIUAAAAANpGz_g8EIqDHtGsCPop8aKUMevT';
		
		$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$captcha);
        $response = json_decode($verifyResponse,true);
      
		
        if($response['success'] == 0){
          echo "<script type='text/javascript'>alert('Please Enter Valid captcha.');</script>";
				exit;
        }
        else{


    	$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

    	$reviewdata=array(

    		'_UserID'=>$userid,
    		'_Name'=>$this->input->post('name'),
    		'_Rating'=>$this->input->post('star'),
    		'_Title'=>$this->input->post('subject'),
    		'_Description'=>$this->input->post('comment'),
    		'_Created'=>date('Y-m-d H:i:s'),
    	);
	  		
		 $sql_query=$this->App_model->form_insertdata('bn_reviews',$reviewdata);
		
		 if($sql_query){
			
			redirect('reviews');
			/*echo "<script type='text/javascript'>alert('Please Login .');</script>";*/
		}
		else{
			$this->session->set_flashdata('error', 'Somthing worng. Error!!');
			redirect('reviews');
		}
		
       }
      }
      
      	else{
		echo "<script type='text/javascript'>alert('Please Enter Valid captcha.');</script>";
		exit;
		}
      


    }
	
	function validate_captcha() {
      
      	/* captcha validation start  */
		$recaptcha = trim($this->input->post('g-recaptcha-response'));
        $userIp= $this->input->ip_address();
        $secret=secretkey;
        $data = array(
            'secret' => "$secret",
            'response' => "$recaptcha",
            'remoteip' =>"$userIp"
        );

        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($verify);
        $status= json_decode($response, true);
        if(empty($status['success'])){
            return FALSE;
        }else{

            return TRUE;
        }

    }

	public function contact()
	{  
		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	}
		
		error_reporting(0);
		 $errmessage="";
		 $this->form_validation->set_rules('name','name', 'required');
		 $this->form_validation->set_rules('email','email', 'required');
		 $this->form_validation->set_rules('subject','subject','required');
		 $this->form_validation->set_rules('message','message', 'required');
		
		 
		  if ($this->form_validation->run() == FALSE)
		     {
				  if(validation_errors())
				  {
				  $data['errmessage']=validation_errors();
				   //print_r( $data['errmessage']); die;
				 
				  }
				  else
				  { 
				  $data['errmessage']=validation_errors();
				  }
			 } else {

			if($this->input->post('g-recaptcha-response') && !empty($this->input->post('g-recaptcha-response'))){

				$captcha = $this->input->post('g-recaptcha-response');
				$secretKey = '6LeAiVIUAAAAANpGz_g8EIqDHtGsCPop8aKUMevT';
				
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$captcha);
		        $response = json_decode($verifyResponse,true);
		      
				
		        if($response['success'] == 0){
		        	$data['succmsg']="Please try again after some time.";
		          //redirect('contact');
		        }
		        else{


		    	$contactarr=array(
					'name'=>$this->input->post('name'),
					'email'=>$this->input->post('email'),
					'subject'=>$this->input->post('subject'),
					'message'=>$this->input->post('message'),
					'created'=>date('Y-m-d H:i:s'),
					'ip'=>$this->input->ip_address()
					); 	
				 	$varibles=$this->App_model->getdata('bn_variables','_Name','supportmail');

					$body = $this->load->view('theme/email/contact.php','',TRUE);
				
					/*$config = array();
			        $config['useragent']           = 'CodeIgniter';
			        $config['mailpath']            = '/usr/bin/sendmail'; // or "/usr/sbin/sendmail"
			        $config['protocol']            = 'mail';
			        $config['smtp_host']           = 'localhost';
			        $config['smtp_port']           = '25';
			        $config['mailtype'] = 'html';
			        $config['charset']  = 'iso-8859-1';
			        $config['newline']  = '\r\n';
			        $config['wordwrap'] = TRUE;*/

			        $this->load->library('email');

			       // $this->email->initialize($config);
			        $this->email->set_newline("\r\n"); 

					$toEmail=$this->input->post('email');
					$fromEmail=$varibles[0]['_Value'];
					$fromName= $sitename[0]['_Value'];
					
					$this->email->from($fromEmail);
					$this->email->set_mailtype("html");
					$this->email->to($fromEmail);
					$this->email->reply_to($toEmail);
					/*$this->email->to('demotech0@gmail.com');*/

					$this->email->subject('ContactUs: '.$this->input->post('email'));
					$this->email->message($body);
					$this->email->send();
	            	$data['succmsg']="Your message has been sent. Thank you!";			
		       }
		    }else{
				//echo "<script type='text/javascript'>alert('Please Enter Valid captcha.');</script>";
				$data['succmsg']="Please Enter Valid captcha.";
				//redirect('contact');
			}	
			}
		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header');
		$this->load->view('theme/contact',$data);
		$this->load->view('theme/include/footer');
	}
	/*public function homepage()
	{
		$this->load->view('theme/include/header');
		$this->load->view('theme/home');
		$this->load->view('theme/include/footer');
	}*/

	public function ajaxgetpayouts()
	{
		/*echo "hi";*/
		 $offset11=$this->input->post('offset');
		
		  
		 
		  $withresult=$this->App_model->getdata('bn_wiithdrwal','_Coin_Type','1');

		  $countres=count($withresult);
		
		  if( $offset11 < $countres)
		  {
			  
			     if($offset11=='0')
		         {
			     $offset='0'; 
		         }
				 else
				 {
					$offset=$offset11;  
				 }
		  $data['payoutsresult']=$this->App_model->getdatawithlimitoffset('bn_wiithdrwal','_Coin_Type','1',$offset);
		/*  echo "<pre>";
		  print_r($data);exit;*/
		  //echo count($data['payoutsresult']);
	      }
	      else
	      {
			$data['errmsg']="no more records Found !";  
		  }
	
		$this->load->view('theme/ajaxpayouts',$data);
		
		
	}
	public function ajaxgetpayoutsLtc()
	{
		/*echo "hi";*/
		 $offset11=$this->input->post('offsetlt');
		
		  
		 
		  $withresult=$this->App_model->getdata('bn_wiithdrwal','_Coin_Type','2');

		  $countres=count($withresult);
		
		  if( $offset11 < $countres)
		  {
			  
			     if($offset11=='0')
		         {
			     $offset='0'; 
		         }
				 else
				 {
					$offset=$offset11;  
				 }
		  $data['payoutsresult']=$this->App_model->getdatawithlimitoffset('bn_wiithdrwal','_Coin_Type','2',$offset);
		/*  echo "<pre>";
		  print_r($data);exit;*/
		  //echo count($data['payoutsresult']);
	      }
	      else
	      {
			$data['errmsg']="no more records Found !";  
		  }
		
		$this->load->view('theme/ajaxpayoutsltc',$data);
		
		
	}

	public function ajaxgetpayoutseth()
	{
		/*echo "hi";*/
		 $offset11=$this->input->post('offseteth');
		
		  
		 
		  $withresult=$this->App_model->getdata('bn_wiithdrwal','_Coin_Type','3');

		  $countres=count($withresult);
		
		  if( $offset11 < $countres)
		  {
			  
			     if($offset11=='0')
		         {
			     $offset='0'; 
		         }
				 else
				 {
					$offset=$offset11;  
				 }
		  $data['payoutsresult']=$this->App_model->getdatawithlimitoffset('bn_wiithdrwal','_Coin_Type','3',$offset);
		/*  echo "<pre>";
		  print_r($data);exit;*/
		  //echo count($data['payoutsresult']);
	      }
	      else
	      {
			$data['errmsg']="no more records Found !";  
		  }
	
		$this->load->view('theme/ajaxpayoutseth',$data);
		
		
	}

	public function account()
	{
		$this->load->view('theme/include/header');
		$this->load->view('theme/account');
		$this->load->view('theme/include/footer');
	}
	public function affilate1()
	{
		$l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);

		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);
/*			echo "<pre>";
		print_r($data);exit;*/

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;
	     
		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/affiliate',$data);
		$this->load->view('theme/include/footer',$data);	
	}
	public function promo_banner_btc()
	{
		$l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);

		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/promo-banner-btc',$data);
		$this->load->view('theme/include/footer',$data);
	}
	public function promo_banner_ltc()
	{
		$l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);

		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;


		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/promo-banner-ltc',$data);
		$this->load->view('theme/include/footer',$data);
	}
	public function promo_banner_eth()
	{
		$l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);

		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;


		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/promo-banner-eth',$data);
		$this->load->view('theme/include/footer',$data);	
	}
	public function commissionReport()
	{
		$l_id=$this->App_model->authentication_login();
		if($l_id=='0')
		{
			redirect('');
		}
		
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		/*$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		*/
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}		
			 

		

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
		$userid=$this->session->userdata['loginuser']['userid'];
		$data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		$data['financeresult']=$this->App_model->getdata('bn_wiithdrwal','_UserID',$userid);
		$data['refferalresult']=$this->App_model->getdata('bn_referral','_UserID',$userid);

		$data['visitesresult']=$this->App_model->getdata('bn_visits','_UserID',$userid);
	/*	echo "<pre>";
		print_r($data);exit;*/

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/commission-report',$data);
		$this->load->view('theme/include/footer',$data);
	}

	public function ajaxgetreview()
	{
		/*echo "hi";*/
		 $offset11=$this->input->post('offsetreview');
		
		  
		 
		  $withresult=$this->App_model->getdata('bn_reviews','_Status','1');

		  $countres=count($withresult);
		
		  if( $offset11 < $countres)
		  {
			  
			     if($offset11=='0')
		         {
			     $offset='0'; 
		         }
				 else
				 {
					$offset=$offset11;  
				 }
		  $data['review']=$this->App_model->getaccountdatawithlimitoffset('bn_reviews','_Status','1',$offset);
		/*  echo "<pre>";
		  print_r($data);exit;*/
		  //echo count($data['payoutsresult']);
	      }
	      else
	      {
			$data['errmsg']="no more records Found !";  
		  }
	
		$this->load->view('theme/ajaxreviewdata',$data);
		
		
	}

	
public function about()
	{

		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
	    $current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/about',$data);
		$this->load->view('theme/include/footer',$data);
	}

	$this->load->view('theme/include/header');
	$this->load->view('theme/about');
	$this->load->view('theme/include/footer');
}

	public function cookie_policy()
	{

		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
	    $current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/cookie_policy',$data);
		$this->load->view('theme/include/footer',$data);
	}

	$this->load->view('theme/include/header');
	$this->load->view('theme/cookie_policy');
	$this->load->view('theme/include/footer');
}
	public function terms_condition()
	{

		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
	    $current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/terms_condition',$data);
		$this->load->view('theme/include/footer',$data);
	}

	$this->load->view('theme/include/header');
	$this->load->view('theme/terms_condition');
	$this->load->view('theme/include/footer');
}

	public function disclaimer()
	{

		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
	    $current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/disclaimer',$data);
		$this->load->view('theme/include/footer',$data);
	}

	$this->load->view('theme/include/header');
	$this->load->view('theme/disclaimer');
	$this->load->view('theme/include/footer');
}

	public function policy()
	{

		if(isset($this->session->userdata['loginuser']['userid']))
		{
		if($this->session->userdata['loginuser']['usertype']==1){
		    $btc = 'BTC';
		    $usertype1='Bitcoin';
		}
		else if($this->session->userdata['loginuser']['usertype']==2)
		{
		    $btc = 'LTC';
		     $usertype1='Litcoin';
		 	
		} 
		else if($this->session->userdata['loginuser']['usertype']==3)
		{
		    $btc = 'ETH';
		    $usertype1='Ethereum';
		}

		$userid=$this->session->userdata['loginuser']['userid'];
		$b_address=$this->session->userdata['loginuser']['baddress'];

	   $data['loginuserinfo']=$this->App_model->getdata('bn_users','_ID',$userid);
		
		$loginuserinfo = $data['loginuserinfo'];


		$rate = earning_p_min;
		$per_day=earning_p_day;
		  

		$data['rate'] = $rate;
		$data['eth_convert']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		$data['ltc_per_min']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');
		$data['eth_per_day']=number_format((float)$this->exp2dec(parent::btc_to_eth_convert($per_day)), 8, '.', '');
		$data['ltc_per_day']=number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($per_day)), 8, '.', '');
		
		$earnings = parent::earnings();		
	
		$data['earnings_diff_coins'] = $earnings['_Amount'];
		
		$get_Min_withdraw_bal_model = 0.005;
		
	    $user_version = $loginuserinfo[0]['_Version'];
	    $versionresult=$this->App_model->getdata('bn_versions','_ID',$user_version); 
        $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
        $earningperday = $versionresult[0]['_Btcperday'];



		if($btc == 'LTC')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(0.005)), 8, '.', '');	

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($rate)), 8, '.', '');

		}

		/* btc version price price in eth */

		if($btc == 'ETH')
		{
			$data['user_earningparday_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningperday)), 10, '.', '');

         	$data['user_earningpermin_version'] = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earningpermin)), 10, '.', '');

         	$get_Min_withdraw_bal_model =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert(0.005)), 8, '.', ''); 

         	//$earnings_diff_coin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($earnings['_Amount'])), 8, '.', '');

			$data['rate'] =  number_format((float)$this->exp2dec(parent::btc_to_eth_convert($rate)), 8, '.', '');
		}

        $data['user_earningparday_version'] = $earningperday;

        $data['user_earningpermin_version'] = $earningpermin;
	    $data['min_withdraw_bal'] = $get_Min_withdraw_bal_model;
		//$data['earnings_diff_coins'] = $earnings_diff_coin;
	
	    $current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		$current_user_version    = $loginuserinfo[0]['_Version'];
		$withdraw_limits = $this->App_model->getdata('bn_withdraw_limit', '_ID', 1);
		$w_limits = json_decode($withdraw_limits[0]['_WithdrawLimits']);

		if(array_key_exists($current_user_version, $w_limits)){
			$min_value = $w_limits[$current_user_version][0];
		}

		if($this->btc == "ETH"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_eth_convert($min_value)), 8, '.', '');
		elseif($this->btc == "LTC"):
			$min_value = number_format((float) $this->exp2dec(parent::btc_to_ltc_convert($min_value)), 8, '.', '');
		endif;

		$data['min_value'] = $min_value;

		$this->load->view('theme/include/header',$data);
		$this->load->view('theme/policy',$data);
		$this->load->view('theme/include/footer',$data);
	}

	$this->load->view('theme/include/header');
	$this->load->view('theme/policy');
	$this->load->view('theme/include/footer');
}	

	
}
