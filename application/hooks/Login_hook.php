<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_hook 
{
    private $CI;
    private $db;
    private $client;
 
    public function __construct(){
        error_reporting(0);
        $this->CI =& get_instance();
      
        $this->CI->load->library('guzzle/Guzzle');
        $this->db = $this->CI->db;

        $this->client = new GuzzleHttp\Client(['verify' => false ]);
    }    
	  
    public function version_check()
    {
      error_reporting(0);

      if(get_class($this->CI) != 'Account') return;

      if($this->CI->is_hookable){
          
          $cur_usr_id =  $this->CI->session->userdata('loginuser')['userid'];
          
          $seldata = "select b._ID AS userid,bn._ID As upgradeid,bn._Version,bn._VersionID,bn._LastPayPrice,bn.`_Payto`,bn.`_Price`,b._Baddress,b._Coin_Type,bn.`_Datetime`,bn._Status, bn._PayStatus as pay_status from bn_upgrades As bn JOIN bn_users AS b ON bn.`_UserID`=b.`_ID` where b._ID = $cur_usr_id AND bn._PayStatus = '0' ORDER BY bn._Datetime DESC";

          $res = $this->db->query($seldata)->result();

          if(count($res) < 1){
            return;
          }
          
          foreach ($res as $value) {

            $price = $value->_Price;
            $version = $value->_Version;
            $version_id = $value->_VersionID;
            $last_pay = $value->_LastPayPrice;
            $today  = date("Y-m-d H:i:s");

            $ethprice = $value->_ETHPrice;
            $ltc_price = $value->_LTCPrice;
            $payto = $value->_Payto;
            $paytobal = json_decode($payto);

            switch($version){
              case 'Tire1' :
                $version_id = '1';
                break;
              case 'Tire2' :
                $version_id = '2';
                break;
              case 'Tire3' :
                $version_id = '3';
                break;
              case 'Tire4' :
                $version_id = '4';
                break;
              default :
                $version_id = '0';
            }

            foreach ($paytobal as $k => $v) {


              $result = $this->getBalance($v, $k);
              
              $result = ($result) && (strlen($result) > 8) ? substr($result,0,8) : $result;

              if($result > 0){
                $totalbtc = $result/100000000;

                $qry = "SELECT * FROM bn_users WHERE _ID = '$value->userid'";
                $f = $this->db->query($qry)->result();

                $upgraded = $f[0]->_Upgraded;
                $oldbtc   = ($f[0]->_Oldbtc != null && $f[0]->_Oldbtc > 0) ? number_format($f[0]->_Oldbtc,8) : 0;
                $uid    = $f[0]->_ID;
                $to_time  = strtotime($today);
                $getversion = $f[0]->_Version;
                //echo $version;

                if($getversion == '0' && $upgraded == '0000-00-00 00:00:00' && $oldbtc == null){
                //  echo "hi";
                  $from_time = strtotime($f[0]->_Created);
                  $minute = round(abs($to_time - $from_time) / 60);
                  $minval = $minute * earning_p_min;
                } else {

                  $selear = "Select * from bn_versions where _ID = '".$getversion."'";
                  $rowear = $this->db->query($selear)->result();
                  $earningpermin = $rowear[0]->_Btcpermin;

                  $from_time = strtotime($f[0]->_Upgraded);
                  $minute = round(abs($to_time - $from_time) / 60);
                  $minval = $minute * earning_p_min;
                }
                $newbtc = $oldbtc + $minval;
                $newbtc = number_format($newbtc,8);


                if($k == "eth"){
                  //$price = number_format($this->btc_to_eth_convert($price),8);
                  $main_total = $last_pay + $ethprice;

                    if( $totalbtc >= $main_total ){
                        $updsta = "update bn_upgrades set _Status = '1', _PayStatus = '1', _LastPayPrice = $main_total where  _VersionID = '$version_id' AND _UserID = '$value->userid' ";
                        $this->db->query($updsta);
                        
                        $updqry = "UPDATE bn_users SET  _Oldbtc = '".$newbtc."', _Version = '".$version_id."' , _Upgraded = '".$today."' WHERE _ID = '$value->userid'";
                        $this->db->query($updqry);
                    }
                }
                else if($k == "ltc"){

                  //$ltc_price = number_format($this->btc_to_ltc_convert($price),8);
                  $main_total = $last_pay + $ltc_price;
                  
                  if( $totalbtc >= $main_total ){
                      $updsta = "update bn_upgrades set _Status = '1', _PayStatus = '1', _LastPayPrice = $main_total where  _VersionID = '$version_id' AND _UserID = '$value->userid' ";
                      $this->db->query($updsta);
                      
                      $updqry = "UPDATE bn_users SET _Oldbtc = '".$newbtc."', _Version = '".$version_id."' , _Upgraded = '".$today."' WHERE _ID = '$value->userid'";
                      $this->db->query($updqry);
                  }
                }else{
                  $price = number_format($price,8);
                  $main_total = $last_pay + $price;

                    if( $totalbtc >= $main_total ){
                        $updsta = "update bn_upgrades set _Status = '1', _PayStatus = '1', _LastPayPrice = $main_total where  _VersionID = '$version_id' AND _UserID = '$value->userid' ";
                        $this->db->query($updsta);
                        
                        $updqry = "UPDATE bn_users SET _Oldbtc = '".$newbtc."', _Version = '".$version_id."' , _Upgraded = '".$today."' WHERE _ID = '$value->userid'";
                        $this->db->query($updqry);
                    }
                } 
              }
            }
          }
      }
    }

    public function btc_to_ltc_convert($value) {
        error_reporting(0);
        /*$ltc_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc');*/
        $response = $this->client->request( 'GET',  'https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc' );
        $result = json_decode($response->getBody());
        $ltc_price=$result->result[0]->Last;
        $get_ltc = $value / $ltc_price;
        return $get_ltc;
    }

    public function btc_to_eth_convert( $value ){
        error_reporting(0);
        /*$eth_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth');*/
        $response = $this->client->request( 'GET',  'https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth' );
        $result = json_decode($response->getBody()); 
        $eth_price=$result->result[0]->Last; 
        $get_eth = $value / $eth_price;
        return $get_eth;
    }

    public function getBalance($address,$type="btc"){
      error_reporting(0);
      // https://api.blockcypher.com/v1/eth/main/addrs/0x66584824c0645ca90568263c9881d11de07215e2/balance
      $type = $type ? $type : "btc";

      if($type == "btc"){
        $url = "https://blockchain.info/rawaddr/$address";
      }else{
        $url = "https://api.blockcypher.com/v1/$type/main/addrs/$address/balance";
      }
      $response = $this->client->request( 'GET',  $url );
      $result = json_decode($response->getBody());
      if($result){
        return $result->total_received;  
      }else{
        return 0;
      }

    }

    public function exp2dec($number) 
    {
        $occurence = strpos($number,"E");
        if($occurence){
          $char = '+';
          $pos = strpos($number, $char);   

          if($pos == TRUE) {
            $float  = (float) $number;
            return $float;
          }else {
            preg_match('/(.*)E-(.*)/', str_replace(".", "", $number), $matches);
            $num = "0.";
            while ( $matches[2] > 0 ){
              $num .= "0";
              $matches[2]--;
            }
            $result =  $num . $matches[1];
          }
        }else{
          $result = $number;
        }
        return $result;
    }
}
?>