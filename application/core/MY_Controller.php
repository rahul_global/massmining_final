<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller
{
  function __construct() { 
        parent::__construct(); 
        $this->load->database(); 
        $this->load->library('session');
        /*if($this->session->userdata('loginuser')){
             $this->session->unset_userdata('loginuser');
        }*/
         //$this->load->helper('cookie');
    } 


    /*public function get_different_coins(){
/*
      $cookie = array('name'=>'demo','value'=>'testing','domain'=>'192.168.5.192');
      $this->input->set_cookie($cookie);*/
      /*echo '<pre>';
      print_r($this->session->all_userdata());
      print_r($this->session->loginuser['type']);
      echo '</pre>';

       $eth_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth');
        $response_eth=json_decode($eth_url); 
        $eth_price=$response_eth->result[0]->Last; 
        $data['BTC_ApiPrice_for_ETH'] = $eth_price;
        

        $ltc_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc');
        $response=json_decode($ltc_url);
          $ltc_price=$response->result[0]->Last;
          $data['BTC_ApiPrice_for_LTC'] = $ltc_price;
        
          $prices = array('BTC_LTC'=>$data['BTC_ApiPrice_for_LTC'],'BTC_ETH'=>$data['BTC_ApiPrice_for_ETH']);


      return $prices;


    }*/


    public function btc_to_ltc_convert($value)
    {
        
        //$get_price = $this->get_different_coins();
        $ltc_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc');
        $response=json_decode($ltc_url);
        $ltc_price=$response->result[0]->Last;
        
        $get_ltc = $value / $ltc_price;
        
        return $get_ltc;

    }


    public function btc_to_eth_convert( $value )
    {

//        $get_price = $this->get_different_coins();
        $eth_url = file_get_contents('https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-eth');
        $response_eth=json_decode($eth_url); 
        $eth_price=$response_eth->result[0]->Last; 
        $get_eth = $value / $eth_price;
        return $get_eth;

    }


   /*public function header_price_convert()
   {
      $static_price = 0.000092;

      $multicoin_price=0.00000005;

      $Balance_Price=0.01110092;

      $header_value = array(
        'BTC_LTC'=>array(
          number_format((float)$this->exp2dec($this->btc_to_ltc_convert($static_price)), 8, '.', ''),
          number_format((float) $this->exp2dec($this->btc_to_eth_convert($static_price)), 8, '.', ''),
          number_format((float) $this->exp2dec($this->btc_to_ltc_convert($multicoin_price)), 8, '.', ''),
          number_format((float) $this->exp2dec($this->btc_to_eth_convert($multicoin_price)), 8, '.', ''),
          number_format((float)$this->exp2dec($this->btc_to_ltc_convert($Balance_Price)), 8, '.', ''),
          number_format((float) $this->exp2dec($this->btc_to_eth_convert($Balance_Price)), 8, '.', '')
         ));


      return $header_value;
   }*/

    function exp2dec($number) 
  {
        $occurence = strpos($number,"E");

        if($occurence)
        {
      $char = '+';
      
      $pos = strpos($number, $char);
      
      if($pos == TRUE)
      {
        
        $float  = (float) $number;
          
        return $float;
      
      }else
      {
        
        preg_match('/(.*)E-(.*)/', str_replace(".", "", $number), $matches);
        
        $num = "0.";
        
        while ( $matches[2] > 0 ) 
        {
        
          $num .= "0";
        
          $matches[2]--;
        
        }
        
        $result =  $num . $matches[1];
      }
        }else
        {
          $result = $number;
        }

        return $result;
    }


    public function earnings()
    {


            if($this->session->userdata['loginuser']['usertype']==1){
                $btc = 'BTC';
                $usertype1='Bitcoin';
            }
            else if($this->session->userdata['loginuser']['usertype']==3)
            {
                $btc = 'ETH';
                $usertype1='Ethereum';

               
            }else if($this->session->userdata['loginuser']['usertype']==2)
            {
                $btc = 'LTC';
                 $usertype1='Litcoin';
                
            } 

   $result=$this->App_model->getrecordswithlimit_desc('bn_wiithdrwal');
   $amount=$result[0]['_Amount'];
   /*echo "<pre>";
   print_r($earnings);exit;*/
   
   if(!empty($result)){$withdrawalid = count($result)+1;}else{$withdrawalid = "1";}
          
            $userid=$this->session->userdata['loginuser']['userid'];
             $loginuserinfo=$this->App_model->getdata('bn_users','_ID',$userid);
               $usertype = $loginuserinfo[0]['_Usertype'];
               $today = date("Y-m-d H:i:s");
                
                     $version = $loginuserinfo[0]['_Version'];
                $upgraded = $loginuserinfo[0]['_Upgraded'];
                $oldearning = $loginuserinfo[0]['_Oldbtc'];
                $rankey = $loginuserinfo[0]['_Payto'];
                $created = $loginuserinfo[0]['_Created'];
                 if($usertype == "0")
                 {
               
                  if($version == "0")
                  {
                           $earningpermin=earning_p_min;

                           /*if($btc == 'LTC')
                           {
                              $earningpermin= number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(earning_p_min)), 8, '.', '');
                           }

                           if($btc == 'ETH')
                           {
                              $earningpermin= number_format((float)$this->exp2dec(parent::btc_to_eth_convert(earning_p_min)), 8, '.', '');
                           }*/

                           $to_time = strtotime($today);
                           $from_time = strtotime($created);
                           $minute = round(abs($to_time - $from_time) / 60);

                           $earnings = $minute * $earningpermin;
                           $versiontxt = "v1.0";
                           $earningperday =earning_p_day;

                           /*if($btc == 'LTC')
                           {
                              $earningperday= number_format((float)$this->exp2dec(parent::btc_to_ltc_convert(earning_p_day)), 8, '.', '');
                           }

                           if($btc == 'ETH')
                           {
                              $earningperday= number_format((float)$this->exp2dec(parent::btc_to_eth_convert(earning_p_day)), 8, '.', '');
                           }*/
                    
                }
                else
                {
                     $versionresult=$this->App_model->getdata('bn_versions','_ID',$version); 
                   $earningpermin = number_format($versionresult[0]['_Btcpermin'],8);
                   $earningperday = $versionresult[0]['_Btcperday'];  
                    /*if($btc == 'LTC')
                    {
                      $earningpermin = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($versionresult[0]['_Btcpermin'])), 8, '.', '');

                      $earningperday = number_format((float)$this->exp2dec(parent::btc_to_ltc_convert($versionresult[0]['_Btcperday'])), 8, '.', '');
                    }

                    if($btc == 'ETH')
                    {
                      $earningpermin = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($versionresult[0]['_Btcpermin'])), 8, '.', '');

                      $earningperday = number_format((float)$this->exp2dec(parent::btc_to_eth_convert($versionresult[0]['_Btcperday'])), 8, '.', '');
                    }*/


                    if($version == "1")
                    {
                      $from_time  = strtotime($upgraded);
                      $to_time    = strtotime($today);
                      $minute     = round(abs($to_time - $from_time) / 60);
                      $newearning = $minute * $earningpermin;
                      $earnings   = $newearning + $oldearning;  
                      $versiontxt = "v1.1";   
                    }
                    else if($version == "2")
                    {
                      $from_time  = strtotime($upgraded);
                      $to_time    = strtotime($today);
                      $minute     = round(abs($to_time - $from_time) / 60);
                      $newearning = $minute * $earningpermin;
                      $earnings   = $newearning + $oldearning;
                      $versiontxt = "v1.2";
                      
                    }
                    else if($version == "3")
                    {

                      $from_time  = strtotime($upgraded);
                      $to_time    = strtotime($today);
                      $minute     = round(abs($to_time - $from_time) / 60);
                      $newearning = $minute * $earningpermin;
                      $earnings   = $newearning + $oldearning;
                      $versiontxt = "v1.3";
                  
                            }
                            else if($version == "4")
                            {

                      $from_time  = strtotime($upgraded);
                      $to_time    = strtotime($today);
                      $minute     = round(abs($to_time - $from_time) / 60);
                      $newearning = $minute * $earningpermin;
                      $earnings   = $newearning + $oldearning;
                      $versiontxt = "v1.4";
                  
                            }
                  
                }
               
             }
             else
             {
             
                  $earnings=$amount;
             
             }


             $earnings = $earnings;
             $checkearning = (float)$earnings;
            
             $withdrawarray=array(
                                '_UserID'=>$userid,
                                '_WithID'=>$withdrawalid,
                                '_Amount'=>$earnings,
                                '_Source'=>'Withdraw',
                                '_Status'=>'0',
                                '_Datetime'=>date('Y-m-d H:i:s')); 

             

             return $withdrawarray;

    }

    public function earningData()
    {

        $userid=$this->session->userdata['loginuser']['userid'];
             $loginuserinfo=$this->App_model->getdata('bn_users','_ID',$userid);
          
        $created = $loginuserinfo[0]['_Created'];
        $today=date('Y-m-d H:i:s');
        $earningpermin=earning_p_min;
        $to_time         = strtotime($today);
        $from_time       = strtotime($created);
        $minute          = round(abs($to_time - $from_time) / 60);
        $earnings        = $minute * $earningpermin;
        $versiontxt      = version_name;
        $earningperday   = earning_p_day;
    }
    
  /* public function set_cookie_custom( $cookie )
   {
       $this->input->set_cookie($cookie);
   }

   public function get_cookie_custom()
   {
      return $this->input->cookie('userid',true);
   }

   public function delete_cookie_custom()
   {
      delete_cookie('userid'); 
   }*/

   /*public function check_cookie()
    {
      
     
        $cookie = $this->input->cookie('userid',TRUE);
        if(isset($cookie))
        {
           //echo 'yes'; 
             redirect('Account/dashboard');
           
        }else
        {
           $data['resultbn_versions']=$this->App_model->getrecords('bn_versions');
            $this->load->view('theme/include/header');
            $this->load->view('theme/dashboard',$data);
            $this->load->view('theme/include/footer');
 
        }
    }*/
}
?>