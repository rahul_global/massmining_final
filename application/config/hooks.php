<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

/*$hook['post_controller'][] = array(
        'class'    => 'Cookie_hook',
        'function' => 'check_cookie_generates',
        'filename' => 'Cookie_hook.php',
        'filepath' => 'hooks',
        'params'   => array('beer', 'wine', 'snacks')
);*/

/*$hook['post_controller_construt'] = array(
        'class'    => 'Cookie_hook',
        'function' => 'check_cookie',
        'filename' => 'Cookie_hook.php',
        'filepath' => 'hooks',
        'params'   => array()
);*/

$hook['post_system'][] = array(
        'class'    => 'Login_hook',
        'function' => 'version_check',
        'filename' => 'Login_hook.php',
        'filepath' => 'hooks',
);